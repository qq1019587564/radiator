<?php
// +----------------------------------------------------------------------
// | 命令行生成单个图集页
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Env;
use think\facade\Db;
use app\admin\model\LinkModel;
use cmf\controller\HomeBaseController;
use think\facade\Queue;
use think\facade\View;
use think\facade\Cache;
use app\cms\model\CmsChannelModel;
use app\cms\model\CmsArticleModel;
use app\cms\model\CmsImageModel;
use app\cms\service\CmsService;
use app\cms\model\CmsPageModel;

class homeOneImage extends Command
{
    protected function configure()
    {
        // 生成文章页
        $this->setName('homeOneImage')
        ->addArgument('user_name',Option::VALUE_REQUIRED)//传递用户名  admin
        ->addOption('id', '-i',Option::VALUE_REQUIRED)//传递图集id 1
        ;
    }

    protected function execute(Input $input, Output $output)
    {
        error_reporting(E_ERROR | E_PARSE);
        cache('OneImageCreate',NULL);
        $userName = $input->getArgument('user_name')??'admin';
        $id = $input->getOption('id');
        if(empty($id))
        {
            $output->writeln('参数错误');
            exit();
        }
        $startTime = time();
        //插入日志
        $logData = ['type'=>'生成单个图集页','create_time'=>time(),'status'=>2,'user_name'=>$userName];
        $logId = Db::name('cms_create_log')->strict(false)->insertGetId($logData);
        
        $rootDir = WEB_ROOT.'/themes/xst/cms/';
        //公共数据
        $side = commonData()['side'];
        $setting = commonData()['setting'];
        View::assign('side', $side ?? '');
        View::assign('setting', $setting??"");
      
        try {
            //域名
        $domain  = request()->domain();
        //生成详情页
        //生成图集模型  START
        $CmsArticlelModel = new CmsImageModel();
        $article = $CmsArticlelModel->where(['status' => 1, 'delete_time' => 0,'id'=>$id])->select()->toArray();
        
        if (!empty($article)) {
            foreach ($article as $k => $v) {
                $channel = CmsChannelModel::where('id', $v['channel_id'])->where(['delete_time' => 0])->find();
                if (!empty($channel) && strpos($channel['article_tpl'],'show_') !==false ) {
                    if(!file_exists($rootDir. $channel['article_tpl'].'.html'))
                    {
                        continue;
                    }
                    $fields = DB::name('cms_fields_bind')->where('channel_id', $v['channel_id'])->where('status', 1)->select()->toArray();
                    foreach ($fields as $k1 => $v1) {
                        switch ($v1['type']) {
                            case 'editor':
                                if(!empty($v[$v1['name']]))
                                {
                                    $v[$v1['name']] = cmf_replace_content_file_url(htmlspecialchars_decode($v[$v1['name']]));
                                }
                                break;
                            case 'images':
                                $v[$v1['name']] = json_decode($v[$v1['name']], true);
                                if (!empty([$v1['name']][$v1['name']])) {
                                    $name = $v1['name'];
                                    foreach ($v1[$name] as $k2 => $v2) {
                                        @$v[$name][$k2]['url'] = image($v2['url']);
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    View::assign('channel', $channel);
                    View::assign('article', $v);
                    $prevArticle = CmsService::publishedPrevImage($v['id']);
                    $nextArticle = CmsService::publishedNextImage($v['id']);
                    View::assign('prev', $prevArticle);
                    View::assign('next', $nextArticle);

                    //模板地址
                    $template = root_path() . 'public' . $channel['route'] . $v['id'] . '.html';
                    $templateDir = root_path() . 'public' . $channel['route'];
                    //跳转的路径
                    $templateUrl = $domain . '/public' . $channel['route']   . $v['id'] . '.html';
                    //判断静态界面是否存在
                    beforeBuild($templateUrl, $templateDir, $template);
                    // TODO 需要填入的模板数据
                    $html = View::fetch($rootDir. $channel['article_tpl'].'.html');
                    //跳转手机端js
                    if(web_type())
                    {
                        $js = '<script>var os=function(){var a=navigator.userAgent,b=/(?:Windows Phone)/.test(a),c=/(?:SymbianOS)/.test(a)||b,d=/(?:Android)/.test(a),e=/(?:Firefox)/.test(a),g=(/(?:Chrome|CriOS)/.test(a),/(?:iPad|PlayBook)/.test(a)||d&&!/(?:Mobile)/.test(a)||e&&/(?:Tablet)/.test(a)),h=/(?:iPhone)/.test(a)&&!g,i=!h&&!d&&!c;return{isTablet:g,isPhone:h,isAndroid:d,isPc:i};}();(os.isAndroid||os.isPhone)&&(href=window.location.href.substring(9),window.location.href=window.location.protocol+"//"+window.location.host+"/m"+href.substring(href.indexOf("/"),href.length));</script></head>';
                        $html = str_replace(['__TMPL__','http://localhost','</head>'],['/themes/xst','',$js],$html);
                    }else{
                        $html = str_replace(['__TMPL__','http://localhost'],['/themes/xst',''],$html);
                    }
                    $sendData = ['url' => $templateUrl, 'temp' => $template, 'html' => $html];
                    $this->push('image', $sendData);
                } else {
                    continue;
                }
            }
        }
            
            
            $diff_time = $this->secToTime(time()- $startTime);
            $updateLogData = ['type'=>'生成'.$article['title'].'图集页','diff_time'=>$diff_time,'update_time'=>time(),'total'=>cache('OneImageCreate'),'status'=>1];
            $logId = Db::name('cms_create_log')->where('id',$logId)->update($updateLogData);
            //生成文章模型 END
            $output->writeln('生成图集页成功');
        } catch (\Exception $e) {
            $diff_time = $this->secToTime(time()- $startTime);
            $updateLogData = ['type'=>'生成'.$article['title'].'图集页','diff_time'=>$diff_time,'update_time'=>time(),'total'=>cache('OneImageCreate'),'status'=>0,'reason'=>$e->getMessage()];
            $logId = Db::name('cms_create_log')->where('id',$logId)->update($updateLogData);
            $output->writeln($e->getMessage());
        }
        
        
    }
    
    /**
     *      把秒数转换为时分秒的格式
     *      @param Int $times 时间，单位 秒
     *      @return String
     */
    public function secToTime($times){
            $result = '00:00:00';
            if ($times>0) {
                    $hour = floor($times/3600);
                    $minute = floor(($times-3600 * $hour)/60);
                    $second = floor((($times-3600 * $hour) - 60 * $minute) % 60);
                    $result = $hour.':'.$minute.':'.$second;
            }
            return $result;
    }
    
    /**
     * 队列推送
     *
     * @param string $type  任务类型
     * @param array $data 队列数据
     * @return void
     */
    public function push($type, $data)
    {
        Cache::inc('OneImageCreate');
        //当轮到该任务时，系统将生成一个该类的实例，并默认调用其 fire 方法
        $jobHandlerClassName = 'app\queue\controller\CreateController@' . $type;
        //将该任务推送到消息队列，等待对应的消费者去执行
        $isPushed = Queue::push($jobHandlerClassName, $data);
    }
}
