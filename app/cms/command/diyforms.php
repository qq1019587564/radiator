<?php
// +----------------------------------------------------------------------
// | 转换dede_diyforms表数据
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Env;
use think\facade\Db;
use app\admin\model\LinkModel;
use app\cms\model\CmsChannelModel;
use app\cms\model\CmsFieldsBindModel;
use app\cms\model\CmsFieldsModel;
use app\cms\library\Alter;

use think\facade\Console;
use think\facade\Config;

class diyforms extends Command
{
    protected function configure()
    {
        // 留言管理转换
        $this->setName('diyforms');
        // ->addArgument('name', Argument::OPTIONAL, "your name")
        //     ->addOption('city', '-c', Option::VALUE_REQUIRED, 'city name')
        //     ->setDescription('Say App Hello');
    }

    protected function execute(Input $input, Output $output)
    {
        // 启动事务
        Db::startTrans();
        try {


            $dede = Db::connect('dede')
                ->table('dede_diyforms')
                ->select()
                ->toArray();
            foreach ($dede as $k => $v) {
                //添加栏目
                $channelData = [
                    'name' => $v['name'],
                    'parent_id' => 0,
                    'status' => 0,
                    'model_id' => 3,
                    'type'=>3,
                    'list_order' => 300,
                    'create_time' => time(),
                    'update_time' => time(),
                ];
                $channel_id = Db::name('cms_channel')->insertGetId($channelData);
                $admin_url = '/cms/admin_message/index/channel_id/' . $channel_id . '/model_id/3';
                Db::name('cms_channel')->where('id', $channel_id)->update(['admin_url' => $admin_url]);
                //先查询模型已有的字段
                $fieldsData = [];
                $model_fields = CmsFieldsModel::where(['model_id' => 3, 'channel_id' => 0, 'status' => 1])->select()->toArray();
                foreach ($model_fields as $k11 => $v11) {
                    $fieldsData[$k11] = $v11;
                    $fieldsData[$k11]['field_id'] = $v11['id'];
                    $fieldsData[$k11]['channel_id'] = $channel_id ?? 1;
                    unset($fieldsData[$k11]['id']);
                }
                if (!empty($fieldsData)) {
                    $CmsFieldsBindModel = new CmsFieldsBindModel();
                    $CmsFieldsBindModel->saveAll($fieldsData);
                }

                preg_match_all("/(?:itemname=\")(.*)(?:\" autofield)/i", $v['info'], $nameData);
                preg_match_all("/(?:<field:)(.*)(?: itemname)/i", $v['info'], $fieldData);
                $newFieldData =  [];
                //组装新的数组
                foreach ($fieldData[1] as $k1 => $v1) {
                    $newFieldData[$k1]['name'] = $v;
                    $newFieldData[$k1]['title'] = $nameData[1][$k1];
                }
                //判断表单模型是否含有该字段
                $zid = Db::name('cms_fields')->where('model_id', 3)->column('name');
                $diff = array_diff($fieldData[1], $zid);
                
                $zidData = [];
                if (!empty($diff)) {
                    foreach ($diff as $k3 => $v3) {
                        $zidData[$k3] = [
                            "model_id" => "3",
                            "diyform_id" => "0",
                            "title" => $nameData[1][array_search($v3, $fieldData[1])],
                            "name" => $v3,
                            "type" => "string",
                            "content" => "",
                            "decimals" => "0",
                            "bindtext" => "",
                            "bindtexts" => "",
                            "sqltreeid" => "",
                            "sqlname" => "",
                            "sqlval" => "",
                            "showoption" => "select",
                            "showrow" => "",
                            "is_thumb" => "0",
                            "is_save" => "0",
                            "img_width" => "",
                            "img_height" => "",
                            "defaultvalue" => "",
                            "required" => "required",
                            "rule" => "",
                            "msg" => "",
                            "length" => "255",
                            "status" => "1",
                        ];
                        //判断字段是否存在
                        $sql = "select count(*) as num from information_schema.columns where table_name = 'xst_cms_message' and column_name = '" . $zidData[$k3]['name'] . "'";
                        
                        $count = Db::query($sql);
                        if ($count[0]['num'] == 0) {
                            $sql = Alter::instance()
                                ->setTable('cms_message')
                                ->setName($zidData[$k3]['name'])
                                ->setLength($zidData[$k3]['length'])
                                ->setContent($zidData[$k3]['content'])
                                ->setDecimals($zidData[$k3]['decimals'])
                                ->setDefaultvalue($zidData[$k3]['defaultvalue'])
                                ->setComment($zidData[$k3]['title'])
                                ->setType($zidData[$k3]['type'])
                                ->getAddSql();
                            Db::query($sql);
                        }
                    }
                    
                    //把没有的字段添加模型字段表里去
                    Db::name('cms_fields')->insertAll($zidData);


                    //先查询模型已有的字段
                    $fieldsData = [];
                    $model_fields = CmsFieldsModel::where(['model_id' => 3, 'channel_id' => 0, 'status' => 1])->select()->toArray();

                    foreach ($model_fields as $k11 => $v11) {
                        $fieldsData[$k11] = $v11;
                        $fieldsData[$k11]['field_id'] = $v11['id'];
                        $fieldsData[$k11]['channel_id'] = $channel_id ?? 1;
                        if (in_array($v11['name'], $fieldData[1])) {
                            $fieldsData[$k11]['title'] = $nameData[1][array_search($v11['name'], $fieldData[1])];
                        }
                        unset($fieldsData[$k11]['id']);
                    }
                    if (!empty($fieldsData)) {
                        $CmsFieldsBindModel = new CmsFieldsBindModel();
                        $CmsFieldsBindModel->saveAll($fieldsData);
                    }
                }

                //添加表单的内容
                $dede2 = Db::connect('dede')
                    ->table($v['table'])
                    ->select()
                    ->toArray();
                $formData = [];
                if (!empty($dede2)) {
                    foreach ($dede2 as $ks => $vs) {
                        $check = json_encode($vs);
                        if (strpos($check, 'OR') === false && strpos($check, 'select') === false && strpos($check, 'CHR') === false && strpos($check, 'sleep') === false) {
                            $formData[$ks] = array_filter($vs);
                            $formData[$ks]['user_id'] = 1;
                            $formData[$ks]['channel_id'] = $channel_id;
                            $formData[$ks]['model_id'] = 3;
                            $formData[$ks]['create_time'] = time();
                            $formData[$ks]['update_time'] = time();
                            unset($formData[$ks]['id']);
                            unset($formData[$ks]['ifcheck']);
                            Db::name('cms_message')->insert($formData[$ks]);
                        }
                    }
                    // dump($formData);die;
                    // Db::name('cms_message')->insertAll($formData);
                }
            }

            Db::commit();
        } catch (\Exception $th) {
            $output->writeln($th->getMessage());
            // 回滚事务
            Db::rollback();
            return;
        }
        $output->writeln('dede_diyforms 转换成功');
    }
}
