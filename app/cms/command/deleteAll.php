<?php
// +----------------------------------------------------------------------
// | 清楚数据
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Env;
use think\facade\Db;
use app\admin\model\LinkModel;

class deleteAll extends Command
{
    protected function configure()
    {
        // 
        $this->setName('deleteAll');
    }

    protected function execute(Input $input, Output $output)
    {
        // 启动事务
        Db::startTrans();
        try {
            //数据库前缀
            $prefix = Env::get('DATABASE_PREFIX');
            // truncate table users
            $table = Db::name('cms_model')->column('table');
            $sql = 'truncate table ';
            foreach ($table as $k => $v) {
              $table[$k] = $sql . $prefix . $v;
            }
            // array_push($table, $sql . 'xst_nav');
            // array_push($table, $sql . 'xst_nav_menu');
            array_push($table, $sql . 'xst_setting');
            array_push($table, $sql . 'xst_operation_log');
            array_push($table, $sql . 'xst_recycle_bin');
            array_push($table, $sql . 'xst_login_log');
            array_push($table, $sql . 'xst_cms_channel');
            array_push($table, $sql . 'xst_cms_banner');
            array_push($table, $sql . 'xst_cms_fields_bind');
            array_push($table, $sql . 'xst_link');
            array_push($table, $sql . 'xst_slide');
            array_push($table, $sql . 'xst_slide_item');
            array_push($table, $sql . 'xst_asset');
            array_push($table,  'delete from xst_route where id>1');
            foreach ($table as $v) {
                Db::query($v);
              }
            // 提交事务
            Db::commit();
        } catch (\Exception $th) {
            $output->writeln($th->getMessage());
            // 回滚事务
            Db::rollback();
            // dump($th->getMessage());die;
            // $output->writeln($th->getMessage());
            return;
        }
        $output->writeln('ok');
    }
}
