<?php
// +----------------------------------------------------------------------
// | 转换dede_flink表数据
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Env;
use think\facade\Db;
use app\admin\model\LinkModel;

class flink extends Command
{
    protected function configure()
    {
        // 友情链接转换
        $this->setName('flink');
    }

    protected function execute(Input $input, Output $output)
    {
        // 启动事务
        Db::startTrans();
        try {
            $i = 0;
            $dede = Db::connect('dede')
                ->table('dede_flink')
                ->select()
                ->toArray();
                // dump($dede);die;
            // $newData  = [];
            foreach ($dede as $k => $v) {
                $newData[$k]['name'] = $v['webname'];
                $newData[$k]['url'] = $v['url'];
                $newData[$k]['list_order'] = $v['sortrank'];
                $newData[$k]['image'] = $v['logo'];
            }
            // dump($newData);die;
            Db::name('link')->insertAll($newData);
            // 提交事务
            Db::commit();
        } catch (\Exception $th) {
            $output->writeln($th->getMessage());
            // 回滚事务
            Db::rollback();
            return;
        }
        // return 'dede_flink 数据转换成功，成功转换：' . $i . '条';
        $output->writeln('dede_flink 数据转换成功');
    }
}
