<?php
// +----------------------------------------------------------------------
// | 转换dede_sysconfig表数据
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use app\form\model\FormModel;
use think\facade\Env;
use think\facade\Db;
use app\cms\model\CmsChannelModel;
use app\cms\model\CmsPageModel;
use app\cms\model\CmsModelModel;
use app\cms\model\CmsFieldsBindModel;
use app\cms\model\CmsFieldsModel;
use app\admin\model\RouteModel;

class arctype extends Command
{
    protected function configure()
    {
        // 转换栏目
        $this->setName('arctype');
    }

    protected function execute(Input $input, Output $output)
    {

        try {
            $i = 0;
            $dede = Db::connect('dede')
                ->table('dede_arctype')
                // ->field('id,ispart,reid,topid,sortrank,typename,typedir,channeltype,tempindex,templist,temparticle,description,keywords,seotitle,content,ishidden,typenameen,typeimg')
                ->select()
                ->toArray();
            $newData  = [];
            $routeModel = new RouteModel();
            foreach ($dede as $k => $v) {
                $newData[$i]['id'] = $v['id'];
                $newData[$i]['model_id'] = $v['channeltype'];
                if ($newData[$i]['model_id'] == 13 || $newData[$i]['model_id'] == 14) {
                    $newData[$i]['model_id'] = 1;
                }
                if($v['channeltype']>14)
                {
                    $newData[$i]['model_id'] = 1;
                }
                switch ($v['ispart']) {
                    case '0':
                        $part = 1; //列表
                        break;
                    case '1':
                        $part = 2; //单页
                        $newData[$i]['model_id'] = 5;
                        break;
                    default:
                        $part = 1; //列表
                        break;
                }
                $newData[$i]['type'] = $part;
                $newData[$i]['parent_id'] = $v['reid'];
                $newData[$i]['list_order'] = $v['sortrank'];
                $newData[$i]['name'] = $v['typename'];
                $newData[$i]['route'] = str_replace(['/{cmspath}', '{cmspath}'], '', $v['typedir']);
                //判断前面是否有斜杠/
                if (substr($newData[$i]['route'], 0, 1) != '/') {
                    $newData[$i]['route'] = '/' . $newData[$i]['route'];
                }
                //判断最后一个是否有斜杠/
                if (substr($newData[$i]['route'], -1) != '/') {
                    $newData[$i]['route'] =  $newData[$i]['route'] . '/';
                    // $newData[$i]['route'] =  rtrim($newData[$i]['route'],'/');
                }
                $newData[$i]['page_tpl'] = str_replace(['{style}/', '.htm', 'index_'],  ['', '', 'page_'], $v['tempindex']);
                //


                $newData[$i]['list_tpl'] = str_replace(['{style}/', '.htm'], '', $v['templist']);
                $newData[$i]['article_tpl'] = str_replace(['{style}/', '.htm', 'article_'], ['', '', 'show_'], $v['temparticle']);
                $newData[$i]['seo_description'] = $v['description'];
                $newData[$i]['description'] = $v['description'];
                $newData[$i]['seo_keywords'] = $v['keywords'];
                $newData[$i]['seo_title'] = $v['seotitle'];
                $newData[$i]['page_id'] = 0;
                if ($v['ishidden'] == 0) {
                    $ishidden = 1;
                } else {
                    $ishidden = 0;
                }
                $newData[$i]['status'] = $ishidden;
                $newData[$i]['alias'] = $v['typenameen'] ?? " ";
                $newData[$i]['thumbnail'] = $v['typeimg'] ?? "";

                $table_name = CmsModelModel::where('id', $newData[$i]['model_id'])->value('table');
                $table_name = str_replace('cms', 'admin', $table_name);
                //添加单页数据
                if ($part == 2) {
                    $page['id'] = $newData[$i]['id'];
                    $page['title'] = $newData[$i]['name'];
                    $page['channel_id'] = $newData[$i]['id'];
                    $page['user_id'] = 1;
                    $page['content'] = $v['content'];
                    $CmsPageModel = new CmsPageModel();
                    $CmsPageModel->save($page);
                    $pageId = $CmsPageModel->id;
                    $newData[$i]['page_id'] = $pageId;
                    $admin_url = '/cms/' . $table_name . '/index/channel_id/' . $newData[$i]['id'] . '/model_id/' . $newData[$i]['model_id'] . '/id/' . $pageId;
                } else {
                    $admin_url = '/cms/' . $table_name . '/index/channel_id/' . $newData[$i]['id'] . '/model_id/' . $newData[$i]['model_id'];
                }
                $newData[$i]['admin_url'] = $admin_url;
                $newData[$i]['create_time'] = time();
                $newData[$i]['update_time'] = time();

                //添加栏目 把模型已有的字段都添加进去
                //先查询模型已有的字段
                $fieldsData = [];
                $model_fields = CmsFieldsModel::where(['model_id' => $newData[$i]['model_id'], 'channel_id' => 0, 'status' => 1])->select()->toArray();
                foreach ($model_fields as $k1 => $v1) {
                    $fieldsData[$k1] = $v1;
                    $fieldsData[$k1]['field_id'] = $v1['id'];
                    $fieldsData[$k1]['channel_id'] = $newData[$i]['id'];
                    unset($fieldsData[$k1]['id']);
                }
                if (!empty($fieldsData)) {
                    $CmsFieldsBindModel = new CmsFieldsBindModel();
                    $CmsFieldsBindModel->saveAll($fieldsData);
                }
                //设置伪静态
                //判断是否有同名的栏目，有则添加随机字符串
//                $route = RouteModel::where('url', $newData[$i]['route'])->where('status', 1)->where('channel_id', '<>', $newData[$i]['id'])->count();
//                if ($route >= 1) {
//                    $str = cmf_random_string(5) . '/';
//                    $newData[$i]['route'] = rtrim($newData[$i]['route'], '/') . $str;
//                }
                if (!empty($newData[$i]['route'])) {
                    $level = explode('/', $newData[$i]['route']);
                    $list_order = 100 - count($level);
                    //判断有没有手机端
                    if (web_type()) {
                        $routem = '/m' . $newData[$i]['route'];
                        $levelm = explode('/', '/m' . $routem);
                        $list_orderm = 100 - count($levelm);
                        // $routeModel->setRoute('/m', 'm/Article/index', ['cid' => $newData[$i]['id']], 2, '4998', $newData[$i]['id']);
                    }

                    switch ($newData[$i]['type']) {
                        case '1':
                            $routeModel->setRoute($newData[$i]['route'], 'cms/List/index', ['id' => $newData[$i]['id']], 2, $list_order, $newData[$i]['id']);
                            if ($newData[$i]['model_id'] == 1) {
                                $routeModel->setRoute($newData[$i]['route'] . ':id', 'cms/Article/index', ['cid' => $newData[$i]['id']], 2, $list_order, $newData[$i]['id']);
                            } else {
                                $routeModel->setRoute($newData[$i]['route'] . ':id', 'cms/Image/index', ['cid' => $newData[$i]['id']], 2, $list_order, $newData[$i]['id']);
                            }
                            //添加手机端路由
                            if (isset($routem) && $levelm && $list_orderm) {
                                $routeModel->setRoute($routem, 'm/List/index', ['id' => $newData[$i]['id']], 2, $list_orderm, $newData[$i]['id']);
                                if ($newData[$i]['model_id'] == 1) {
                                    $routeModel->setRoute($routem . ':id', 'm/Article/index', ['cid' => $newData[$i]['id']], 2, $list_orderm, $newData[$i]['id']);
                                } else {
                                    $routeModel->setRoute($routem . ':id', 'm/Image/index', ['cid' => $newData[$i]['id']], 2, $list_orderm, $newData[$i]['id']);
                                }
                            }
                            break;
                        default:
                            //$routeModel->setRoute($newData[$i]['route'], 'cms/Page/index', ['id' => $pageId], 2, $list_order, $newData[$i]['id']);
                            $routeModel->getRoute($newData[$i]['route'], 'cms/Page/index', ['id' => $pageId], 2, $list_order, $newData[$i]['id'], $newData[$i]['id'], $newData[$i]['parent_id']);
                            //添加手机端路由
                            if (isset($routem) && $levelm && $list_orderm) {
                                $routeModel->getRoute($routem, 'm/Page/index', ['id' => $pageId], 2, $list_orderm, $newData[$i]['id'], $newData[$i]['id'], $newData[$i]['parent_id']);
                            }
                            break;
                    }
                }
                $routeModel->getRoutes(true);

                $i++;
            }

            $pageTplData = array_column($newData, 'page_tpl');
            $pagePageData = ['dailijizhang','gongsibiangeng','gongsizhuxiao','shangbiaozhuce','zhucegongsi','lianxi','book','singlepage','danye', 'danye_m', 'liuyan', 'liuyan_m', 'contact', 'contact_m', 'message', 'message_m', 'map', 'map_m', 'join', 'join_m', 'about', 'about_m', 'service', 'service_m'];
            $mubanTpl = Db::name('theme_file')->where('theme', 'xst')->column('file');
            // dump($mubanTpl);die;
            $mubanTplData = [];
            foreach ($mubanTpl as $k => $v) {
                $mubanTplData[] = str_replace('cms/', '', $v);
            }

            foreach ($pageTplData as $k1 => $v1) {
                if (strpos($v1, "page_") === false) {
                    $newTpl  = 'page_' . $v1 . '1';
                    if (in_array($v1, $pagePageData) && in_array($newTpl, $mubanTplData)) {
                        $newData[$k1]['page_tpl']  = 'page_' . $v1 . '1';
                    } else {
                        $newData[$k1]['page_tpl']  = 'page_' . $v1;
                    }
                }
            }

            Db::name('cms_channel')->insertAll($newData);
        } catch (\Exception $th) {
            dump($th);
            die;
            // $output->writeln($th->getMessage());
            return;
        }
        $output->writeln('dede_arctype 数据转换成功');
    }
}
