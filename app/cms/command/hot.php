<?php
// +----------------------------------------------------------------------
// | 转换dede_myppt表数据
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Env;
use think\facade\Db;
use app\admin\model\LinkModel;

class hot extends Command
{
    protected function configure()
    {
        // 热门搜索转换
        $this->setName('hot');
    }

    protected function execute(Input $input, Output $output)
    {
        // 启动事务
        Db::startTrans();
        try {
            $i = 0;
            $dede = Db::connect('dede')
                ->table('dede_search_keywords')
                ->select()
                ->toArray();
            $newData = [];
            if (!count($dede)) {
                $output->writeln('暂无数据');
                return;
            }
                foreach ($dede as $k => $v) {
                    $newData[$i]['key'] = $v['keyword'];
                    $newData[$i]['status'] =1;
                    $newData[$i]['spwords'] = $v['spwords'];
                    $newData[$i]['create_time'] = $v['lasttime'];
                    $i++;
                }
                Db::name('hot')->insertAll($newData);
                // 提交事务
                Db::commit();
            }
        catch
            (\Exception $th) {
                $output->writeln($th->getMessage());
                // 回滚事务
                Db::rollback();
                // dump($th->getMessage());die;
                // $output->writeln($th->getMessage());
                return;
            }

        return 'dede_search_keywords 数据转换成功';
        // $output->writeln($a);
    }
}
