<?php
// +----------------------------------------------------------------------
// | 转换dede_archives表数据
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Env;
use think\facade\Db;
use app\admin\model\LinkModel;
use app\cms\model\CmsChannelModel;

class archives extends Command
{
    protected function configure()
    {
        // 文章转换
        $this->setName('archives');
    }

    protected function execute(Input $input, Output $output)
    {
        // 启动事务
        Db::startTrans();
        try {
            $i = 0;
            $a = 0;
            $b = 0;

            $dede = Db::connect('dede')
                ->table('dede_archives')
                ->alias('a')
                ->join('dede_arctype b', 'a.typeid=b.id')
                // ->join('dede_addonarticle c','a.id=c.aid')
                // ->join('dede_addonimages d','a.id=d.aid')
                ->field('a.*,b.channeltype')
                // ->fetchSql(true)
                ->select()
                // dump($dede);die;
                ->toArray();
            $newData  = [];
            $new2Data  = [];

            //回收站
            $recycleData  = [];
            $tableList = [
                1 => 'cms_article',
                2 => 'cms_image',
                3 => 'cms_message',
                4 => 'cms_category',
                5 => 'cms_page',
                13 => 'cms_article',
                14 => 'cms_article',
                15 => 'cms_article',
                16 => 'cms_article',
                17 => 'cms_article',
                18 => 'cms_article',
            ];

            foreach ($dede as $k => $v) {

                $table_name = $tableList[$v['channeltype']];
                
                switch ($v['channeltype']) {
                    case '1':
                    case '13':
                    case '14':
                    case '15':
                    case '16':
                    case '17':
                    case '18':
                        //文章模型
                        $newData[$i]['id'] = $v['id'];
                        $newData[$i]['channel_id'] = $v['typeid'];
                        $newData[$i]['title'] = $v['title'];
                        $newData[$i]['click'] = $v['click'];
                        $newData[$i]['author'] = $v['writer'];
                        $newData[$i]['thumbnail'] = $v['litpic'];
                        $newData[$i]['published_time'] = date('Y-m-d H:i',$v['pubdate']);
                        $newData[$i]['seo_description'] = $v['description'];
                        $newData[$i]['seo_keywords'] = $v['keywords'];
                        $newData[$i]['list_order'] = $v['weight'];
                        $newData[$i]['create_time'] = time();
                        $newData[$i]['update_time'] = time();
                        $newData[$i]['delete_time'] = 0;

                        $newData[$i]['content'] =  Db::connect('dede')
                            ->table('dede_archives')
                            ->alias('a')
                            ->join('dede_addonarticle b', 'a.id=b.aid')
                            ->where('b.aid', $v['id'])
                            ->value('body') ?? "";

                        //回收站数据
                        if ($v['arcrank'] == -2) {
                            $recycleData[$a]['object_id'] = $v['id'];
                            $recycleData[$a]['name'] = $v['title'];
                            $recycleData[$a]['table_name'] = $table_name;
                            $newData[$i]['delete_time'] = $recycleData[$a]['create_time'] = time();
                            $recycleData[$a]['user_id'] = 1;
                            $a++;
                        }
                        $i++;
                        break;
                    case '2':
                        //图集模型
                        $new2Data[$b]['id'] = $v['id'];
                        $new2Data[$b]['channel_id'] = $v['typeid'];
                        $new2Data[$b]['title'] = $v['title'];
                        $new2Data[$b]['click'] = $v['click'];
                        $new2Data[$b]['author'] = $v['writer'];
                        $new2Data[$b]['thumbnail'] = $v['litpic'];
                        $new2Data[$b]['published_time'] = date('Y-m-d H:i',$v['pubdate']);
                        $new2Data[$b]['seo_description'] = $v['description'];
                        $new2Data[$b]['seo_keywords'] = $v['keywords'];
                        $new2Data[$b]['list_order'] = $v['weight'];
                        $new2Data[$b]['create_time'] = time();
                        $new2Data[$b]['update_time'] = time();
                        $new2Data[$b]['delete_time'] = 0;

                        $new2Data[$b]['content'] =  Db::connect('dede')
                            ->table('dede_archives')
                            ->alias('a')
                            ->join('dede_addonimages b', 'a.id=b.aid')
                            ->where('b.aid', $v['id'])
                            ->value('body') ?? "";
                        //图集
                        $dede_listimg = Db::connect('dede')
                            ->table('dede_archives')
                            ->alias('a')
                            ->join('dede_addonimages b', 'a.id=b.aid')
                            ->where('b.aid', $v['id'])
                            ->value('imgurls') ?? "";
                        $new2Data[$b]['list_img'] = '';
                        if (!empty($dede_listimg)) {
                            $imgData = [];
                            preg_match_all("/(?:ddimg=')(.*)(?:' t)/i", $dede_listimg, $list_img);
                            if (!empty($list_img[1])) {
                                foreach ($list_img[1] as $k2 => $v2) {
                                    $imgData[$k2]['url'] = $v2;
                                    $imgData[$k2]['name'] = getFileName($v2);
                                }
                                $new2Data[$b]['list_img'] = json_encode($imgData);
                            }
                        }
                        //回收站数据
                        if ($v['arcrank'] == -2) {
                            $recycleData[$a]['object_id'] = $v['id'];
                            $recycleData[$a]['name'] = $v['title'];
                            $recycleData[$a]['table_name'] = $table_name;
                            $new2Data[$b]['delete_time'] = $recycleData[$a]['create_time'] = time();
                            $recycleData[$a]['user_id'] = 1;
                            $a++;
                        }
                        $b++;
                        break;
                    default:
                         //文章模型
                        $newData[$i]['id'] = $v['id'];
                        $newData[$i]['channel_id'] = $v['typeid'];
                        $newData[$i]['title'] = $v['title'];
                        $newData[$i]['click'] = $v['click'];
                        $newData[$i]['author'] = $v['writer'];
                        $newData[$i]['thumbnail'] = $v['litpic'];
                        $newData[$i]['published_time'] = date('Y-m-d H:i',$v['pubdate']);
                        $newData[$i]['seo_description'] = $v['description'];
                        $newData[$i]['seo_keywords'] = $v['keywords'];
                        $newData[$i]['list_order'] = $v['weight'];
                        $newData[$i]['create_time'] = time();
                        $newData[$i]['update_time'] = time();
                        $newData[$i]['delete_time'] = 0;

                        $newData[$i]['content'] =  Db::connect('dede')
                            ->table('dede_archives')
                            ->alias('a')
                            ->join('dede_addonarticle b', 'a.id=b.aid')
                            ->where('b.aid', $v['id'])
                            ->value('body') ?? "";

                        //回收站数据
                        if ($v['arcrank'] == -2) {
                            $recycleData[$a]['object_id'] = $v['id'];
                            $recycleData[$a]['name'] = $v['title'];
                            $recycleData[$a]['table_name'] = $table_name;
                            $newData[$i]['delete_time'] = $recycleData[$a]['create_time'] = time();
                            $recycleData[$a]['user_id'] = 1;
                            $a++;
                        }
                        $i++;
                        break;
                        break;
                }
            }


            //回收站数据
            Db::name('recycle_bin')->insertAll($recycleData);
            //文章模型
            Db::name('cms_article')->insertAll($newData);
            //图集模型
            Db::name('cms_image')->insertAll($new2Data);
            // 提交事务
            Db::commit();
        } catch (\Exception $th) {
            $output->writeln($th->getMessage());
            // 回滚事务
            Db::rollback();
            dump($th);die;
            // $output->writeln($th->getMessage());
            return;
        }
        return 'dede_archives 数据转换成功，成功转换：' . $i . '条';
        // $output->writeln($a);
    }
}
