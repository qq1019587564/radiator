<?php
// +----------------------------------------------------------------------
// | 命令行生成手机端首页
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Env;
use think\facade\Db;
use app\admin\model\LinkModel;
use cmf\controller\HomeBaseController;
use think\facade\Queue;
use think\facade\View;
use think\facade\Cache;

class mobile extends Command
{
   
    protected function configure()
    {
        // 生成首页
        $this->setName('mobile')
        ->addArgument('user_name',Option::VALUE_REQUIRED)//传递用户名  admin
        ;
    }

    protected function execute(Input $input, Output $output)
    {
        error_reporting(E_ERROR | E_PARSE);
        cache('MobileCreate',NULL);
        $rootDir = WEB_ROOT.'/themes/xst/cms/';
        $startTime = time();
        $output->writeln('手机端首页开始生成');
        
        //公共数据
        $side = commonData()['side'];
        $setting = commonData()['setting'];
        View::assign('side', $side ?? '');
        View::assign('setting', $setting??"");
        
        
     
        try {
            //插入日志
            $userName = $input->getArgument('user_name')??'admin';
            $logData = ['type'=>'生成手机端首页','create_time'=>time(),'status'=>2,'user_name'=>$userName];
            $logId = Db::name('cms_create_log')->strict(false)->insertGetId($logData);
            
            //生成首页START
            //域名
            $domain  = request()->domain();
            // //生成首页
            $url = request()->url(true);
            //模板生成的地址
            $template = root_path() . 'public/m/index.html';
            //跳转的路径
            $templateUrl = $url . 'm/index.html';
    
            $html = View::fetch($rootDir.'index_m.html',['setting'=>$setting]);
            $html = str_replace(['http://localhost','__TMPL__'],['','/themes/xst'],$html);
            // dump($html);die;
            chmod(root_path() . 'public', 0755);
            
            $sendData = ['url' => $templateUrl, 'temp' => $template, 'html' => $html];
            $this->push('index', $sendData);
            //生成首页END  OK
            $output->writeln('手机端首页生成成功');
            $diff_time = $this->secToTime(time()- $startTime);
            $updateLogData = ['diff_time'=>$diff_time,'update_time'=>time(),'total'=>cache('MobileCreate'),'status'=>1];
            $logId = Db::name('cms_create_log')->where('id',$logId)->update($updateLogData);
            //生成列表页END
            $output->writeln('手机端首页生成成功'.date('Y-m-d H:i:s',time()).'——'.cache('MobileCreate'));
        } catch (\Exception $e) {
            $diff_time = $this->secToTime(time()- $startTime);
            $updateLogData = ['diff_time'=>$diff_time,'update_time'=>time(),'total'=>cache('MobileCreate'),'status'=>0,'reason'=>$e->getMessage()];
            $logId = Db::name('cms_create_log')->where('id',$logId)->update($updateLogData);
            $output->writeln($e->getMessage());
        
        }
    }
    
    
    /**
     *      把秒数转换为时分秒的格式
     *      @param Int $times 时间，单位 秒
     *      @return String
     */
    public function secToTime($times){
            $result = '00:00:00';
            if ($times>0) {
                    $hour = floor($times/3600);
                    $minute = floor(($times-3600 * $hour)/60);
                    $second = floor((($times-3600 * $hour) - 60 * $minute) % 60);
                    $result = $hour.':'.$minute.':'.$second;
            }
            return $result;
    }
    
    
    /**
     * 队列推送
     *
     * @param string $type  任务类型
     * @param array $data 队列数据
     * @return void
     */
    public function push($type, $data)
    {
        Cache::inc('MobileCreate');
        //当轮到该任务时，系统将生成一个该类的实例，并默认调用其 fire 方法
        $jobHandlerClassName = 'app\queue\controller\CreateController@' . $type;
        //将该任务推送到消息队列，等待对应的消费者去执行
        $isPushed = Queue::push($jobHandlerClassName, $data);
    }
}
