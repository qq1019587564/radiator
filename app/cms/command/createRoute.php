<?php
// +----------------------------------------------------------------------
// | 生成伪静态
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Env;
use think\facade\Db;
use app\admin\model\RouteModel;

class createRoute extends Command
{
    protected function configure()
    {
        $this->setName('createRoute');
    }

    protected function execute(Input $input, Output $output)
    {
        // 启动事务
        Db::startTrans();
        try {
            //设置伪静态
            $routeModel = new RouteModel();
            $routeModel->getRoutes(true);
            // 提交事务
            Db::commit();
        } catch (\Exception $th) {
            $output->writeln($th->getMessage());
            // 回滚事务
            Db::rollback();
            return;
        }
        $output->writeln('伪静态生成成功');
    }
}
