<?php
// +----------------------------------------------------------------------
// | 转换dede_sysconfig表数据
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use app\form\model\FormModel;
use think\facade\Env;
use think\facade\Db;
use app\setting\model\SettingModel;

class sysconfig extends Command
{
    protected function configure()
    {
        // 转换系统基本设置
        $this->setName('sysconfig');
    }

    protected function execute(Input $input, Output $output)
    {

        // 启动事务
        Db::startTrans();
        try {
            $i = 0;
            $dedeConfig = Db::connect('dede')
                ->table('dede_sysconfig')
                ->where('groupid', 1)
                ->select()
                ->toArray();
            $newData  = [];
            $noSet = ['cfg_basehost', 'cfg_indexurl', 'cfg_indexname', 'cfg_arcdir', 'cfg_medias_dir', 'cfg_fck_xhtml', 'cfg_df_style', ''];
            foreach ($dedeConfig as $k => $v) {
                if (in_array($v['varname'], $noSet)) {
                    continue;
                }
                switch ($v['type']) {
                    case 'string':
                        $newData[$i]['type'] = 1; //单行
                        break;
                    case 'bstring':
                        $newData[$i]['type'] = 2; //多行
                        break;
                    case 'pic':
                        $newData[$i]['type'] = 3; //图片
                        break;
                    default:
                        $newData[$i]['type'] = 1;
                        break;
                }
                $newData[$i]['name'] = $v['info'];
                $newData[$i]['read_name'] = str_replace('cfg_', '', $v['varname']);
                $newData[$i]['value'] = $v['value'];
                $newData[$i]['list_order'] = 100;

                if ($v['varname'] == 'cfg_webname') {
                    $newData[$i]['list_order'] = 1;
                    $newData[$i]['name'] = '首页SEO标题';
                    $newData[$i]['read_name'] = 'seo_title';
                }
                if ($v['varname'] == 'cfg_keywords') {
                    $newData[$i]['list_order'] = 2;
                    $newData[$i]['name'] = '首页SEO关键字';
                    $newData[$i]['read_name'] = 'seo_keywords';
                }
                if ($v['varname'] == 'cfg_description') {
                    $newData[$i]['list_order'] = 3;
                    $newData[$i]['name'] = '首页SEO描述';
                    $newData[$i]['read_name'] = 'seo_descriptions';
                }
                $i++;
            }
            $SettingModel = new SettingModel();
            $SettingModel->saveAll($newData);
            // 提交事务
            Db::commit();
        } catch (\Exception $th) {
            // 回滚事务
            Db::rollback();
            // dump($th->getMessage());die;
            $output->writeln($th->getMessage());
            return;
        }
        return 'dede_sysyconfig 数据转换成功，成功转换：' . $i . '条';
        // $output->writeln('dede_sysyconfig 数据转换成功，成功转换：' . $i . '条');
    }
}
