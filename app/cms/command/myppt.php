<?php
// +----------------------------------------------------------------------
// | 转换dede_myppt表数据
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Env;
use think\facade\Db;
use app\admin\model\LinkModel;

class myppt extends Command
{
    protected function configure()
    {
        // 幻灯片转换
        $this->setName('myppt');
    }

    protected function execute(Input $input, Output $output)
    {
        // 启动事务
        Db::startTrans();
        try {
            $i = 0;
            $dede = Db::connect('dede')
                ->query('show tables like "dede_myppt"');
            if (!empty($dede)) {
                //添加幻灯片分组
                $slideData = [];
                $newData = [];
                $myppttype = Db::connect('dede')->table('dede_myppttype')->select()->toArray();
                if (empty($myppttype)) {
                    $slideData = ['status' => 1, 'name' => '默认'];
                    Db::name('slide')->insert($slideData);
                } else {
                    foreach ($myppttype as $k => $v) {
                        $slideData[$k]['id'] = $v['id'];
                        $slideData[$k]['name'] = $v['typename'];
                        $slideData[$k]['status'] = 1;
                    }
                    Db::name('slide')->insertAll($slideData);
                }

                //添加幻灯片
                $myppt = Db::connect('dede')->table('dede_myppt')->select()->toArray();
                if (!empty($myppt)) {
                    foreach ($myppt as $k => $v) {
                        $newData[$k]['id'] = $v['aid'];
                        $newData[$k]['slide_id'] = $v['typeid'];
                        $newData[$k]['status'] = 1;
                        $newData[$k]['list_order'] = $v['orderid'];
                        $newData[$k]['title'] = $v['title'];
                        $newData[$k]['image'] = $v['pic'];
                        $newData[$k]['url'] = $v['url'];
                    }
                    Db::name('slide_item')->insertAll($newData);
                }
            }
            // 提交事务
            Db::commit();
        } catch (\Exception $th) {
            $output->writeln($th->getMessage());
            // 回滚事务
            Db::rollback();
            return;
        }
        return 'dede_myppt 数据转换成功';
        // $output->writeln($a);
    }
}
