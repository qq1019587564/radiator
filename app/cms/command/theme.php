<?php
// +----------------------------------------------------------------------
// | 命令行更新模板文件
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Env;
use think\facade\Db;
use cmf\controller\HomeBaseController;
use think\facade\Queue;
use think\facade\View;
use think\facade\Cache;
use app\admin\model\ThemeModel;
use app\admin\model\ThemeFileModel;

class theme extends Command
{
   
    protected function configure()
    {
        // 更新模板
        $this->setName('theme');
    }

    protected function execute(Input $input, Output $output)
    {
        //模板文件
        $theme = 'xst';
        error_reporting(E_ERROR | E_PARSE);
        //判断模板目录是否存在
        $tpl  = WEB_ROOT.'/themes/xst/cms';
        if(is_dir($tpl))
        {
            $files = scandir($tpl);
            foreach ($files as $k => $v) {
                if ($v == '.' || $v == '..') {
                    continue;
                }
                $fileName = $tpl . '/' . $v;
                //把后缀htm换成html
                if (getExtension($v) == 'html') {
                    $jsonDir = str_replace('/cms', '', $tpl);
                    //生成对应的json文件
                    if (strpos($fileName, 'show_') !== false) {
                        $jsonName = str_replace('.html', '.json', $fileName);
                        $jsonContent = file_get_contents($jsonDir . '/public/show_detail.json');
                        file_put_contents($jsonName, $jsonContent);
                    }
                    if (strpos($fileName, 'page_') !== false) {
                        $jsonName = str_replace('.html', '.json', $fileName);
                        $jsonContent = file_get_contents($jsonDir . '/public/page.json');
                        file_put_contents($jsonName, $jsonContent);
                    }
                    if (strpos($fileName, 'list_') !== false) {
                        $jsonName = str_replace('.html', '.json', $fileName);
                        $jsonContent = file_get_contents($jsonDir . '/public/list_example.json');
                        file_put_contents($jsonName, $jsonContent);
                    }
                    
                    $configFile = str_replace('html','json',$fileName);
                    if(file_exists($configFile))
                    {
                        $file       = strtolower("cms/{$v}");
                        $config     = json_decode(file_get_contents($configFile), true);
                        $findFile   = ThemeFileModel::where(['theme' => $theme, 'file' => $file])->find();
                        $isPublic   = empty($config['is_public']) ? 0 : 1;
                        $listOrder  = empty($config['order']) ? 0 : floatval($config['order']);
                        $configMore = empty($config['more']) ? [] : $config['more'];
                        $more       = $configMore;
                
                        if (empty($findFile)) {
                            ThemeFileModel::insert([
                                'theme'       => $theme,
                                'action'      => $config['action'],
                                'file'        => $file,
                                'name'        => $config['name'],
                                'more'        => json_encode($more),
                                'config_more' => json_encode($configMore),
                                'description' => $config['description'],
                                'is_public'   => $isPublic,
                                'list_order'  => $listOrder
                            ]);
                        } else { // 更新文件
                            $moreInDb = $findFile['more'];
                            $more     = $this->updateThemeConfigMore($configMore, $moreInDb);
                            ThemeFileModel::where(['theme' => $theme, 'file' => $file])->update([
                                'theme'       => $theme,
                                'action'      => $config['action'],
                                'file'        => $file,
                                'name'        => $config['name'],
                                'more'        => json_encode($more),
                                'config_more' => json_encode($configMore),
                                'description' => $config['description'],
                                'is_public'   => $isPublic,
                                'list_order'  => $listOrder
                            ]);
                        }
                    }
                }
            }
            
        
            $output->writeln('模板更新成功');
        }
        
        
        
        
    }

    
    
  
}


