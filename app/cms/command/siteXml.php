<?php
// +----------------------------------------------------------------------
// | sitemap地图生成
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Env;
use think\facade\Db;
use app\admin\model\LinkModel;
use app\cms\model\CmsChannelModel;
use app\cms\model\CmsArticleModel;
use app\cms\model\CmsImageModel;
use cmf\controller\HomeBaseController;
use app\cms\model\CmsPageModel;

include CMF_ROOT.'extend/sitemap/sitemap.php';

class siteXml extends Command
{
    protected function configure()
    {
        $this->setName('siteXml')
        ->addArgument('domain', Option::VALUE_REQUIRED)//传递域名  http://a.com
        ;
    }

    protected function execute(Input $input, Output $output)
    {
        error_reporting(E_ERROR | E_PARSE);
       
        $domain = $input->getArgument('domain');
        if(empty($domain))
        {
            $output->writeln('域名错误');
            exit();
        }
        $dir = CMF_ROOT.'public/';
        // 启动事务
        Db::startTrans();
        try {
        //首页
        $url = [];
        $url[]  =$domain.PHP_EOL;
        //列表页
        $channel = CmsChannelModel::where('delete_time', 0)->select()->toArray();
        if (!empty($channel)) {
            foreach ($channel as $k => $v) {
                if(!empty($v['route']))
                {
                  $url[] = $domain.$v['route'].PHP_EOL;  
                }
            }
        }
        
         //文章页
        $article = Db::name('cms_article')
        ->alias('a')
        ->join('cms_channel b', 'a.channel_id = b.id')
        ->field('b.route,a.id')
        ->where(['a.status' => 1, 'a.delete_time' => 0])
        ->select()
        ->toArray();
        if (!empty($article)) {
            foreach ($article as $k => $v) {
                if(!empty($v['route']))
                {
                   $url[] = $domain.$v['route'].$v['id'].'.html'.PHP_EOL; 
                }
                
            }
        }
        
        //图集页
        $image = Db::name('cms_image')
        ->alias('a')
        ->join('cms_channel b', 'a.channel_id = b.id')
        ->field('b.route,a.id')
        ->where(['a.status' => 1, 'a.delete_time' => 0])
        ->select()
        ->toArray();
        if (!empty($image)) {
            foreach ($image as $k => $v) {
                if(!empty($v['route']))
                {
                   $url[] = $domain.$v['route'].$v['id'].'.html'.PHP_EOL; 
                }
            }
        }
        
   
        
        //生成txt
        file_put_contents(WEB_ROOT.'sitemap.txt',$url);
        //生成xml
        $sitemap = new \sitemap($domain,$dir);
        
        foreach ($url as $k => $v) {
            $url1 = str_replace([$domain,PHP_EOL], '', $v);
            $sitemap->addItem($url1, '1.0', 'daily', 'Today');
        }
        $sitemap->endSitemap();
            // 提交事务
            Db::commit();
        } catch (\Exception $th) {
            $output->writeln($th->getMessage());
            // 回滚事务
            Db::rollback();
        }
        $output->writeln('sitemap ok');
    }
}
