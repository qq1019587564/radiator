<?php

declare(strict_types=1);

namespace app\cms\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use app\form\model\FormModel;
use think\facade\Env;
class sync extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('sync');
    }

    protected function execute(Input $input, Output $output)
    {
        $FormModel = new FormModel();
        $stime = '1632585600';
        $etime = '1632818381';
        $data = $FormModel
            ->where('create_time', '>=', $stime)
            ->where('create_time', '<=', $etime)
            ->where('name', 'not like', '%测试%')
            ->where('pangu_msg', 'not like', '%百度推广服务网上申请成功%')
            // ->group('tel')
            ->order('create_time', 'desc')
            ->where('pangu_msg1','<>','')
            ->select()
            ->toArray();
            // dump(count($data));die;
        foreach ($data as $k => $v) {
            // dump($v);die;
            //提交到盘古
            $panguData = [
                "currProvId"  =>  $v['province_id'], //省份
                "currCityId"  =>  $v['city_id'], //城市id
                "companyName"  =>  $v['company'], //公司名
                "contactName"  =>  $v['name'], //联系人名
                "contactCell"  =>  $v['tel'], //电话
                "refer" => $v['refer'] ?? 1, //来源参数
            ];
            $panguMsg = safeHttp(Env::get('PANGU_URL'), $panguData, 'post', array(
                "Content-Type: application/x-www-form-urlencoded",
            ));

            $res = $FormModel->where('id', $v['id'])
            ->update(['pangu_msg1'=> $panguMsg]);
            
            dump($v['id'].'----'.$panguMsg);
            
        }
        die;
        
        
        // $output->writeln('www.');
        $dir = trim($input->getArgument('dir'));
        // dump($dir);die;
        $a = exec('sh /root/shell/file_rsync.sh '.$dir);
        // $a = exec('ls');
        dump($a);die;
        $output->writeln($a);
    }
}
