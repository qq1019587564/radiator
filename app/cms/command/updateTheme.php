<?php
// +----------------------------------------------------------------------
// | 织梦模板转过来后再做替换
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Env;
use think\facade\Db;
use app\admin\model\LinkModel;
use app\admin\model\ThemeModel;
use app\admin\model\ThemeFileModel;

class updateTheme extends Command
{
    protected function configure()
    {
        // 织梦模板转换规则
        $this->setName('updateTheme');
    }

    protected function execute(Input $input, Output $output)
    {
        // 启动事务
        // Db::startTrans();
        try {
            //清除之前的模板文件数据
            ThemeFileModel::where('theme', 'xst')->delete();

            $mubanDir = WEB_ROOT . 'themes/xst/cms';
            $files = scandir($mubanDir);
            foreach ($files as $k => $v) {
                $oldName = $mubanDir . '/' . $v;

                if ($v == '.' || $v == '..' || !is_file($oldName)) {
                    continue;
                }
                //把后缀htm换成html
                if (getExtension($v) == 'htm') {
                    //模板文件重命名
                    if($v =='index_m.htm')
                    {
                        $newName = $mubanDir . '/index_m.html';
                        rename($oldName, $newName);
                    }elseif ($v =='list_article_m.htm') {
                        $newName = $mubanDir . '/list_article_m.html';
                        rename($oldName, $newName);
                    }else{
                        $newName = $mubanDir . '/' . str_replace(['htm', 'article_', 'index_'], ['html', 'show_', 'page_'], $v);
                        rename($oldName, $newName);
                    }
                    

                    $jsonDir = str_replace('/cms', '', $mubanDir);
                    //生成对应的json文件
                    if (strpos($newName, 'show_') !== false) {
                        $jsonName = str_replace('.html', '.json', $newName);
                        $jsonContent = file_get_contents($jsonDir . '/public/show_detail.json');
                        file_put_contents($jsonName, $jsonContent);
                    }
                    if (strpos($newName, 'page_') !== false) {
                        $jsonName = str_replace('.html', '.json', $newName);
                        $jsonContent = file_get_contents($jsonDir . '/public/page.json');
                        file_put_contents($jsonName, $jsonContent);
                    }
                    if (strpos($newName, 'list_') !== false) {
                        $jsonName = str_replace('.html', '.json', $newName);
                        $jsonContent = file_get_contents($jsonDir . '/public/list_example.json');
                        file_put_contents($jsonName, $jsonContent);
                    }
                }
            }
            $files = scandir($mubanDir);
            $jsonDir = str_replace('/cms', '', $mubanDir);
            foreach ($files as $key => $value) {
                $oldName = $mubanDir . '/' . $value;
                if ($v == '.' || $v == '..' || !is_file($oldName)) {
                    continue;
                }
                //单页规则
                $page = ['dailijizhang.html', 'gongsibiangeng.html', 'gongsizhuxiao.html', 'shangbiaozhuce.html', 'zhucegongsi.html', 'lianxi.html', 'book.html', 'singlepage.html', 'danye.html', 'danye_m.html', 'liuyan.html', 'liuyan_m.html', 'contact.html', 'contact_m.html', 'message.html', 'message_m.html', 'map.html', 'map_m.html', 'join.html', 'join_m.html', 'about.html', 'about_m.html', 'service.html', 'service_m.html'];
                foreach ($page as $k => $v) {
                    if ($v == $value) {
                        $oldName = $mubanDir . '/' . $value;
                        $newName = $mubanDir . '/' . 'page_' . $value;
                        //判断文件是否已经存在
                        if (file_exists($newName)) {
                            $pageName = str_replace('.html', '', $value);
                            $newName = $mubanDir . '/' . 'page_' . $pageName . '1.html';
                        }
                        rename($oldName, $newName);
                        $jsonName = str_replace('.html', '.json', $newName);
                        $jsonContent = file_get_contents($jsonDir . '/public/page.json');
                        file_put_contents($jsonName, $jsonContent);
                    }
                }
            }

            //更新模板
            $this->updateThemeFiles('xst');
            // 提交事务
            // Db::commit();
        } catch (\Exception $th) {
            // $output->writeln($th->getMessage());
            // 回滚事务
            // Db::rollback();
            dump($th);
            die;
            // $output->writeln($th->getMessage());
            return;
        }


        $output->writeln('ok');
    }


    private function updateThemeFiles($theme, $suffix = 'html')
    {
        $dir                = WEB_ROOT . 'themes/' . $theme;
        $themeDir           = $dir;
        $tplFiles           = [];
        $root_dir_tpl_files = cmf_scan_dir("$dir/*.$suffix");
        foreach ($root_dir_tpl_files as $root_tpl_file) {
            $root_tpl_file           = "$dir/$root_tpl_file";
            $configFile              = preg_replace("/\.$suffix$/", '.json', $root_tpl_file);
            $root_tpl_file_no_suffix = preg_replace("/\.$suffix$/", '', $root_tpl_file);
            if (is_file($root_tpl_file) && file_exists_case($configFile)) {
                array_push($tplFiles, $root_tpl_file_no_suffix);
            }
        }
        $subDirs = cmf_sub_dirs($dir);
        foreach ($subDirs as $dir) {
            $subDirTplFiles = cmf_scan_dir("$dir/*.$suffix");
            foreach ($subDirTplFiles as $tplFile) {
                $tplFile         = "$dir/$tplFile";
                $configFile      = preg_replace("/\.$suffix$/", '.json', $tplFile);
                $tplFileNoSuffix = preg_replace("/\.$suffix$/", '', $tplFile);
                if (is_file($tplFile) && file_exists_case($configFile)) {
                    array_push($tplFiles, $tplFileNoSuffix);
                }
            }
        }
        foreach ($tplFiles as $tplFile) {
            $configFile = $tplFile . ".json";
            $file       = preg_replace('/^themes\/' . $theme . '\//', '', $tplFile);
            $file = str_replace([WEB_ROOT, 'themes/xst/'], '', $file);
            $file       = strtolower($file);
            $config     = json_decode(file_get_contents($configFile), true);
            if (empty($config)) {
                continue;
            }
            $findFile   = ThemeFileModel::where(['theme' => $theme, 'file' => $file])->find();
            $isPublic   = empty($config['is_public']) ? 0 : 1;
            $listOrder  = empty($config['order']) ? 0 : floatval($config['order']);
            $configMore = empty($config['more']) ? [] : $config['more'];
            $more       = $configMore;
            // dump($config);die;

            if (empty($findFile)) {

                ThemeFileModel::insert([
                    'theme'       => $theme,
                    'action'      => $config['action'],
                    'file'        => $file,
                    'name'        => $config['name'],
                    'more'        => json_encode($more),
                    'config_more' => json_encode($configMore),
                    'description' => $config['description'],
                    'is_public'   => $isPublic,
                    'list_order'  => $listOrder
                ]);
            } else { // 更新文件
                ThemeFileModel::where(['theme' => $theme, 'file' => $file])->update([
                    'theme'       => $theme,
                    'action'      => $config['action'],
                    'file'        => $file,
                    'name'        => $config['name'],
                    'config_more' => json_encode($configMore),
                    'description' => $config['description'],
                    'is_public'   => $isPublic,
                    'list_order'  => $listOrder
                ]);
            }
        }
    }
}
