<?php
// +----------------------------------------------------------------------
// | 转换dede_channeltype表数据
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Env;
use think\facade\Db;
use app\admin\model\LinkModel;
use app\cms\model\CmsChannelModel;
use app\cms\model\CmsFieldsBindModel;
use app\cms\model\CmsFieldsModel;
use app\cms\library\Alter;

class channeltype extends Command
{
    protected function configure()
    {
        // 内容模型字段表转换 TODO
        $this->setName('channeltype');
    }

    protected function execute(Input $input, Output $output)
    {
        //  $a = Db::name('cms_fields')->where('model_id', 2)->where('name','pagestyle')->delete();
        //       $b = Db::name('cms_fields')->where('model_id', 2)->where('name','cpbz')->delete();
        //       dump($a);
        //       dump($b);die;
        // 启动事务
        Db::startTrans();
        try {
             
            $dede = Db::connect('dede')
                ->table('dede_channeltype')
                ->where('nid', 'image')
                ->find();
            // ->toArray();
            if (!empty($dede)) {
                //查询图集模型的字段
                preg_match_all("/(?:itemname=\")(.*)(?:\" )/i", $dede['fieldset'], $nameData);
                preg_match_all("/(?:<field:)(.*)(?: itemname)/i", $dede['fieldset'], $fieldData);
            }
            //过滤掉body\图集
            $body = array_search('body', $fieldData[1]);
            unset($fieldData[1][$body]);
            unset($nameData[1][$body]);
            $imgurls = array_search('imgurls', $fieldData[1]);
            unset($fieldData[1][$imgurls]);
            unset($nameData[1][$imgurls]);

            if (!empty($nameData[1])) {
                foreach ($nameData[1] as $k => $v) {
                    $name = explode('"', $v);
                    $nameData[1][$k] = $name[0];
                }
            }
            
            // dump($fieldData);die;
            //查询数据库是否有这个字段
            $zid = Db::name('cms_fields')->where('model_id', 2)->column('name');
            
            $diff = array_diff($fieldData[1], $zid);
// dump($zid);die;

            $zidData = [];
            if (!empty($diff)) {
                foreach ($diff as $k3 => $v3) {
                    $zidData[$k3] = [
                        "model_id" => "2",
                        "diyform_id" => "0",
                        "title" => $nameData[1][array_search($v3, $fieldData[1])],
                        "name" => $v3,
                        "type" => "string",
                        "content" => "",
                        "decimals" => "0",
                        "bindtext" => "",
                        "bindtexts" => "",
                        "sqltreeid" => "",
                        "sqlname" => "",
                        "sqlval" => "",
                        "showoption" => "select",
                        "showrow" => "",
                        "is_thumb" => "0",
                        "is_save" => "0",
                        "img_width" => "",
                        "img_height" => "",
                        "defaultvalue" => "",
                        "required" => "required",
                        "rule" => "",
                        "msg" => "",
                        "length" => "1000",
                        "status" => "1",
                    ];
                    // dump($zidData);die;
                    //判断字段是否存在
                    $sql = "select count(*) as num from information_schema.columns where table_name = 'xst_cms_image' and column_name = '" . $zidData[$k3]['name'] . "'";
                    // dump($sql);
                    $count = Db::query($sql);
                    // dump($count);die;
                    if ($count[0]['num'] == 0) {
                        $sql = Alter::instance()
                            ->setTable('cms_image')
                            ->setName($zidData[$k3]['name'])
                            ->setLength($zidData[$k3]['length'])
                            ->setContent($zidData[$k3]['content'])
                            ->setDecimals($zidData[$k3]['decimals'])
                            ->setDefaultvalue($zidData[$k3]['defaultvalue'])
                            ->setComment($zidData[$k3]['title'])
                            ->setType($zidData[$k3]['type'])
                            ->getAddSql();
                        Db::execute($sql);
                    }
                }

                //把没有的字段添加模型字段表里去
                Db::name('cms_fields')->insertAll($zidData);


                //先查询模型已有的字段
                $fieldsData = [];
                $model_fields = CmsFieldsModel::where(['model_id' => 2, 'channel_id' => 0, 'status' => 1])->select()->toArray();

                foreach ($model_fields as $k11 => $v11) {
                    $fieldsData[$k11] = $v11;
                    $fieldsData[$k11]['field_id'] = $v11['id'];
                    $fieldsData[$k11]['channel_id'] = $channel_id ?? 1;
                    if (in_array($v11['name'], $fieldData[1])) {
                        $fieldsData[$k11]['title'] = $nameData[1][array_search($v11['name'], $fieldData[1])];
                    }
                    unset($fieldsData[$k11]['id']);
                }
                if (!empty($fieldsData)) {
                    $CmsFieldsBindModel = new CmsFieldsBindModel();
                    $CmsFieldsBindModel->saveAll($fieldsData);
                }
            }
            // 提交事务
            Db::commit();
        } catch (\Exception $th) {
            $output->writeln($th->getMessage());
            // 回滚事务
            Db::rollback();
        }
        $output->writeln('图集字段转换成功');
    }
}
