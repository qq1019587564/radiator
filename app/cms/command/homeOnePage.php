<?php
// +----------------------------------------------------------------------
// | 命令行生成首页
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Env;
use think\facade\Db;
use app\admin\model\LinkModel;
use cmf\controller\HomeBaseController;
use think\facade\Queue;
use think\facade\View;
use think\facade\Cache;
use app\cms\model\CmsChannelModel;
use app\cms\model\CmsArticleModel;
use app\cms\model\CmsImageModel;
use app\cms\service\CmsService;
use app\cms\model\CmsPageModel;

class homeOnePage extends Command
{
    protected function configure()
    {
        // 生成单页
        $this->setName('homeOnePage')
        ->addArgument('user_name', Option::VALUE_REQUIRED)//传递用户名  admin
        ->addOption('id', '-i',Option::VALUE_REQUIRED)//传递单页id 1
        ;
    }

    protected function execute(Input $input, Output $output)
    {
        error_reporting(E_ERROR | E_PARSE);
        cache('OnePageCreate',NULL);
        $startTime = time();
        $rootDir = WEB_ROOT.'/themes/xst/cms/';
        $userName = $input->getArgument('user_name')??'admin';
        $id = $input->getOption('id');
        if(empty($id))
        {
            $output->writeln('参数错误');
            exit();
        }
        //插入日志
        $logData = ['type'=>'生成一个单页','create_time'=>time(),'status'=>2,'user_name'=>$userName];
        $logId = Db::name('cms_create_log')->strict(false)->insertGetId($logData);
        
        //公共数据
        $side = commonData()['side'];
        $setting = commonData()['setting'];
        View::assign('side', $side ?? '');
        View::assign('setting', $setting??"");
        
        //域名
        $domain  = request()->domain();
        try {
            //生成单页 START
        $page = CmsPageModel::where('delete_time', 0)->where('id',$id)->select()->toArray();
        if (!empty($page)) {
            foreach ($page as $k => $v) {
                $channel = CmsChannelModel::where('id', $v['channel_id'])->where('delete_time', 0)->where('type', 2)->find();
                if (!empty($channel)) {
                    if(!file_exists($rootDir. $channel['page_tpl'].'.html'))
                    {
                        continue;
                    }
                    View::assign('channel', $channel);
                    $page_tpl = $channel['page_tpl'];
                    View::assign('page', $v);
                    //模板地址
                    $template = root_path() . 'public' . $channel['route'] . 'index.html';
                    $templateDir = root_path() . 'public' . $channel['route'];
                    //跳转的路径
                    $templateUrl = $domain . '/public' . $channel['route'] . 'index.html';
                    //判断静态界面是否存在
                    beforeBuild($templateUrl, $templateDir, $template);
                    $html = View::fetch($rootDir . $page_tpl.'.html');
                    //跳转手机端js
                    if(web_type())
                    {
                        $js = '<script>var os=function(){var a=navigator.userAgent,b=/(?:Windows Phone)/.test(a),c=/(?:SymbianOS)/.test(a)||b,d=/(?:Android)/.test(a),e=/(?:Firefox)/.test(a),g=(/(?:Chrome|CriOS)/.test(a),/(?:iPad|PlayBook)/.test(a)||d&&!/(?:Mobile)/.test(a)||e&&/(?:Tablet)/.test(a)),h=/(?:iPhone)/.test(a)&&!g,i=!h&&!d&&!c;return{isTablet:g,isPhone:h,isAndroid:d,isPc:i};}();(os.isAndroid||os.isPhone)&&(href=window.location.href.substring(9),window.location.href=window.location.protocol+"//"+window.location.host+"/m"+href.substring(href.indexOf("/"),href.length));</script></head>';
                        $html = str_replace(['__TMPL__','http://localhost','</head>'],['/themes/xst','',$js],$html);
                    }else{
                        $html = str_replace(['__TMPL__','http://localhost'],['/themes/xst',''],$html);
                    }
                    //生成静态界面
                    $sendData = ['url' => $templateUrl, 'temp' => $template, 'html' => $html];
                    $this->push('page', $sendData);
                } else {
                    continue;
                }
            }
        }
            $diff_time = $this->secToTime(time()- $startTime);
            $updateLogData = ['type'=>'生成'.$page['title'].'单页','diff_time'=>$diff_time,'update_time'=>time(),'total'=>cache('OnePageCreate'),'status'=>1];
            $logId = Db::name('cms_create_log')->where('id',$logId)->update($updateLogData);
            $output->writeln('生成单页成功');
        } catch (\Exception $e) {
            $diff_time = $this->secToTime(time()- $startTime);
            $updateLogData = ['type'=>'生成'.$page['title'].'单页','diff_time'=>$diff_time,'update_time'=>time(),'total'=>cache('OnePageCreate'),'status'=>0,'reason'=>$e->getMessage()];
            $logId = Db::name('cms_create_log')->where('id',$logId)->update($updateLogData);
            $output->writeln($e->getMessage());
        }
        
    }
    
    /**
     *      把秒数转换为时分秒的格式
     *      @param Int $times 时间，单位 秒
     *      @return String
     */
    public function secToTime($times){
            $result = '00:00:00';
            if ($times>0) {
                    $hour = floor($times/3600);
                    $minute = floor(($times-3600 * $hour)/60);
                    $second = floor((($times-3600 * $hour) - 60 * $minute) % 60);
                    $result = $hour.':'.$minute.':'.$second;
            }
            return $result;
    }    
    
    
    /**
     * 队列推送
     *
     * @param string $type  任务类型
     * @param array $data 队列数据
     * @return void
     */
    public function push($type, $data)
    {
        Cache::inc('OnePageCreate');
        //当轮到该任务时，系统将生成一个该类的实例，并默认调用其 fire 方法
        $jobHandlerClassName = 'app\queue\controller\CreateController@' . $type;
        //将该任务推送到消息队列，等待对应的消费者去执行
        $isPushed = Queue::push($jobHandlerClassName, $data);
    }
}
