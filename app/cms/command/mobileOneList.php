<?php
// +----------------------------------------------------------------------
// | 命令行生成手机端单个列表页
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Env;
use think\facade\Db;
use app\admin\model\LinkModel;
use cmf\controller\HomeBaseController;
use think\facade\Queue;
use think\facade\View;
use think\facade\Cache;
use app\cms\model\CmsChannelModel;
use app\cms\model\CmsArticleModel;
use app\cms\model\CmsImageModel;
use app\cms\service\CmsService;
use app\cms\model\CmsPageModel;

class mobileOneList extends Command
{
    protected function configure()
    {
        // 生成列表页
        $this->setName('mobileOneList')
        ->addArgument('domain_name', Option::VALUE_REQUIRED)//传递域名  http://a.com
        ->addOption('user_name', '-u',Option::VALUE_REQUIRED)//传递用户名  admin
        ->addOption('id', '-i',Option::VALUE_REQUIRED)//传递栏目id 1
        // ->addArgument('pici', Argument::OPTIONAL)//日志批次
        ;
    }

    protected function execute(Input $input, Output $output)
    {
        error_reporting(E_ERROR | E_PARSE);
        cache('MobileOneListCreate',NULL);
        $domainName = $input->getArgument('domain_name');
        if(empty($domainName))
        {
            $output->writeln('域名错误');
            exit();
        }
        $id = $input->getOption('id');
        if(empty($id))
        {
            $output->writeln('参数错误');
            exit();
        }
        
        $userName = $input->getOption('user_name')??'admin';
        
        $startTime = time();
        $output->writeln('手机端列表页开始生成'.date('Y-m-d H:i:s',time()));
        try {
        //插入日志
        $logData = ['type'=>'生成手机端单个列表页','create_time'=>time(),'status'=>2,'user_name'=>$userName];
        $logId = Db::name('cms_create_log')->strict(false)->insertGetId($logData);
        $domainName = trim($input->getArgument('domain_name'));
        
        $rootDir = WEB_ROOT.'/themes/xst/cms/';
     
        //公共数据
        $side = commonData()['side'];
        $setting = commonData()['setting'];
        View::assign('side', $side ?? '');
        View::assign('setting', $setting??"");
        
        //域名
        $domain  = request()->domain();
        
        //生成列表页START
        $channel = CmsChannelModel::where('type', 1)->where('id',$id)->where('delete_time', 0)->select()->toArray();
        if (!empty($channel)) {
            foreach ($channel as $k => $v) {
                $list_tpl = $rootDir. $v['list_tpl'].'_m.html';
                if(!file_exists($list_tpl))
                {
                    $list_tpl = $rootDir.'list_default_m.html';
                    if(!file_exists($list_tpl))
                    {
                        continue;
                    }
                }
                //定义栏目变量
                View::assign('channel', $v);
                //模板地址
                $template = root_path() . 'public/m' . $v['route'] . 'index.html';
                $templateDir = root_path() . 'public/m' . $v['route'];
                //跳转的路径
                $templateUrl = $domain . '/public/m' . $v['route'] . 'index.html';
                //判断静态界面是否存在
                $html = View::fetch( $list_tpl);
              
                
                
                $aaurl =  $v['route'] . '?page=';
                preg_match_all('/href=\"http:\/\/localhost?.*?page=([0-9]*)\"/', $html, $match);
                
                if (!empty($match[1])) {
                    $page_href = array_unique($match[1]);
                    $max = max($page_href);
                    for ($ii = 1; $ii <= $max; $ii++) {
                        $html = '';
                        $aaurl = $domainName . '/m/list/index?id=' . $v['id'] . '&page=' . $ii;
                        $tempName = 'list_' . $v['id'] . '_' . $ii . '.html';
                        $template1 = root_path() . 'public/m' . $v['route'] . $tempName;
                        $html = cmf_curl_get($aaurl);
                        $html = str_replace(['__TMPL__','http://localhost'],['/themes/xst',''],$html);
                        // dump($html);die;
                        for ($iii = 1; $iii <= $max; $iii++) {
                            $fenyeNewname = 'list_' . $v['id'] . '_' . $iii . '.html"';
                            //列表分页静态访问
                            $dongtaiUrl = "/cms/list/index?id=" . $v['id'] . "&amp;";
                            $dongtaiUrl2 = "/m/list/index?id=" . $v['id'] . "&amp;";
                            $html = str_replace([$dongtaiUrl,$dongtaiUrl2, '/index.php', 'page=' . $iii . '"'], ['/m'.$v['route'],'/m'.$v['route'], '', $fenyeNewname], $html);
                        }
                        beforeBuild($templateUrl, $templateDir, $template1);
                        
                        $sendData = ['url' => $templateUrl, 'temp' => $template1, 'html' => $html];
                        $this->push('list', $sendData);
                        //生成有分页的列表页的index.html
                        if ($ii == 1) {
                            beforeBuild($templateUrl, $templateDir, $template);
                            $sendData = ['url' => $templateUrl, 'temp' => $template, 'html' => $html];
                            $this->push('list', $sendData);
                        }
                    }
                } else {
                    //没有分页的
                    $html = str_replace(['__TMPL__','http://localhost'],['/themes/xst',''],$html);
                    $sendData = ['url' => $templateUrl, 'temp' => $template, 'html' => $html];
                    beforeBuild($templateUrl, $templateDir, $template);
                    $this->push('list', $sendData);
                }
            }
        }
        $diff_time = $this->secToTime(time()- $startTime);
         $updateLogData = ['type'=>'生成'.$channel['name'].'列表页','diff_time'=>$diff_time,'update_time'=>time(),'total'=>cache('MobileOneListCreate'),'status'=>1];
        $logId = Db::name('cms_create_log')->where('id',$logId)->update($updateLogData);
        //生成列表页END
        $output->writeln('手机端列表页生成页成功'.date('Y-m-d H:i:s',time()).'——'.cache('MobileOneListCreate'));
        } catch (\Exception $e) {
            $diff_time = $this->secToTime(time()- $startTime);
            $updateLogData = ['type'=>'生成'.$channel['name'].'列表页','diff_time'=>$diff_time,'update_time'=>time(),'total'=>cache('MobileOneListCreate'),'status'=>0,'reason'=>$e->getMessage()];
            $logId = Db::name('cms_create_log')->where('id',$logId)->update($updateLogData);
            $output->writeln('手机端列表页：'.$e->getMessage());
        }
    }
    
    /**
     *      把秒数转换为时分秒的格式
     *      @param Int $times 时间，单位 秒
     *      @return String
     */
    public function secToTime($times){
            $result = '00:00:00';
            if ($times>0) {
                    $hour = floor($times/3600);
                    $minute = floor(($times-3600 * $hour)/60);
                    $second = floor((($times-3600 * $hour) - 60 * $minute) % 60);
                    $result = $hour.':'.$minute.':'.$second;
            }
            return $result;
    }
        /**
     * 队列推送
     *
     * @param string $type  任务类型
     * @param array $data 队列数据
     * @return void
     */
    public function push($type, $data)
    {
        Cache::inc('MobileOneListCreate');
        //当轮到该任务时，系统将生成一个该类的实例，并默认调用其 fire 方法
        $jobHandlerClassName = 'app\queue\controller\CreateController@' . $type;
        //将该任务推送到消息队列，等待对应的消费者去执行
        $isPushed = Queue::push($jobHandlerClassName, $data);
    }
}
