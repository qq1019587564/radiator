<?php
// +----------------------------------------------------------------------
// | 命令行生成列表页
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Env;
use think\facade\Db;
use app\admin\model\LinkModel;
use cmf\controller\HomeBaseController;
use think\facade\Queue;
use think\facade\View;
use think\facade\Cache;
use app\cms\model\CmsChannelModel;
use app\cms\model\CmsArticleModel;
use app\cms\model\CmsImageModel;
use app\cms\service\CmsService;
use app\cms\model\CmsPageModel;

class homeList extends Command
{
    protected function configure()
    {
        // 生成列表页
        $this->setName('homeList')
        ->addArgument('domain_name', Option::VALUE_REQUIRED)//传递域名  http://a.com
        ->addOption('user_name', '-u',Option::VALUE_REQUIRED)//传递用户名  admin
        // ->addArgument('pici', Argument::OPTIONAL)//日志批次
        ;
    }

    protected function execute(Input $input, Output $output)
    {
        error_reporting(E_ERROR | E_PARSE);
        cache('ListCreate',NULL);
        $domainName = $input->getArgument('domain_name');
        if(empty($domainName))
        {
            $output->writeln('域名错误');
            exit();
        }
        $userName = $input->getOption('user_name')??'admin';
        $isMobile = web_type();
        $startTime = time();
        $output->writeln('pc列表页开始生成'.date('Y-m-d H:i:s',time()));
        try {
        //插入日志
        $logData = ['type'=>'生成列表页','create_time'=>time(),'status'=>2,'user_name'=>$userName];
        $logId = Db::name('cms_create_log')->strict(false)->insertGetId($logData);
        $domainName = trim($input->getArgument('domain_name'));
        
        $rootDir = WEB_ROOT.'/themes/xst/cms/';
     
        //公共数据
        $side = commonData()['side'];
        $setting = commonData()['setting'];
        View::assign('side', $side ?? '');
        View::assign('setting', $setting??"");
        
        //域名
        $domain  = request()->domain();
        
        //生成列表页START
        $channel = CmsChannelModel::where('type', 1)->where('delete_time', 0)->select()->toArray();
        if (!empty($channel)) {
            foreach ($channel as $k => $v) {
                if(!file_exists($rootDir. $v['list_tpl'].'.html'))
                {
                    continue;
                }
                //定义栏目变量
                View::assign('channel', $v);
                //模板地址
                $template = root_path() . 'public' . $v['route'] . 'index.html';
                $templateDir = root_path() . 'public' . $v['route'];
                //跳转的路径
                $templateUrl = $domain . '/public' . $v['route'] . 'index.html';
                //判断静态界面是否存在
                $html = View::fetch( $rootDir. $v['list_tpl'].'.html');
                
                $aaurl =  $v['route'] . '?page=';
                preg_match_all('/href=\"http:\/\/localhost?.*?page=([0-9]*)\"/', $html, $match);
                
                if (!empty($match[1])) {
                    $page_href = array_unique($match[1]);
                    $max = max($page_href);
                    for ($ii = 1; $ii <= $max; $ii++) {
                        $html = '';
                        $aaurl = $domainName . '/cms/list/index?id=' . $v['id'] . '&page=' . $ii;
                        $tempName = 'list_' . $v['id'] . '_' . $ii . '.html';
                        $template1 = root_path() . 'public' . $v['route'] . $tempName;
                        $html = cmf_curl_get($aaurl);
                        //跳转手机端js
                        if($isMobile)
                        {
                            $js = '<script>var os=function(){var a=navigator.userAgent,b=/(?:Windows Phone)/.test(a),c=/(?:SymbianOS)/.test(a)||b,d=/(?:Android)/.test(a),e=/(?:Firefox)/.test(a),g=(/(?:Chrome|CriOS)/.test(a),/(?:iPad|PlayBook)/.test(a)||d&&!/(?:Mobile)/.test(a)||e&&/(?:Tablet)/.test(a)),h=/(?:iPhone)/.test(a)&&!g,i=!h&&!d&&!c;return{isTablet:g,isPhone:h,isAndroid:d,isPc:i};}();(os.isAndroid||os.isPhone)&&(href=window.location.href.substring(9),window.location.href=window.location.protocol+"//"+window.location.host+"/m"+href.substring(href.indexOf("/"),href.length));</script></head>';
                           
                            $html = str_replace(['__TMPL__','http://localhost','</head>'],['/themes/xst','',$js],$html);
                        }else{
                            $html = str_replace(['__TMPL__','http://localhost'],['/themes/xst',''],$html);
                        }
                        //无法传递分页，TODO？
                        // View::assign('page',$ii);
                        // $html = View::fetch($rootDir. $v['list_tpl'].'.html');
                        for ($iii = 1; $iii <= $max; $iii++) {
                            $fenyeNewname = 'list_' . $v['id'] . '_' . $iii . '.html"';
                            //列表分页动态访问
                            //$dongtaiUrl = "http://localhost?";
                            // $html = str_replace([$dongtaiUrl, '/index.php', '?page=' . $iii . '"'], [$v['route'], '', $fenyeNewname], $html);
                            //列表分页静态访问
                            $dongtaiUrl = "/cms/list/index?id=" . $v['id'] . "&amp;";
                            $html = str_replace([$dongtaiUrl, '/index.php', 'page=' . $iii . '"'], [$v['route'], '', $fenyeNewname], $html);
                        }
                        beforeBuild($templateUrl, $templateDir, $template1);
                        
                        $sendData = ['url' => $templateUrl, 'temp' => $template1, 'html' => $html];
                        $this->push('list', $sendData);
                        //生成有分页的列表页的index.html
                        if ($ii == 1) {
                            beforeBuild($templateUrl, $templateDir, $template);
                            $sendData = ['url' => $templateUrl, 'temp' => $template, 'html' => $html];
                            $this->push('list', $sendData);
                        }
                    }
                } else {
                    if($isMobile)
                    {
                        $js = '<script>if ((navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i))) {window.location.href="/m'.$v['route'].'";}</script></head>';
                        $html = str_replace(['__TMPL__','http://localhost','</head>'],['/themes/xst','',$js],$html);
                    }else{
                        $html = str_replace(['__TMPL__','http://localhost'],['/themes/xst',''],$html);
                    }
                    //没有分页的
                    $sendData = ['url' => $templateUrl, 'temp' => $template, 'html' => $html];
                    beforeBuild($templateUrl, $templateDir, $template);
                    $this->push('list', $sendData);
                }
            }
        }
        $diff_time = $this->secToTime(time()- $startTime);
        $updateLogData = ['diff_time'=>$diff_time,'update_time'=>time(),'total'=>cache('ListCreate'),'status'=>1];
        $logId = Db::name('cms_create_log')->where('id',$logId)->update($updateLogData);
        //生成列表页END
        $output->writeln('pc列表页生成页成功'.date('Y-m-d H:i:s',time()).'——'.cache('ListCreate'));
        } catch (\Exception $e) {
            $diff_time = $this->secToTime(time()- $startTime);
            $updateLogData = ['diff_time'=>$diff_time,'update_time'=>time(),'total'=>cache('ListCreate'),'status'=>0,'reason'=>$e->getMessage()];
            $logId = Db::name('cms_create_log')->where('id',$logId)->update($updateLogData);
            $output->writeln('pc列表页：'.$e->getMessage());
        }
    }
    
    /**
     *      把秒数转换为时分秒的格式
     *      @param Int $times 时间，单位 秒
     *      @return String
     */
    public function secToTime($times){
            $result = '00:00:00';
            if ($times>0) {
                    $hour = floor($times/3600);
                    $minute = floor(($times-3600 * $hour)/60);
                    $second = floor((($times-3600 * $hour) - 60 * $minute) % 60);
                    $result = $hour.':'.$minute.':'.$second;
            }
            return $result;
    }
        /**
     * 队列推送
     *
     * @param string $type  任务类型
     * @param array $data 队列数据
     * @return void
     */
    public function push($type, $data)
    {
        Cache::inc('ListCreate');
        //当轮到该任务时，系统将生成一个该类的实例，并默认调用其 fire 方法
        $jobHandlerClassName = 'app\queue\controller\CreateController@' . $type;
        //将该任务推送到消息队列，等待对应的消费者去执行
        $isPushed = Queue::push($jobHandlerClassName, $data);
    }
}
