<?php
// +----------------------------------------------------------------------
// | 表模型
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\model;

use think\Model;
use app\cms\model\CmsTagModel;

/**
 * @mixin \think\Model
 */
class CmsCaseModel extends Model
{
    /**
     * 模型名称
     * @var string
     */
    protected $name = 'cms_case';

    //开启自动时间戳
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updataTime = 'update_time';

    /**
     * published_time 自动完成
     * @param $value
     * @return false|int
     */
    public function setPublishedTimeAttr($value)
    {
        if (empty($value)) {
            return time();
        }
        return strtotime($value);
    }

    /**
     * content 自动转化
     * @param $value
     * @return string
     */
    public function getContentAttr($value)
    {
        if (!empty($value)) {
            return cmf_replace_content_file_url(htmlspecialchars_decode($value));
        }
    }

    public function setContentAttr($value)
    {
        return htmlspecialchars(cmf_replace_content_file_url(htmlspecialchars_decode($value), true));
    }

    public function setMoreAttr($value)
    {
        return json_encode($value);
    }

    public function getMoreAttr($value)
    {
        return json_decode($value, true);
    }

    // public function getTagIdsAttr($value)
    // {
    //     $tags = CmsTagModel::where('id', 'in', $value)->column('title');
    //     $tags = implode('|', $tags);
    //     return $tags;
    // }

    /**
     * 添加
     */
    public function add($data)
    {
        $result = true;
        if (!empty($data['thumbnail'])) {
            $data['thumbnail'] = cmf_asset_relative_url($data['thumbnail']);
        }
        $data['tag_ids'] = tag($data['tag_ids'],2);

        self::startTrans();
        try {
            $this->save($data);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            $result = $e->getMessage();
            $result = false;
        }
        return $result;
    }

    /**
     * 编辑
     */
    public function edit($data)
    {
        $result = true;
        if (!empty($data['thumbnail'])) {
            $data['thumbnail'] = cmf_asset_relative_url($data['thumbnail']);
        }
        $data['tag_ids'] = tag($data['tag_ids'],2);

        self::startTrans();
        try {
            if (!isset($data['status'])) {
                $data['status'] = 0;
            }
            if (!isset($data['recommended'])) {
                $data['recommended'] = 0;
            }
            if (!isset($data['is_index'])) {
                $data['is_index'] = 0;
            }
            $this->update($data);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            $result = $e->getMessage();
            dump($result);
            die;
            $result = false;
        }
        return $result;
    }
}
