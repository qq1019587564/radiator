<?php
// +----------------------------------------------------------------------
// | 栏目表模型
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\model;

use think\Model;
use app\admin\model\RouteModel;
use app\cms\model\CmsPageModel;
use think\db\Query;
use Overtrue\Pinyin\Pinyin;
use app\cms\model\CmsFieldsModel;
use app\cms\model\CmsFieldsBindModel;
use think\facade\Db;
use app\cms\model\CmsModelModel;

/**
 * @mixin \think\Model
 */
class CmsChannelModel extends Model
{
    /**
     * 模型名称
     * @var string
     */
    protected $name = 'cms_channel';

    //开启自动时间戳
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updataTime = 'update_time';



    /**
     * 关联模型表
     */
    public function model()
    {
        // return $this->belongsTo('CmsModelModel', 'model_id'); 
        return $this->hasOne(CmsModelModel::class, 'id', 'model_id')->bind([
            "model_name" => "name"
        ]);
    }

    /**
     * 栏目添加处理
     */
    public function add($data)
    {
        $result = true;
        $table_name = CmsModelModel::where('id', $data['model_id'])->value('table');
        $table_name = str_replace('cms', 'admin', $table_name);
        self::startTrans();
        try {
            if (!empty($data['thumbnail'])) {
                $data['thumbnail'] = cmf_asset_relative_url($data['thumbnail']);
            }
            $type = Check_stringType($data['name']);

            if (empty($data['route'])) {
                $pinyin = new Pinyin();
                //1全英文2中文3中英
                switch ($type) {
                    case '1':
                        $data['route'] = $data['name'];
                        break;
                    case '2':
                        $data['route'] = $pinyin->abbr($data['name'], PINYIN_KEEP_ENGLISH);
                        break;
                    case '3':
                        $data['route'] = $pinyin->abbr($data['name'], PINYIN_KEEP_ENGLISH);
                        break;
                }
            }
            $pageId = 0;
            //判断是否有斜杠/
            if (substr($data['route'], 0, 1) != '/') {
                $data['route'] = '/' . $data['route'];
            }
            //判断最后一个是否有斜杠/
            if (substr($data['route'], -1) != '/') {
                $data['route'] =  $data['route'] . '/';
            }
            $this->save($data);
            $id = $this->id;

            //添加单页数据
            if ($data['type'] == 2) {
                $page['id'] = $id;
                $page['title'] = $data['name'];
                $page['channel_id'] = $id;
                $page['user_id'] = $data['user_id'];
                $CmsPageModel = new CmsPageModel();
                $CmsPageModel->save($page);
                $pageId = $CmsPageModel->id;
                $admin_url = '/cms/' . $table_name . '/index/channel_id/' . $id . '/model_id/' . $data['model_id'] . '/id/' . $id;
            } else {
                $admin_url = '/cms/' . $table_name . '/index/channel_id/' . $id . '/model_id/' . $data['model_id'];
            }

            $update = ['id' => $id, 'page_id' => $pageId, 'list_order' => $id, 'admin_url' => $admin_url];

            //添加栏目 把模型已有的字段都添加进去
            //先查询模型已有的字段
            $model_fields = CmsFieldsModel::where(['model_id' => $data['model_id'], 'channel_id' => 0, 'status' => 1])->select()->toArray();
            foreach ($model_fields as $k1 => $v1) {
                $fieldsData[$k1] = $v1;
                $fieldsData[$k1]['field_id'] = $v1['id'];
                $fieldsData[$k1]['update_time'] = time();
                $fieldsData[$k1]['create_time'] = time();
                $fieldsData[$k1]['channel_id'] = $id;
                $fieldsData[$k1]['list_order'] = $v1['list_order'];
                unset($fieldsData[$k1]['id']);
            }
            if (!empty($fieldsData)) {
                $CmsFieldsBindModel = new CmsFieldsBindModel();
                $CmsFieldsBindModel->saveAll($fieldsData);
            }


            //设置伪静态
            $routeModel = new RouteModel();
            //判断是否有同名的栏目，有则添加随机字符串
            $route = RouteModel::where('url', $data['route'])->where('status', 1)->where('channel_id', '<>', $id)->count();
            if ($route >= 1) {
                $str = cmf_random_string(5) . '/';
                $data['route'] = rtrim($data['route'], '/') . $str;
            }
            $update['route'] = $data['route'];

            $this->update($update);
            $level = explode('/', $data['route']);
            $list_order = 100 - count($level);


            if (!empty($data['route']) && !empty($id)) {
                switch ($data['type']) {
                    case '1':
                        $routeModel->setRoute($data['route'], 'cms/List/index', ['id' => $id], 2, $list_order, $id);
                        if ($data['model_id'] == 1) {
                            $routeModel->setRoute($data['route'] . ':id', 'cms/Article/index', ['cid' => $id], 2, $list_order, $id);
                        } else {
                            $routeModel->setRoute($data['route'] . ':id', 'cms/Image/index', ['cid' => $id], 2, $list_order, $id);
                        }
                        break;
                    default:
                        $routeModel->setRoute($data['route'], 'cms/Page/index', ['id' => $pageId], 2, $list_order, $id);
                        break;
                }
            }
            $routeModel->getRoutes(true);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            $result = $e->getMessage();
            $result = false;
        }

        return $result;
    }

    /**
     * 栏目编辑处理
     */
    public function edit($data)
    {
        $result = true;

        $id = intval($data['id']);

        $oldChannel = $this->where('id', $id)->find();
        $pageId = CmsPageModel::where('channel_id', $id)->value('id');
        $data['model_id'] = $oldChannel['model_id'];
        $data['type'] = $oldChannel['type'];
        $table_name = CmsModelModel::where('id', $data['model_id'])->value('table');
        $table_name = str_replace('cms', 'admin', $table_name);
        self::startTrans();
        try {
            if ($oldChannel['route'] != $data['route']) {
                $routeModel = new RouteModel();
                //判断是否有同名的栏目，有则添加随机字符串
                $route = RouteModel::where('url', $data['route'])->where('status', 1)->where('channel_id', '<>', $id)->count();
                if ($route >= 1) {
                    $str = cmf_random_string(5) . '/';
                    $data['route'] = rtrim($data['route'], '/') . $str;
                }

                //判断是否有斜杠/
                if (substr($data['route'], 0, 1) != '/') {
                    $data['route'] = '/' . $data['route'];
                }
                //判断最后一个是否有斜杠/
                if (substr($data['route'], -1) != '/') {
                    $data['route'] =  $data['route'] . '/';
                }
                $level = explode('/', $data['route']);
                $list_order = 100 - count($level);
                switch ($oldChannel['type']) {
                    case '1':
                        $routeModel->setRoute($data['route'], 'cms/List/index', ['id' => $id], 2, $list_order);
                        if ($data['model_id'] == 1) {
                            $routeModel->setRoute($data['route'] . ':id', 'cms/Article/index', ['cid' => $id], 2, $list_order, $id);
                        } else {
                            $routeModel->setRoute($data['route'] . ':id', 'cms/Image/index', ['cid' => $id], 2, $list_order, $id);
                        }
                        break;
                    default:
                        $routeModel->setRoute($data['route'], 'cms/Page/index', ['id' => $pageId], 2, $list_order);
                        break;
                }
                //删除原有的伪静态
                $routeModel->where('url', $oldChannel['route'])->where('channel_id', $oldChannel['id'])->delete();
                $routeModel->getRoutes(true);
            }
            //栏目改成单页的
            if ($data['type'] != $oldChannel['type'] && $data['type'] == 2) {
                if (empty(Db::name('cms_page')->where(['channel_id' => $id, 'delete_time' => 0])->count())) {
                    $page['id'] = $id;
                    $page['title'] = $data['name'];
                    $page['channel_id'] = $id;
                    $page['user_id'] = $data['user_id'];
                    $CmsPageModel = new CmsPageModel();
                    $CmsPageModel->save($page);
                    $pageId = $CmsPageModel->id;
                } else {
                    $pageId = $data['page_id'];
                }
                $admin_url = '/cms/' . $table_name . '/index/channel_id/' . $id . '/model_id/' . $data['model_id'] . '/id/' . $id;
            } else {
                $admin_url = '/cms/' . $table_name . '/index/channel_id/' . $id . '/model_id/' . $data['model_id'];
            }
            $data['admin_url'] = $admin_url;
            $this->update($data);

            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            $result = $e->getMessage();
            $result = false;
        }

        return $result;
    }
}
