<?php
// +----------------------------------------------------------------------
// | 单页表模型
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class CmsPageModel extends Model
{
    /**
     * 模型名称
     * @var string
     */
    protected $name = 'cms_page';

    //开启自动时间戳
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updataTime = 'update_time';

    
    /**
     * content 自动转化
     * @param $value
     * @return string
     */
    public function getContentAttr($value)
    {
        if (!empty($value)) {
            return cmf_replace_content_file_url(htmlspecialchars_decode($value));
        }
    }


    /**
     * 编辑单页
     */
    public function edit($data)
    {
        $result = true;
        if (!empty($data['thumbnail'])) {
            $data['thumbnail'] = cmf_asset_relative_url($data['thumbnail']);
        }
        self::startTrans();
        try {
            $this->update($data);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            $result = $e->getMessage();
            $result = false;
        }
        return $result;
    }
}
