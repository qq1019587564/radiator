<?php
// +----------------------------------------------------------------------
// | {$name_tpl}表模型
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\model;

use think\Model;
use think\db\Query;


/**
 * @mixin \think\Model
 */
class {$model_tpl} extends Model
{
    /**
     * 模型名称
     * @var string
     */
    protected $name = '{$table}';

    //开启自动时间戳
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updataTime = 'update_time';

    /**
     * 添加文章
     */
    public function add($data)
    {
        $result = true;
        self::startTrans();
        try {
            $this->save($data);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            $result = $e->getMessage();
            $result = false;
        }
        return $result;
    }

    /**
     * 编辑
     */
    public function edit($data)
    {
        $result = true;
        self::startTrans();
        try {
            $this->update($data);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            $result = $e->getMessage();
            $result = false;
        }
        return $result;
    }


}
