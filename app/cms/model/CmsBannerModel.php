<?php
// +----------------------------------------------------------------------
// | banner表模型
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class CmsBannerModel extends Model
{
    /**
     * 模型名称
     * @var string
     */
    protected $name = 'cms_banner';

    //开启自动时间戳
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updataTime = 'update_time';


    /**
     * 添加banner
     */
    public function add($data)
    {
        $result = true;
        if (!empty($data['pc_image'])) {
            $data['pc_image'] = cmf_asset_relative_url($data['pc_image']);
        }
        if (!empty($data['mb_image'])) {
            $data['mb_image'] = cmf_asset_relative_url($data['mb_image']);
        }
        self::startTrans();
        try {
            $this->save($data);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            $result = $e->getMessage();
            dump($result);die;
            $result = false;
        }
        return $result;
    }


    /**
     * 编辑单页
     */
    public function edit($data)
    {
        $result = true;
        if (!empty($data['pc_image'])) {
            $data['pc_image'] = cmf_asset_relative_url($data['pc_image']);
        }
        if (!empty($data['mb_image'])) {
            $data['mb_image'] = cmf_asset_relative_url($data['mb_image']);
        }
        self::startTrans();
        try {
            $this->update($data);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            $result = $e->getMessage();
            $result = false;
        }
        return $result;
    }
}
