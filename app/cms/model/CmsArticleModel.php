<?php
// +----------------------------------------------------------------------
// | 文章表模型
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\model;

use think\Model;
use think\db\Query;


/**
 * @mixin \think\Model
 */
class CmsArticleModel extends Model
{
    /**
     * 模型名称
     * @var string
     */
    protected $name = 'cms_article';

    //开启自动时间戳
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updataTime = 'update_time';

    /**
     * published_time 自动完成
     * @param $value
     * @return false|int
     */
    public function setPublishedTimeAttr($value)
    {
        if (empty($value)) {
            return time();
        }
        return strtotime($value);
    }

    /**
     * content 自动转化
     * @param $value
     * @return string
     */
    public function getContentAttr($value)
    {
        if (!empty($value)) {
            return cmf_replace_content_file_url(htmlspecialchars_decode($value));
        }
    }

    public function setContentAttr($value)
    {
        return htmlspecialchars(cmf_replace_content_file_url(htmlspecialchars_decode($value), true));
    }



    /**
     * 添加文章
     */
    public function add($data)
    {
        $result = true;
        if (!empty($data['thumbnail'])) {
            $data['thumbnail'] = cmf_asset_relative_url($data['thumbnail']);
        }
        $data['tag_ids'] = tag($data['tag_ids']);
        self::startTrans();
        try {
            $this->save($data);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            $result = $e->getMessage();
            $result = false;
        }
        return $result;
    }

    /**
     * 编辑文章
     */
    public function edit($data)
    {
        $result = true;
        if (!empty($data['thumbnail'])) {
            $data['thumbnail'] = cmf_asset_relative_url($data['thumbnail']);
        }
        //判断标签是否需要更新
        $data['tag_ids'] = tag($data['tag_ids']);

        self::startTrans();
        try {
            if (!isset($data['status'])) {
                $data['status'] = 0;
            }
            if (!isset($data['is_top'])) {
                $data['is_top'] = 0;
            }
            if (!isset($data['recommended'])) {
                $data['recommended'] = 0;
            }
            $this->update($data);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            $result = $e->getMessage();
            dump($result);
            die;
            $result = false;
        }
        return $result;
    }


}
