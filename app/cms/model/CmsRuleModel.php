<?php
// +----------------------------------------------------------------------
// | 验证规则模型
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\model;

use think\Model;
use think\db\Query;

/**
 * @mixin \think\Model
 */
class CmsRuleModel extends Model
{
    /**
     * 模型名称
     * @var string
     */
    protected $name = 'cms_rule';

    //开启自动时间戳
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updataTime = 'update_time';

    public static   $STATUS = array(
        1=>"已启用",
        0=>"未启用",

    );





    /**
     * 添加
     */
    public function add($data)
    {
        $result = true;
        self::startTrans();
        try {
            $this->save($data);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            $result = $e->getMessage();
            dump($result);die;

            $result = false;
        }
        return $result;
    }

    /**
     * 编辑
     */
    public function edit($data)
    {
        $result = true;
        self::startTrans();
        try {
            $this->update($data);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            $result = $e->getMessage();
            dump($result);die;
            $result = false;
        }
        return $result;
    }
}
