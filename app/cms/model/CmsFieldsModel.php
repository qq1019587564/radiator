<?php
// +----------------------------------------------------------------------
// | 模型字段表模型
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\model;

use think\Model;
use app\cms\library\Alter;
use think\facade\Db;

class CmsFieldsModel extends Model
{
    /**
     * 模型名称
     * @var string
     */
    protected $name = 'cms_fields';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    // 追加属性
    protected $append = [
        //'status_text',
        //'content_list',
    ];
    protected static $listFields = ['select', 'selects', 'checkbox', 'radio', 'array'];

    public function setError($error)
    {
        $this->error = $error;
    }

    /**
     * 添加
     */
    public function add($data)
    {
        $result = true;
        $model = CmsModelModel::find($data['model_id']);
        $sql = Alter::instance()
            ->setTable($model['table'])
            ->setName($data['name'])
            ->setLength($data['length'])
            ->setContent($data['content'])
            ->setDecimals($data['decimals'])
            ->setDefaultvalue($data['defaultvalue'])
            ->setComment($data['title'])
            ->setType($data['type'])
            ->getAddSql();
        self::startTrans();
        try {
            Db::query($sql);
            $this->save($data);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            $result = $e->getMessage();
            $result = false;
        }
        return $result;
    }

    /**
     * 编辑
     */
    public function edit($data)
    {
        $result = true;
        $model = CmsModelModel::find($data['model_id']);
        $sql = Alter::instance()
            ->setTable($model['table'])
            ->setName($data['name'])
            ->setLength($data['length'])
            ->setContent($data['content'])
            ->setDecimals($data['decimals'])
            ->setDefaultvalue($data['defaultvalue'])
            ->setComment($data['title'])
            ->setType($data['type'])
            ->getAddSql(2);

        self::startTrans();
        try {
            Db::query($sql);
            $this->update($data);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            $result = $e->getMessage();
            dump($result);die;
            $result = false;
        }
        return $result;
    }


    public function getContentListAttr($value, $data)
    {
        return in_array($data['type'], self::$listField) ? ConfigModel::decode($data['content']) : $data['content'];
    }

    public function getStatusList()
    {
        return [1 => '显示', 0 => '隐藏'];
    }

    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : $data['status'];
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function model()
    {
        return $this->belongsTo('CmsModelModel', 'model_id'); //->setEagerlyType(0);
    }


    /**
     * 获取字典列表字段
     * @return array
     */
    public static function getListFields()
    {
        return self::$listFields;
    }
}
