<?php
// +----------------------------------------------------------------------
// | 配置模型
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
namespace app\cms\model;

use think\Model;

/**
 * 配置模型
 */
class ConfigModel extends Model
{
    /**
     * 读取配置类型
     * @return array
     */
    public static function getFieldTypeList()
    {
        $getFieldTypeList = [
            'tinyint'       => 'tinyint',
            'smallint'       => 'smallint',
            'int'       => 'int',
            'bigint'       => 'bigint',
            'varchar'       => 'varchar',
            'char'       => 'char',
            'float'       => 'float',
            'double'       => 'double',
            'decimal'       => 'decimal',
            'text'       => 'text',
            'longtext'       => 'longtext',
        ];
        return $getFieldTypeList;
    }
    /**
     * 读取配置类型
     * @return array
     */
    public static function getHtmlTypeList()
    {
        $getHtmlTypeList = [
            'string'       => '字符',
            'text'         => '文本',
            'editor'       => '编辑器',
            'number'       => '数字',
            'date'         => '日期 2020',
            'time'         => '时间 06-21',
            'datetime'     => '日期时间 2020-06-21 13:21',
            'image'        => '图片',
            'images'       => '图片(多)',
            'file'         => '文件',
            'files'        => '文件(多)',
        ];
        return $getHtmlTypeList;
    }
    /**
     * 读取配置类型
     * @return array
     */
    public static function getTypeList()
    {
        $typeList = [
            'string'       => '字符',
            'text'         => '文本',
            'editor'       => '编辑器',
            'number'       => '数字',
            'date'         => '日期',
            'time'         => '时间',
            'datetime'     => '日期时间',
            'image'        => '图片',
            'images'       => '图片(多)',
            'file'         => '文件',
            'files'        => '文件(多)',
            //'select'       => '列表',
            //'selects'      => '列表(多选)',
            //'switch'       => '开关',
            'checkbox'     => '复选',
            'radio'        => '单选',
            'option'        => '选项',
            //'array'        => '数组',
        ];
        return $typeList;
    }

    public static function getBindType()
    {
        $bindtype = [
            'text' => '绑定文本',
            'sql' => '绑定SQL',
            'tree' => '绑定多级类别',
            'channel' => '指定当前栏目',
        ];
        return $bindtype;
    }

    //
    public static function getShowOption()
    {
        $showoption = [
            'select' => '下拉列表框',
            'radio' => '单选按钮组',
            'checkbox' => '多选按钮组',
            'select_two' => '下拉列表框(二级联动)',
            'window' => '弹框选择',

        ];
        return $showoption;
    }

    public static function getRegexList()
    {
        $regexList = [
            '' => '',
            'number'   => '数字',
            'letters'  => '字母',
            // 'date'     => '日期',
            // 'time'     => '时间',
            'email'    => '邮箱',
            'url'      => '网址',
            // 'qq'       => 'QQ号',
            // 'IDcard'   => '身份证',
            'tel'      => '座机电话',
            'mobile'   => '手机号',
            // 'zipcode'  => '邮编',
            'chinese'  => '中文',
            // 'username' => '用户名',
            'password' => '密码'
        ];
        return $regexList;
    }

    /**
     * 读取分类分组列表
     * @return array
     */
    // public static function getGroupList()
    // {
    //     $groupList = config('site.configgroup');
    //     foreach ($groupList as $k => &$v) {
    //         $v = __($v);
    //     }
    //     return $groupList;
    // }

    public static function getArrayData($data)
    {
        $fieldarr = $valuearr = [];
        $field = isset($data['field']) ? $data['field'] : [];
        $value = isset($data['value']) ? $data['value'] : [];
        foreach ($field as $m => $n) {
            if ($n != '') {
                $fieldarr[] = $field[$m];
                $valuearr[] = $value[$m];
            }
        }
        return $fieldarr ? array_combine($fieldarr, $valuearr) : [];
    }

    /**
     * 将字符串解析成键值数组
     * @param string $text
     * @return array
     */
    public static function decode($text, $split = "\r\n")
    {
        $content = explode($split, $text);
        $arr = [];
        foreach ($content as $k => $v) {
            if (stripos($v, "|") !== false) {
                $item = explode('|', $v);
                $arr[$item[0]] = $item[1];
            }
        }
        return $arr;
    }

    /**
     * 将键值数组转换为字符串
     * @param array $array
     * @return string
     */
    public static function encode($array, $split = "\r\n")
    {
        $content = '';
        if ($array && is_array($array)) {
            $arr = [];
            foreach ($array as $k => $v) {
                $arr[] = "{$k}|{$v}";
            }
            $content = implode($split, $arr);
        }
        return $content;
    }
}
