<?php
// +----------------------------------------------------------------------
// | 模型表模型
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\model;

use think\Model;
use think\db\Query;
use app\cms\library\Alter;
use think\facade\Db;
use think\facade\Env;
use think\facade\App;

/**
 * @mixin \think\Model
 */
class CmsModelModel extends Model
{
    /**
     * 模型名称
     * @var string
     */
    protected $name = 'cms_model';

    //开启自动时间戳
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updataTime = 'update_time';



    /**
     * 添加
     */
    public function add($data)
    {
        $result = true;

        //判断
        $result = $this->where("name", $data['name'])->find();
        if ($result) {
            // $this->error('该模型名称已存在');
            // return '该模型名称已存在！';
            return false;
        }
        $result = $this->where("table", $data['table'])->find();
        if ($result) {
            return false;
            // return '该表名已存在！';
        }
        //TODO
        // 拷贝控制器
        $dir = App::getAppPath();
        $copyTplC  = $dir . 'cms/controller/Controller.tpl';
        $newFile = explode('_', $data['table']);
        $str = $data['table'];
        $str = str_replace([' ', 'cms'], '', $str);
        $newFile  = 'Admin' . ucwords($str) . 'Controller.php';
        $newControllerTpl =  $dir . '/cms/controller/' . $newFile;
        //判断控制器是否存在
        if(file_exists($newControllerTpl))
        {
            // return '控制器已存在!';
        }


        //拷贝模型
        $copyTplM  = $dir . 'cms/model/Model.tpl';
        $newFile = explode('_', $data['table']);
        $str = $data['table'];
        $str = str_replace('_', ' ', $str);
        $newFile  =  str_replace(' ', '', ucwords($str) . 'Model.php');
        $newModelTpl =  $dir . '/cms/model/' . $newFile;

        if(file_exists($newModelTpl))
        {
            // return '模型已存在!';
        }
        //TODO END

        self::startTrans();
        try {

            $this->save($data);
            $model_id = $this->id;
            //获取表前缀
            $prefix = Env::get('DATABASE_PREFIX');
            //创建表
            $sql = "CREATE TABLE `{$prefix}{$data['table']}` (`id` int(10) NOT NULL AUTO_INCREMENT,`user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '后台用户id',`channel_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '栏目id',`model_id` int(10) UNSIGNED NOT NULL DEFAULT {$model_id} COMMENT '模型id',`create_time` int(10) NULL DEFAULT 0 COMMENT '创建时间',`update_time` int(10) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '更新时间',`delete_time` int(11) NULL DEFAULT 0 COMMENT '删除时间',`lang` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'cn' COMMENT '语言标识',PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='{$data['name']}'";
            Db::query($sql);

            // //拷贝控制器
            // file_put_contents($newControllerTpl, file_get_contents($copyTplC));
            // //拷贝模型
            // file_put_contents($newModelTpl, file_get_contents($copyTplM));

            self::commit();

        } catch (\Exception $e) {
            self::rollback();
            $result = $e->getMessage();
            dump($result);die;
            // return $e->getMessage();
            $result = false;
        }
        return $result;
    }

    /**
     * 编辑
     */
    public function edit($data)
    {
        $result = true;
       
        //判断标签是否需要更新
        self::startTrans();
        try {
            unset($data['table']);
            $this->update($data);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            $result = $e->getMessage();
            dump($result);
            die;
            $result = false;
        }
        return $result;
    }
}
