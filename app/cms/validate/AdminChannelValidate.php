<?php
// +----------------------------------------------------------------------
// | 栏目管理验证器
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
namespace app\cms\validate;

use app\admin\model\RouteModel;
use think\Validate;

class AdminChannelValidate extends Validate
{
    protected $rule = [
        'name'  => 'require|is_number',
        'route' => 'is_number',//checkRoute
        'thumbnail' => 'long',

    ];
    protected $message = [
        'name.require' => '分类名称不能为空',
    ];

    protected $scene = [
        //        'add'  => ['user_login,user_pass,user_email'],
        // 'edit' => ['name'],
    ];


    protected function is_number($value, $rule, $data)
    {
        if (is_numeric($value)) {
            return '栏目不能是全数字';
        } else {
            return true;
        }
    }
    // 自定义验证规则
    protected function checkRoute($value, $rule, $data)
    {

        if (preg_match("/^\d+$/", $value)) {
            return "别名不能为纯数字!";
        }
        $routeModel = new RouteModel();

        if (isset($data['id']) && $data['id'] > 0) {
            switch ($data['type']) {
                case '1':
                    $url = 'cms/List/index';
                    break;
                default:
                    $url = 'cms/Page/index';
                    break;
            }
            $fullUrl = $routeModel->buildFullUrl($url, ['id' => $data['id']]);
        } else {
            $fullUrl = $routeModel->getFullUrlByUrl($data['route']);
        }

        if (!$routeModel->existsRoute($value, $fullUrl)) {
            return true;
        } else {
            return "伪静态已经存在!";
        }
    }

    /**
     * 检测缩略图是否过长
     */
    public function long($src, $data)
    {
        $msg = howLong($src);
        if (!$msg) {
            return '缩略图名称过长';
        }
        return true;
    }
}
