<?php
// +----------------------------------------------------------------------
// | 文章管理验证器
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
namespace app\cms\validate;

use think\Validate;

class AdminArticleValidate extends Validate
{
    protected $rule = [
        'channel_id' => 'require',
        'title' => 'require',
        'thumbnail' => 'long',

    ];
    protected $message = [
        'channel_id.require' => '请选择所属栏目',
        'title.require' => '文章标题不能为空！',
    ];

    protected $scene = [
        //        'add'  => ['user_login,user_pass,user_email'],
        //        'edit' => ['user_login,user_email'],
    ];


    /**
     * 检测缩略图是否过长
     */
    public function long($src, $data)
    {
        $msg = howLong($src);
        if(!$msg)
        {
            return '缩略图名称过长' ;
        }
        return true;
    }
}
