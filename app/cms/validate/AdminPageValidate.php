<?php
// +----------------------------------------------------------------------
// | 栏目管理验证器
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
namespace app\cms\validate;

use app\admin\model\RouteModel;
use think\Validate;

class AdminPageValidate extends Validate
{
    protected $rule = [
        'title' => 'require',
        // 'post_alias' => 'checkAlias'
    ];
    protected $message = [
        'title.require' => '标题不能为空',
    ];

    protected $scene = [];


    /**
     * 检测缩略图是否过长
     */
    public function long($src, $data)
    {
        $msg = howLong($src);
        if(!$msg)
        {
            return '缩略图名称过长' ;
        }
        return true;
    }
}
