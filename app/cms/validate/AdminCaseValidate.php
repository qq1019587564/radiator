<?php
// +----------------------------------------------------------------------
// | 案例管理验证器
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
namespace app\cms\validate;

use think\Validate;

class AdminCaseValidate extends Validate
{
    protected $rule = [
        'channel_id' => 'require',
        'title' => 'require',
        'thumbnail' => 'long',
    ];
    protected $message = [
        'channel_id.require' => '请选择所属栏目',
        'title.require' => '案例标题不能为空！',
    ];

    protected $scene = [
        
    ];


    /**
     * 检测缩略图是否过长
     */
    public function long($src, $data)
    {
        $msg = howLong($src);
        if(!$msg)
        {
            return '缩略图名称过长' ;
        }
        return true;
    }
}
