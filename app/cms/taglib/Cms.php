<?php
// +----------------------------------------------------------------------
// | 前台标签
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
namespace app\cms\taglib;

use think\template\TagLib;

class Cms extends TagLib
{
    /**
     * 定义标签列表
     */
    protected $tags = [
        // 标签定义： attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次
        'navmenu'             => ['attr' => 'channel_id,order', 'close' => 1], //导航栏标签
        'banner'             => ['attr' => 'channel_id,limit,order', 'close' => 1], //banner标签
        'son'             => ['attr' => 'channel_id,limit,order', 'close' => 1], //获取一级子栏目
        'case'             => ['attr' => 'channel_id,limit,order,noself,is_index', 'close' => 1], //获取案例
        'article'             => ['attr' => 'channel_id,limit,orderby,orderway,noself,page', 'close' => 1], //获取新闻
        'agent'             => ['attr' => 'where,channel_id,limit,order', 'close' => 1], //获取代理商
        'one'             => ['attr' => 'channel_id,id,limit,order', 'close' => 1], //获取特殊管理的某一条数据
        'page'             => ['attr' => 'id', 'close' => 1], //获取单页内容
        'channel'             => ['attr' => 'id', 'close' => 1], //获取栏目
        'hot'             => ['attr' => 'num', 'close' => 1], //热门搜索
        'breadcrumb'       => ['attr' => 'id', 'close' => 1], //面包屑
    ];


    /**
     * 热门搜索
     */
    public function tagHot($tag, $content)
    {
        $item          = empty($tag['item']) ? 'vo' : $tag['item']; //循环变量名
        $num         = empty($tag['num']) ? '' : $tag['num'];
        $returnVarName = 'hot';
        $parse = <<<parse
<?php
\${$returnVarName} = \app\cms\service\CmsService::hot([
    'num'     => '{$num}'
]);
 
 ?>
<volist name="{$returnVarName}" id="{$item}">
{$content}
</volist>
parse;
        return $parse;
    }

    /**
     * 导航栏标签 头部
     */
    public function tagNavmenu($tag, $content)
    {
        $item          = empty($tag['item']) ? 'vo' : $tag['item']; //循环变量名
        $order         = empty($tag['order']) ? '' : $tag['order'];
        $channel_id    = empty($tag['channel_id']) ? '' : $tag['channel_id'];
        $returnVarName = 'navMenmu';

        $parse = <<<parse
<?php
\${$returnVarName} = \app\cms\service\CmsService::navMenu([
    'channel_id'     => '{$channel_id}'
]);

 ?>
<volist name="{$returnVarName}" id="{$item}">
{$content}
</volist>
parse;
        return $parse;
    }

    /**
     * banner
     */
    public function tagBanner($tag, $content)
    {

        $item          = empty($tag['item']) ? 'vo' : $tag['item']; //循环变量名
        $order         = empty($tag['order']) ? '' : $tag['order'];
        $limit         = empty($tag['limit']) ? '100' : $tag['limit'];
        $channel_id    = empty($tag['channel_id']) ? '' : $tag['channel_id'];
        $returnVarName = 'banner';
        $channel_id = "''";
        if (!empty($tag['channel_id'])) {
            if (strpos($tag['channel_id'], '$') === 0) {
                $channel_id = $tag['channel_id'];
                $this->autoBuildVar($channel_id);
            } else {
                $channel_id = "'{$tag['channel_id']}'";
            }
        }
        $parse = <<<parse
<?php
\${$returnVarName} = \app\cms\service\CmsService::banner({$channel_id},{$limit});
 ?>
<volist name="{$returnVarName}" id="{$item}">
{$content}
</volist>
parse;
        return $parse;
    }


    /**
     * 获取当前栏目下的子栏目（一级）
     */
    public function tagSon($tag, $content)
    {

        $item          = empty($tag['item']) ? 'vo' : $tag['item']; //循环变量名
        $order         = empty($tag['order']) ? '' : $tag['order'];
        $limit         = empty($tag['limit']) ? '100' : $tag['limit'];
        $channel_id    = empty($tag['channel_id']) ? '' : $tag['channel_id'];
        $returnVarName = 'son';
        $channel_id = "''";
        if (!empty($tag['channel_id'])) {
            if (strpos($tag['channel_id'], '$') === 0) {
                $channel_id = $tag['channel_id'];
                $this->autoBuildVar($channel_id);
            } else {
                $channel_id = "'{$tag['channel_id']}'";
            }
        }
        $parse = <<<parse
<?php
\${$returnVarName} = \app\cms\service\CmsService::son({$channel_id},{$limit});
 ?>
<volist name="{$returnVarName}" id="{$item}">
{$content}
</volist>
parse;
        return $parse;
    }


    /**
     * 获取案例
     */
    public function tagCase($tag, $content)
    {

        $item          = empty($tag['item']) ? 'vo' : $tag['item']; //循环变量名
        $order         = empty($tag['order']) ? '' : $tag['order'];
        $noself         = empty($tag['noself']) ? '1' : $tag['noself'];
        $is_index         = empty($tag['is_index']) ? '0' : $tag['is_index'];
        $limit         = empty($tag['limit']) ? '100' : $tag['limit'];
        $channel_id    = empty($tag['channel_id']) ? '' : $tag['channel_id'];
        $returnVarName = 'case';
        $channel_id = "''";
        if (!empty($tag['channel_id'])) {
            if (strpos($tag['channel_id'], '$') === 0) {
                $channel_id = $tag['channel_id'];
                $this->autoBuildVar($channel_id);
            } else {
                $channel_id = "'{$tag['channel_id']}'";
            }
        }
        if (!empty($tag['noself'])) {
            if (strpos($tag['noself'], '$') === 0) {
                $noself = $tag['noself'];
                $this->autoBuildVar($noself);
            } else {
                $noself = "'{$tag['noself']}'";
            }
        }
        $parse = <<<parse
<?php
\${$returnVarName} = \app\cms\service\CmsService::case({$channel_id},{$limit},{$noself},{$is_index});
 ?>
<volist name="{$returnVarName}" id="{$item}">
{$content}
</volist>
parse;
        return $parse;
    }

    /**
     * 获取新闻
     */
    public function tagArticle($tag, $content)
    {



        $item          = empty($tag['item']) ? 'vo' : $tag['item']; //循环变量名
        $noself         = empty($tag['noself']) ? '1' : $tag['noself'];
        $limit         = empty($tag['limit']) ? '100' : $tag['limit'];
        $channel_id    = empty($tag['channel_id']) ? '' : $tag['channel_id'];
        $orderby    = empty($tag['orderby']) ? 'id' : $tag['orderby'];
        $orderway    = empty($tag['orderway']) ? 'desc' : $tag['orderway'];
        $page    = empty($tag['page']) ? 'false' : $tag['page'];
        $pageVarName   = empty($tag['pageVarName']) ? '__PAGE_VAR_NAME__' : $tag['pageVarName'];
        $returnVarName = 'articletag';
        $channel_id = "''";
        if (!empty($tag['channel_id'])) {
            if (strpos($tag['channel_id'], '$') === 0) {
                $channel_id = $tag['channel_id'];
                $this->autoBuildVar($channel_id);
            } else {
                $channel_id = "'{$tag['channel_id']}'";
            }
        }
        if (!empty($tag['noself'])) {
            if (strpos($tag['noself'], '$') === 0) {
                $noself = $tag['noself'];
                $this->autoBuildVar($noself);
            } else {
                $noself = "'{$tag['noself']}'";
            }
        }
        $parse = <<<parse
<?php
\${$returnVarName} = \app\cms\service\CmsService::article({$channel_id},'{$limit}',{$noself},'{$orderby}','{$orderway}',{$page});
\${$pageVarName} = isset(\${$returnVarName}['page'])?\${$returnVarName}['page']:'';
 ?>
<volist name="{$returnVarName}.data" id="{$item}">
{$content}
</volist>
parse;
        return $parse;
    }

    /**
     * 获取代理商
     */
    public function tagAgent($tag, $content)
    {

        $item          = empty($tag['item']) ? 'vo' : $tag['item']; //循环变量名
        $order         = empty($tag['order']) ? '' : $tag['order'];
        $limit         = empty($tag['limit']) ? '100' : $tag['limit'];
        $channel_id    = empty($tag['channel_id']) ? '' : $tag['channel_id'];
        $returnVarName = 'agent';
        $where = '""';
        if (!empty($tag['where']) && strpos($tag['where'], '$') === 0) {
            $where = $tag['where'];
        }


        $parse = <<<parse
<?php
\${$returnVarName} = \app\cms\service\CmsService::agent({$where},{$limit});
 ?>
<volist name="{$returnVarName}" id="{$item}">
{$content}
</volist>
parse;
        return $parse;
    }

    /**
     * 获取代理商
     */
    public function tagOne($tag, $content)
    {

        $item          = empty($tag['item']) ? 'vo' : $tag['item']; //循环变量名
        $order         = empty($tag['order']) ? '' : $tag['order'];
        $id         = empty($tag['id']) ? '' : $tag['id'];
        $limit         = empty($tag['limit']) ? '100' : $tag['limit'];
        $channel_id    = empty($tag['channel_id']) ? '' : $tag['channel_id'];
        $returnVarName = 'one';

        $parse = <<<parse
<?php
\${$returnVarName} = \app\cms\service\CmsService::one({$id});
 ?>
{$content}
parse;
        return $parse;
    }

    /**
     * 获取单页内容
     */
    public function tagPage($tag, $content)
    {

        $id         = empty($tag['id']) ? '0' : $tag['id'];
        $returnVarName = 'page';
        if (!empty($tag['id'])) {
            if (strpos($tag['id'], '$') === 0) {
                $id = $tag['id'];
                $this->autoBuildVar($id);
            } else {
                $id = "'{$tag['id']}'";
            }
        }
        $parse = <<<parse
<?php
\${$returnVarName} = \app\cms\service\CmsService::page({$id});
 ?>
{$content}
parse;
        return $parse;
    }


    /**
     * channel 获取栏目
     */
    public function tagChannel($tag, $content)
    {

        $item          = empty($tag['item']) ? 'vo' : $tag['item']; //循环变量名
        $limit         = empty($tag['limit']) ? '100' : $tag['limit'];
        $channel_id    = empty($tag['channel_id']) ? '' : $tag['channel_id'];
        $orderby    = empty($tag['orderby']) ? 'list_order' : $tag['orderby'];
        $orderway    = empty($tag['orderway']) ? 'asc' : $tag['orderway'];
        $type    = empty($tag['type']) ? '1' : $tag['type'];
        $noself    = empty($tag['noself']) ? 'no' : $tag['noself'];



        $returnVarName = 'banner';
        $channel_id = "''";
        if (!empty($tag['channel_id'])) {
            if (strpos($tag['channel_id'], '$') === 0) {
                $channel_id = $tag['channel_id'];
                $this->autoBuildVar($channel_id);
            } else {
                $channel_id = "'{$tag['channel_id']}'";
            }
        }
        $parse = <<<parse
<?php
\${$returnVarName} = \app\cms\service\CmsService::channel({$channel_id},{$limit},'{$orderby}','{$orderway}','{$type}','{$noself}');
 ?>
<volist name="{$returnVarName}" id="{$item}">
{$content} 
</volist>
parse;
        return $parse;
    }


    /**
     * 面包屑标签
     */
    public function tagBreadcrumb($tag, $content)
    {
        $id = empty($tag['id']) ? '0' : $tag['id'];

        if (!empty($id)) {
            $this->autoBuildVar($id);
        }

        $self = isset($tag['self']) ? $tag['self'] : 'false';

        $parse = <<<parse
<?php
if(!empty({$id})){
    \$__BREADCRUMB_ITEMS__ = \app\cms\service\ApiService::breadcrumb({$id},{$self});
?>

<volist name="__BREADCRUMB_ITEMS__" id="vo">
    {$content}
</volist>

<?php
}
?>
parse;

        return $parse;
    }
}
