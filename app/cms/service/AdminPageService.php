<?php
// +----------------------------------------------------------------------
// | 后台单页查询接口
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
namespace app\cms\service;

use app\cms\model\CmsPageModel;
use app\admin\model\RecycleBinModel;
use think\db\Query;

class AdminPageService
{
    /**
     * 后台单页查询
     *
     * @param array $filter  搜索参数
     * @return array
     */
    public static function select($filter, $isPage = false)
    {

        $field = 'a.*,u.user_login,u.user_nickname,c.name';

        $CmsPageModel = new CmsPageModel();
        $articlesQuery   = $CmsPageModel->alias('a');
        $articlesQuery->join('user u', 'a.user_id = u.id');
        $articlesQuery->join('cms_channel c', 'a.channel_id = c.id');

        $articles = $articlesQuery->field($field)
            ->where('a.create_time', '>=', 0)
            ->where('a.delete_time', 0)
            ->where(function (Query $query) use ($filter, $isPage) {
                $startTime = empty($filter['start_time']) ? 0 : strtotime($filter['start_time']);
                $endTime   = empty($filter['end_time']) ? 0 : strtotime($filter['end_time']);
                if (!empty($startTime)) {
                    $query->where('a.published_time', '>=', $startTime);
                }
                if (!empty($endTime)) {
                    $query->where('a.published_time', '<=', $endTime);
                }

                $keyword = empty($filter['keyword']) ? '' : $filter['keyword'];
                if (!empty($keyword)) {
                    $query->where('a.title', 'like', "%$keyword%");
                }
            })
            ->order('published_time', 'DESC')
            ->paginate(isset($filter["pageSize"]) ? $filter["pageSize"] : 10);
        return $articles;
    }


    /**
     * 批量删除
     */
    public static function delete($data)
    {
        $CmsPageModel = new CmsPageModel();
        if (isset($data['id'])) {
            $id = $data['id']; //获取删除id

            $res = $CmsPageModel->where('id', $id)->find();

            if ($res) {
                $res = json_decode(json_encode($res), true); //转换为数组

                $recycleData = [
                    'object_id'   => $res['id'],
                    'create_time' => time(),
                    'table_name'  => 'cms_page',
                    'name'        => $res['title'],
                    'user_id'     => cmf_get_current_admin_id(),
                ];

                CmsPageModel::startTrans(); //开启事务
                $transStatus = false;
                try {
                    CmsPageModel::where('id', $id)->update([
                        'delete_time' => time()
                    ]);
                    RecycleBinModel::insert($recycleData);

                    $transStatus = true;
                    // 提交事务
                    CmsPageModel::commit();
                } catch (\Exception $e) {

                    // 回滚事务
                    CmsPageModel::rollback();
                }
                return $transStatus;
            } else {
                return false;
            }
        } elseif (isset($data['ids'])) {
            $ids = $data['ids'];

            $res = $CmsPageModel->where('id', 'in', $ids)
                ->select();

            if ($res) {
                $res = json_decode(json_encode($res), true);
                foreach ($res as $key => $value) {
                    $recycleData[$key]['object_id']   = $value['id'];
                    $recycleData[$key]['create_time'] = time();
                    $recycleData[$key]['table_name']  = 'cms_page';
                    $recycleData[$key]['name']        = $value['title'];
                }

                CmsPageModel::startTrans(); //开启事务
                $transStatus = false;
                try {
                    CmsPageModel::where('id', 'in', $ids)
                        ->update([
                            'delete_time' => time()
                        ]);
                    RecycleBinModel::insertAll($recycleData);
                    $transStatus = true;
                    // 提交事务
                    CmsPageModel::commit();
                } catch (\Exception $e) {
                    // 回滚事务
                    CmsPageModel::rollback();
                }
                return $transStatus;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
