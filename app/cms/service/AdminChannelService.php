<?php
// +----------------------------------------------------------------------
// | 后台栏目查询接口
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
namespace app\cms\service;

use app\cms\model\CmsChannelModel;
use think\db\Query;
use tree\Tree;

class AdminChannelService
{

    /** 
     * 生成分类 select树形结构
     * @param int $selectId   需要选中的分类 id
     * @param int $currentCid 需要隐藏的分类 id
     * @return string
     */
    public static function adminChannelTree($selectId = 0, $currentCid = 0, $type = 1)
    {
        $channels = CmsChannelModel::order("id ASC")
            ->where('delete_time', 0)
            // ->where('status', 1)
            // ->where(function (Query $query) use ($type) {
            //     if ($type == 1) {
            //         $query->where('type', 1);
            //     }
            // })
            ->where(function (Query $query) use ($currentCid) {
                if (!empty($currentCid)) {
                    $query->where('id', '<>', $currentCid);
                }
            })
            ->select()
            ->toArray();

        $tree       = new Tree();
        $tree->icon = ['&nbsp;&nbsp;│', '&nbsp;&nbsp;├─', '&nbsp;&nbsp;└─'];
        $tree->nbsp = '&nbsp;&nbsp;';

        $newChannels = [];
        foreach ($channels as $item) {
            $item['selected'] = $selectId == $item['id'] ? "selected" : "";

            array_push($newChannels, $item);
        }

        $tree->init($newChannels);
        $str     = '<option value=\"{$id}\" {$selected}>{$spacer}{$name}</option>';
        $treeStr = $tree->getTree(0, $str);

        return $treeStr;
    }

    /**
     * 分类树形结构
     * @param int    $currentIds
     * @param string $tpl
     * @return string
     */
    public static function adminChannelTableTree($currentIds = 0, $tpl = '')
    {
        $channels = CmsChannelModel::with(['model'])->order("id ASC")->where('delete_time', 0)->select()->toArray();
        $tree       = new Tree();
        $tree->icon = ['&nbsp;&nbsp;│', '&nbsp;&nbsp;├─', '&nbsp;&nbsp;└─'];
        $tree->nbsp = '&nbsp;&nbsp;';

        if (!is_array($currentIds)) {
            $currentIds = [$currentIds];
        }

        $newChannels = [];
        foreach ($channels as $item) {
            $item['parent_id_node'] = ($item['parent_id']) ? ' class="child-of-node-' . $item['parent_id'] . '"' : '';
            $item['style']          = empty($item['parent_id']) ? '' : 'display:none;';
            $item['status_text']    = empty($item['status']) ? '<span class="label label-warning">隐藏</span>' : '<span class="label label-success">显示</span>';
            $item['checked']        = in_array($item['id'], $currentIds) ? "checked" : "";
            if ($item['name'] == '首页') {
                $item['url']            = '/';
            } else {
                $item['url']            = '/' . ltrim($item['route'], '/');
            }
            $item['str_action']     = '<a class="btn btn-xs btn-primary"  href="' . url("AdminChannel/add", ["parent" => $item['id']]) . '">添加子栏目</a>  <a class="btn btn-xs btn-primary" href="' . url("AdminChannel/edit", ["id" => $item['id']]) . '">' . lang('EDIT') . '</a>  <a class="btn btn-xs btn-danger js-ajax-delete" href="' . url("AdminChannel/delete", ["id" => $item['id']]) . '">' . lang('DELETE') . '</a> ';
            if ($item['status']) {
                $item['str_action'] .= '<a class="btn btn-xs btn-warning js-ajax-dialog-btn" data-msg="您确定隐藏此分类吗" href="' . url('AdminChannel/toggle', ['ids' => $item['id'], 'hide' => 1]) . '">隐藏</a>';
            } else {
                $item['str_action'] .= '<a class="btn btn-xs btn-success js-ajax-dialog-btn" data-msg="您确定显示此分类吗" href="' . url('AdminChannel/toggle', ['ids' => $item['id'], 'display' => 1]) . '">显示</a>';
            }
            // if ($item['description']) {
            //     $item['description'] = '<span title=' . $item['description'] . '>' . mb_substr($item['description'], 0, 50) . "…</span>";
            // }
            if ($item['model_name']) {
                $item['model_name'] = '<span>' . $item['model_name'] . "</span>";
            }
            switch ($item['type']) {
                case '1':
                    $item['type'] = '<span class="label label-info">列表</span>';
                    break;
                case '2':
                    $item['type'] = '<span class="label label-danger">单页</span>';
                    // $item['str_action'] .= ' <a class="btn btn-xs btn-primary" href="' . url('AdminPage/index', ['id' => $item['page_id'],'model_id' => $item['model_id'],'channel_id'=>$item['id']]) . '">编辑内容</a>';
                    $item['str_action'] .= ' <a class="btn btn-xs btn-primary"  href="javascript:parent.openIframeLayer(\'' . url('AdminPage/index', ['id' => $item['page_id'],'model_id' => $item['model_id'],'channel_id'=>$item['id']]) . '\')">编辑内容</a>';
                    // "javascript:parent.openIframeLayer('{:url('AdminArticle/add',array('channel_id' => $channel_id, 'model_id' => $model_id))}');"
                    break;
                case '3':
                        $item['type'] = '<span class="label label-default">表单</span>';
                        break;
            }
            if ($item['model_id'] >= 1) {
                $item['str_action'] .= ' <a class="btn btn-xs btn-primary" href="' . url('AdminFields/index_bind', ['id' => $item['page_id'], 'channel_id' => $item['id'], 'model_id' => $item['model_id']]) . '">字段列表</a>';
            }
            if ($item['model_id'] == 3) {
                $id = $item['id'];
                $item['str_action'] .= "<div id='{$id}' style='display:none'>".createFormHtml($item['id'])."</div>";
                $item['str_action'] .= '<a class="btn btn-xs btn-danger " onclick="copy('.$item['id'].')">复制表单</a>';
            }
            array_push($newChannels, $item);
        }

        $tree->init($newChannels);

        if (empty($tpl)) {
            $tpl = " <tr id='node-\$id' \$parent_id_node style='\$style' data-parent_id='\$parent_id' data-id='\$id'>
                        <td style='padding-left:20px;'><input type='checkbox' class='js-check' data-yid='js-check-y' data-xid='js-check-x' name='ids[]' value='\$id' data-parent_id='\$parent_id' data-id='\$id'></td>
                        <td><input name='list_orders[\$id]' type='text' size='3' value='\$list_order' class='input-order'></td>
                        <td>\$id</td>
                        <td class='bg'>\$spacer <a href='\$url' target='_blank'>\$name</a></td>
                        <td>\$model_name</td>
                        <td>\$type</td>
                        <td>\$status_text</td>
                        <td>\$str_action</td>
                    </tr>";
        }
        $treeStr = $tree->getTree(0, $tpl);

        return $treeStr;
    }


    /**
     * 后台模型管理的菜单数据
     */
    public function channelTree()
    {

        $channelMenus     = CmsChannelModel::field('page_id,model_id,id,name,parent_id as pId,admin_url as url')
            // ->where('status', 1)
            ->where('delete_time', 0)
            ->where('model_id', '>', 0)
            ->order('list_order ASC')
            ->select()
            ->toArray();
        if (!empty($channelMenus)) {
            foreach ($channelMenus as $k => $v) {
                $channelMenus[$k]['open'] = true;
                $channelMenus[$k]['t'] = $v['name'];
                if ($v['model_id'] == 6) {
                    $channelMenus[$k]['name'] = $v['name'] . '(' . $v['page_id'] . ')';
                } else {
                    $channelMenus[$k]['name'] = $v['name'] . '(' . $v['id'] . ')';
                }
            }
        }
        // dump($channelMenus);die;

        return $channelMenus;
    }


    /**
     * 字段添加编辑显示
     *
     * @param integer $model  模型id
     * @param array $selectId 栏目id
     * @param integer $currentCid ？？
     * @return void
     */
    public function adminFieldsTree($model = 0, $selectId = [], $currentCid = 0)
    {
        $channel = CmsChannelModel::order("list_order ASC")
            // ->where('model_id', $model)
            ->where('delete_time', 0)
            // ->where('status', 1)
            // ->where(function (Query $query) use ($currentCid) {
            //     if (!empty($currentCid)) {
            //         $query->where('id', 'neq', $currentCid);
            //     }
            // })
            ->select()->toArray();
        $tree       = new Tree();
        $tree->icon = ['&nbsp;&nbsp;│', '&nbsp;&nbsp;├─', '&nbsp;&nbsp;└─'];
        $tree->nbsp = '&nbsp;&nbsp;';
        $newCategories = [];
        foreach ($channel as $item) {
            $item['selected'] = "";
            if (in_array($item['id'], $selectId)) {
                $item['selected'] = "selected";
            }
            array_push($newCategories, $item);
        }

        $tree->init($newCategories);
        $parent_id = array_column($newCategories, 'parent_id');
        $parent_id = array_unique($parent_id);
        $str     = '<option model=\"{$model_id}\" value=\"{$id}\" name=\"{$name}\" {$selected}>{$spacer}{$name}</option>';
        $treeStr = $tree->getTree(0, $str);
        return $treeStr;
    }
}
