<?php
// +----------------------------------------------------------------------
// | 自定义标签请求的接口
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
namespace app\cms\service;

use app\admin\model\UserModel;
use app\cms\model\CmsChannelModel;
use app\cms\model\CmsBannerModel;
use app\cms\model\CmsCaseModel;
use app\cms\model\CmsTagModel;
use app\cms\model\CmsArticleModel;
use app\cms\model\CmsImageModel;
use app\agent\model\AgentModel;
use app\hot\model\hotModel;
use app\cms\model\CmsFieldsBindModel;


use think\db\Query;
use tree\Tree;
use think\facade\Db;

use think\facade\Cache;

class CmsService
{

    /**
     * 读取导航栏 顶部
     *
     * @param string $channel_id  栏目id
     * @return array
     */
    public static function navMenu($channel_id = '')
    {
        $caa = json_encode($channel_id);
        if (Cache::get('navMenusTree'.$caa)) {
            return Cache::get('navMenusTree'.$caa);
        }

        $CmsChannelModel = new CmsChannelModel();
        $where = [];
        if (!empty($channel_id['channel_id'])) {
            $where[] = ['id', '=', $channel_id['channel_id']];
        }
        $navMenus     = $CmsChannelModel->where($where)
            // ->field('id,name,route,parent_id')
            ->where('delete_time', 0)
            ->where('status', 1)
            ->order('list_order ASC')
            ->select()
            ->toArray();
        $navMenusTree = [];
        if (!empty($navMenus)) {
            $tree = new Tree();
            $tree->init($navMenus);
            $navMenusTree = $tree->getTreeArray(0, 0);
        }
        Cache::tag('navmenu')->set('navMenusTree', $navMenusTree);
        return $navMenusTree;
    }


    /**
     * 读取banner
     *
     * @param string $channel_id  栏目id
     * @return array
     */
    public static function banner($channel_id, $limit)
    {
        if (Cache::get($channel_id . 'banners')) {
            return Cache::get($channel_id . 'banners');
        }
        $CmsBannerModel = new CmsBannerModel();
        $where = [];
        $sql_category = "FIND_IN_SET('{$channel_id}',channel_id) ";
        $where[] = ['', 'exp', Db::raw($sql_category)];
        $banners     = $CmsBannerModel
            ->where($where)
            ->where('delete_time', 0)
            ->where('status', 1)
            ->limit($limit)
            ->order('list_order ASC')
            ->select()
            ->toArray();
        if (!empty($banners)) {
            $banners[0]['one'] = 'active';
        }
        Cache::set($channel_id . 'banners', $banners);

        return $banners;
    }


    /**
     * 读取子栏目
     *
     * @param string $channel_id  栏目id
     * @return array
     */
    public static function son($channel_id, $limit)
    {
        if (Cache::get($channel_id . 'data')) {
            return Cache::get($channel_id . 'data');
        }
        $CmsChannelModel = new CmsChannelModel();
        $where[] = ['parent_id', '=', $channel_id];
        $field = 'id,name,route';
        $data   = $CmsChannelModel
            ->field($field)
            ->where($where)
            ->where('delete_time', 0)
            ->where('status', 1)
            ->limit($limit)
            ->order('list_order ASC')
            ->select()
            ->toArray();

        Cache::set($channel_id . 'data', $data);

        return $data;
    }

    /**
     * 读取文章模型和图集模型
     * @param int $channel_id  栏目id
     * @param int $limit  限制条数
     * @param int $noself  不读取某个id
     * @param string $orderby  排序字段
     * @param string $orderway  排序方法
     * @param bool $page  是否分页
     * @return array
     */
    public static function article($channel_id, $limit, $noself, $orderby, $orderway, $page)
    {
        
        $pageNum = input('page')??'';
        
        if (Cache::get($channel_id .$limit. $noself.$orderby.$orderway.$page.$pageNum.'article')) {
            return Cache::get($channel_id .$limit. $noself.$orderby.$orderway.$page .$pageNum. 'article');
        }

        if (empty($channel_id)) {
            return ['data' => ''];
        }
        $table = cmsModel($channel_id);
        if (empty($table) || $table == 'cms_page') {
            return ['data' => ''];
        }
        $where = [];
        $return = [];

        if (!empty($channel_id) && is_array($channel_id)) {
            $where[] = ['channel_id', 'in', $channel_id];
        } else {
            $where[] = ['channel_id', '=', $channel_id];
        }
        if (!empty($noself) && $noself != 1) {
            $where[] = ['id', '<>', $noself];
        }

        $field = 'a.*,b.name as channel_name,b.route';
        $sons = getChildrenIds($channel_id);
        $sons = ltrim($sons, ',');
        $controller = request()->controller();

        if (!$page) {
            //判断limit有没有逗号
            if (strpos($limit, ',') !== false) {
                $douhao =  explode(',', $limit);
                //判断是否为首页
                if ($controller == 'Index') {
                    $data   = Db::name($table)
                        ->alias('a')
                        ->join('cms_channel b', 'a.channel_id=b.id')
                        ->field($field)
                        ->where(function ($query) use ($where, $sons, $channel_id) {
                            $channel_id = GetTopTypeId($channel_id);
                            $sons = getChildrenIds($channel_id);
                            $sons = ltrim($sons, ',');
                            $where = [];
                            $where[] = ['a.channel_id', 'in', $channel_id . ',' . $sons];
                            $query->where($where);
                        })
                        ->where('a.delete_time', 0)
                        ->where('a.status', 1)
                        ->limit(intval($douhao[0]), intval($douhao[1]))
                        ->order("a.{$orderby} {$orderway}")
                        ->select()
                        ->toArray();
                } else {
                    $data   = Db::name($table)
                        ->alias('a')
                        ->join('cms_channel b', 'a.channel_id=b.id')
                        ->field($field)
                        ->where(function ($query) use ($where, $sons, $channel_id) {
                            if (!empty($sons)) {
                                $where = [];
                                $where[] = ['a.channel_id', 'in', $channel_id . ',' . $sons];
                                $query->whereOr($where);
                            } else {
                                $query->where($where);
                            }
                        })
                        ->where('a.delete_time', 0)
                        ->where('a.status', 1)
                        ->limit(intval($douhao[0]), intval($douhao[1]))
                        ->order("a.{$orderby} {$orderway}")
                        ->select()
                        ->toArray();
                }
            } else {
                //判断是否为首页
                if ($controller == 'Index') {

                    $data   = Db::name($table)
                        ->alias('a')
                        ->join('cms_channel b', 'a.channel_id=b.id')
                        ->field($field)
                        ->where(function ($query) use ($where, $sons, $channel_id) {
                            $channel_id = GetTopTypeId($channel_id);
                            $sons = getChildrenIds($channel_id);
                            $sons = ltrim($sons, ',');

                            $where = [];
                            $where[] = ['a.channel_id', 'in', $channel_id . ',' . $sons];
                            $query->where($where);
                        })
                        ->where('a.delete_time', 0)
                        ->where('a.status', 1)
                        ->limit($limit)
                        ->order("a.{$orderby} {$orderway}")
                        ->select()
                        ->toArray();
                } else {
                    $data   = Db::name($table)
                        ->alias('a')
                        ->join('cms_channel b', 'a.channel_id=b.id')
                        ->field($field)
                        ->where(function ($query) use ($where, $sons, $channel_id) {
                            if (!empty($sons)) {
                                $where = [];
                                $where[] = ['a.channel_id', 'in', $channel_id . ',' . $sons];
                                $query->whereOr($where);
                            } else {
                                $query->where($where);
                            }
                        })
                        ->where('a.delete_time', 0)
                        ->where('a.status', 1)
                        ->limit($limit)
                        ->order("a.{$orderby} {$orderway}")
                        ->select()
                        ->toArray();
                }
            }

            $return['data'] = $data;
        } else {
            $data   = Db::name($table)
                ->alias('a')
                ->join('cms_channel b', 'a.channel_id=b.id')
                ->field($field)
                ->where(function ($query) use ($where, $sons, $channel_id) {
                    if (!empty($sons)) {
                        $where = [];
                        $where[] = ['a.channel_id', 'in', $channel_id . ',' . $sons];
                        $query->whereOr($where);
                    } else {
                        $query->where($where);
                    }
                })
                ->where('a.delete_time', 0)
                ->where('a.status', 1)
                ->limit($limit)
                ->order("a.{$orderby} {$orderway}")
                ->paginate(intval($limit));
            $data->appends(request()->get());
            $data->appends(request()->post());
            $return['data']    = $data->items();
            $return['page']        = $data->render();
            $return['total']       = $data->total();
            $return['last_page'] = $data->lastPage();
            if ($data->lastPage() == 1 && $data->total() <= $limit) {
                $return['page'] = '<li><span class="pageinfo">共 <strong>' . $return['last_page'] . '</strong>页<strong>' . $return['total'] . '</strong>条记录</span></li>';
            }

            $return['first_page'] = 1;
        }

        $i = 1;
        foreach ($return['data'] as $k => $v) {
            $return['data'][$k]['route'] = $v['route'] . $v['id'] . '.html';
            $return['data'][$k]['thumbnail'] = image($v['thumbnail']);
            $return['data'][$k]['k'] = $i;
            $i++;
        }
        Cache::set($channel_id .$limit. $noself.$orderby.$orderway.$page .$pageNum. 'article', $return);
        
        return $return;
    }


    /**
     * 获取特殊管理的某一篇文章
     *
     * @param array $where  查询条件
     * @return array
     */
    public static function one($id)
    {
        $data   = Db::name('cms_special')
            ->where('id', $id)
            ->where('delete_time', 0)
            ->find();
        return $data;
    }


    /**
     * 获取单页内容
     *
     * @param int $id  查询条件
     * @return array
     */
    public static function page($id)
    {
       if (Cache::get($id . 'page')) {
            return Cache::get($id . 'page');
        }
        if (empty($id)) {
            return ['content' => ''];
        }
        $data   = Db::name('cms_page')
            ->where('id', $id)
            ->where('delete_time', 0)
            ->find();
        if (empty($data)) {
            return ['content' => ''];
        }
        $channel = Db::name('cms_channel')->where('id', $data['channel_id'])->find();
        $data['route'] = $channel['route'];
        $data['thumbnail'] = image($channel['thumbnail']);
        Cache::set($id . 'page', $data);
        return $data;
    }

    /**
     * 读取栏目
     *
     * @param string $channel_id  栏目id
     * @param int $limit  条数
     * @param string $orderby  排序字段
     * @param string $orderway  排序方法
     * @param string $type  类型 son=读取子栏目 self=读取当前栏目
     * @param string $noself  是否包含查询当前栏目
     * @return array
     */
    public static function channel($channel_id, $limit, $orderby, $orderway, $type, $noself)
    {
        
        if (Cache::get($channel_id .$limit. $orderby.$orderway.$type.$noself.'channel')) {
            return Cache::get($channel_id .$limit. $orderby.$orderway.$type.$noself.'channel');
        }
        
        $where = [];
        $where[] = ['id', '=', $channel_id];
        $where[] = ['parent_id', '=', $channel_id];
        $CmsChannelModel = new CmsChannelModel();
        switch ($type) {
            case 'son':
                $where = [];
                $where[] = ['parent_id', '=', $channel_id];
                break;
            case 'self':
                $where[] = ['id', '=', $channel_id];
                break;
            case 'top':
                $where = [];
                $where[] = ['parent_id', '=', 0];
                break;
            default:
                break;
        }
        switch ($noself) {
            case 'yes':
                $where = [];
                $where[] = ['id', '<>', $channel_id];
                $where[] = ['parent_id', '=', $channel_id];
                break;
            default:
                break;
        }

        //判断有没有逗号
        if (strpos($channel_id, ',') !== false) {
            $where = [];
            $where[] = ['id', 'in', $channel_id];
            $where[] = ['parent_id', 'in', $channel_id];
        }
        // $field = 'id,parent_id,name,route,description,alias,thumbnail';
        $data   = $CmsChannelModel::where(function ($query) use ($where, $type) {
            if ($type == 'son') {
                $query->where($where);
            } else {
                $query->whereOr($where);
            }
        })
            // ->field($field)
            // ->where($where)
            ->where('delete_time', 0)
            ->where('status', 1)
            ->limit($limit)
            ->order("{$orderby} {$orderway}")
            ->select()
            ->toArray();

        //     if($channel_id==24 )
        //     {
        //           $data   = $CmsChannelModel::where(function ($query) use ($where,$type) {
        //         if($type=='son')
        //         {
        //             $query->where($where);
        //         }else{
        //           $query->whereOr($where); 
        //         }
        //     })
        //     ->where('delete_time', 0)
        //     // ->where('status', 1)
        //     ->limit($limit)
        //     ->order("{$orderby} {$orderway}")
        //     // ->fetchSql(true)
        //     ->select()
        //     ->toArray();
        //     dump($data);die;
        // }

        $navMenusTree = [];

        if (empty($data)) {
            $where = [];
            //如果为空的话，就去查找上一级
            $channel_id = $CmsChannelModel->where('id', $channel_id)->value('parent_id');
            if ($channel_id != 0) {
                $where[] = ['parent_id', '=', $channel_id];
                $data   = $CmsChannelModel::where(function ($query) use ($where, $type) {
                    if ($type == 'son') {
                        $query->where($where);
                    } else {
                        $query->whereOr($where);
                    }
                })
                    // ->field($field)
                    // ->where($where)
                    ->where('delete_time', 0)
                    ->where('status', 1)
                    ->limit($limit)
                    ->order("{$orderby} {$orderway}")
                    ->select()
                    ->toArray();
            }
        }

        if (empty($data)) {
            return [];
        }


        $sons = getChildrenIds($channel_id);
        $sons = ltrim($sons, ',');
     
        if (!empty($sons)) {
            $tree = new Tree();
            $tree->init($data);
            if ($type != 'son' && $noself != 'yes' ) {
                $navMenusTree = $tree->getTreeArray(0, 0);
            } else {
                $navMenusTree = $tree->getTreeArray($channel_id, 0);
            }
            
            $i = 1;
            $j = 1;
            foreach ($navMenusTree as $k => $v) {
                $navMenusTree[$k]['k'] = $i;
                $i++;
                if (!empty($v['children'])) {
                    foreach ($v['children'] as $k1 => $v1) {
                        $navMenusTree[$k]['children'][$k1]['k'] = $j;
                        $j++;
                    }
                }
            }
        } else {
            $navMenusTree = $data;
        }
        
        Cache::set($channel_id .$limit. $orderby.$orderway.$type.$noself.'channel', $navMenusTree);
        return $navMenusTree;
    }

    /**
     * 获取上一篇  文章模型
     *
     * @param int $id
     * @return array
     */
    public static function publishedPrevArticle($id)
    {
         if (Cache::get($id.'publishedPrevArticle')) {
            return Cache::get($id.'publishedPrevArticle');
        }
        
        $CmsArticleModel = new CmsArticleModel();
        $post            = $CmsArticleModel::where('id', $id)->find();
        $where = [
            'a.status'     => 1,
            'a.delete_time'     => 0,
            'a.channel_id' => $post['channel_id'],
        ];

        $field = 'a.id,b.route,a.title,b.model_id';

        $article = $CmsArticleModel
            ->alias('a')
            ->join('cms_channel b', 'a.channel_id=b.id')
            ->field($field)
            ->where($where)
            ->where('a.id', '<', $id)
            ->where('a.published_time', '<', date('Y-m-d H:i'))
            ->order('a.id', 'DESC')
            ->find();
        /**
         * 转换内容
         */
        if ($article) {

            $fields = CmsFieldsBindModel::where('model_id', $article->model_id)
                ->order('list_order asc,id desc')
                ->select();
            foreach ($fields as $k => $v) {
                switch ($v->type) {
                    case "editor": //编辑器
                        $article[$v->name] = cmf_replace_content_file_url(htmlspecialchars_decode($v->name)); //转码
                        break;
                    default:
                        break;
                }
            }
            $article['route'] = $article['route'] . $article['id'] . '.html';
            $article->toArray();
        }
        
        Cache::set($article.'publishedPrevArticle', $article);
        return $article;
    }

    /**
     * 获取下一篇 文章模型
     *
     * @param int $id
     * @return array
     */
    public static function publishedNextArticle($id)
    {
        if (Cache::get($id.'publishedNextArticle')) {
            return Cache::get($id.'publishedNextArticle');
        }
        $CmsArticleModel = new CmsArticleModel();
        $post            = $CmsArticleModel::where('id', $id)->find();
        $where = [
            'a.status'     => 1,
            'a.delete_time'     => 0,
            'a.channel_id' => $post['channel_id'],
        ];
        $field = 'a.id,b.route,a.title';

        $article = $CmsArticleModel
            ->alias('a')
            ->join('cms_channel b', 'a.channel_id=b.id')
            ->field($field)
            ->where($where)
            ->where('a.id', '>', $id)
            ->where('a.published_time', '<', date('Y-m-d H:i'))
            ->order('a.id', 'ASC')
            ->find();
        /**
         * 转换内容
         */
        if ($article) {

            $fields = CmsFieldsBindModel::where('model_id', $article->model_id)
                ->order('list_order asc,id desc')
                ->select();
            foreach ($fields as $k => $v) {
                switch ($v->type) {
                    case "editor": //编辑器
                        $article[$v->name] = cmf_replace_content_file_url(htmlspecialchars_decode($v->name)); //转码
                        break;
                    default:
                        break;
                }
            }
            $article['route'] = $article['route'] . $article['id'] . '.html';
            $article->toArray();
        }
        Cache::set($article.'publishedNextArticle', $article);
        return $article;
    }

    /**
     * 获取上一篇  图集模型
     *
     * @param int $id
     * @return array
     */
    public static function publishedPrevImage($id)
    {
        if (Cache::get($id.'publishedPrevImage')) {
            return Cache::get($id.'publishedPrevImage');
        }
        $CmsImageModel = new CmsImageModel();
        $post            = $CmsImageModel::where('id', $id)->find();
        $where = [
            'a.status'     => 1,
            'a.delete_time'     => 0,
            'a.channel_id' => $post['channel_id'],

        ];
        $field = 'a.id,b.route,a.title';

        $article = $CmsImageModel
            ->alias('a')
            ->join('cms_channel b', 'a.channel_id=b.id')
            ->field($field)
            ->where($where)
            ->where('a.id', '<', $id)
            ->where('a.published_time', '<', date('Y-m-d H:i'))
            ->order('a.id', 'DESC')
            ->find();

        /**
         * 转换内容
         */
        if ($article) {

            $fields = CmsFieldsBindModel::where('model_id', $article->model_id)
                ->order('list_order asc,id desc')
                ->select();
            foreach ($fields as $k => $v) {
                switch ($v->type) {
                    case "editor": //编辑器
                        $article[$v->name] = cmf_replace_content_file_url(htmlspecialchars_decode($v->name)); //转码
                        break;
                    default:
                        break;
                }
            }
            $article['route'] = $article['route'] . $article['id'] . '.html';
            $article->toArray();
        }


        Cache::set($article.'publishedPrevImage', $article);
        return $article;
    }

    /**
     * 获取下一篇 图集模型
     *
     * @param int $id
     * @return array
     */
    public static function publishedNextImage($id)
    {
        if (Cache::get($id.'publishedNextImage')) {
            return Cache::get($id.'publishedNextImage');
        }
        $CmsImageModel = new CmsImageModel();
        $post            = $CmsImageModel::where('id', $id)->find();
        $where = [
            'a.status'     => 1,
            'a.delete_time'     => 0,
            'a.channel_id' => $post['channel_id'],
        ];
        $field = 'a.id,b.route,a.title';

        $article = $CmsImageModel
            ->alias('a')
            ->join('cms_channel b', 'a.channel_id=b.id')
            ->field($field)
            ->where($where)
            ->where('a.id', '>', $id)
            ->where('a.published_time', '<', date('Y-m-d H:i'))
            ->order('a.id', 'ASC')
            ->find();
        /**
         * 转换内容
         */
        if ($article) {
            $fields = CmsFieldsBindModel::where('model_id', $article->model_id)
                ->order('list_order asc,id desc')
                ->select();
            foreach ($fields as $k => $v) {
                switch ($v->type) {
                    case "editor": //编辑器
                        $article[$v->name] = cmf_replace_content_file_url(htmlspecialchars_decode($v->name)); //转码
                        break;
                    default:
                        break;
                }
            }
            $article['route'] = $article['route'] . $article['id'] . '.html';
            $article->toArray();
        }

        Cache::set($article.'publishedNextImage', $article);
        return $article;
    }






















































    /**
     * 客户数据库切换处理
     * @param $params
     * @return bool
     */
    public function changeDbName($params)
    {

        /*切库start*/
        $db_config = config('database');
        $company_db_name = '';
        if (isset($params["company_id"]) && is_numeric($params["company_id"])) { //用户以后登录后台时，根据company_id获取对应数据库
            $company_id = $params["company_id"];
            //获取对应公司站点的数据库，旧cms数据库转换后的新的数据库名称
            $company_db_name = Db::connect('tenant')
                ->table('mod_website_info')
                ->where('company_id', $company_id)
                ->value('db_name'); //以转换后的数据保存的数据字段为准
            // var_dump($company_id);
            var_dump($company_db_name);
        } else {
            $host_url = $params["host_url"];
            $host_arr = explode('//', $host_url);
            $host = $host_arr[1];
            $company_db_name = Db::connect('tenant')
                ->table('mod_website_info')
                ->where(function ($query) use ($host) {
                    $query->where('formal_url', $host) //正式域名
                        ->whereOr('tmp_url', '=', $host); //临时域名
                })->value('db_name'); //以转换后的数据保存的数据字段为准 
        }


        if (!$company_db_name) {
            throw new \Exception('获取公司数据库name不存在');
        }

        // if($company_db_name){//临时调试测试使用
        //     $company_db_name = 'xstcms';
        // }


        $db_config['connections']['mysql']['database'] = $company_db_name;
        config($db_config, 'database');
        Db::connect('mysql', true);
        /*切库end*/
        return true;
    }
    /**
     * @param 热门搜索
     * @return array
     */
    public static function hot($mun)
    {
        $mun = intval($mun);
        // $hotModel = new hotModel();
        $data = Db::name('hot')
            ->where(array("status" => 1))
            ->limit($mun)
            ->order('create_time desc')
            ->select()
            ->toArray();
        return $data;
    }
}
