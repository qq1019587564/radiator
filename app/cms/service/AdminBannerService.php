<?php
// +----------------------------------------------------------------------
// | 后台banner查询接口
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
namespace app\cms\service;

use app\cms\model\CmsBannerModel;
use think\db\Query;

class AdminBannerService
{
    /**
     * 后台banner查询
     *
     * @param array $filter  搜索参数
     * @return array
     */
    public static function select($filter,$isPage = false)
    {

        $field = 'a.*,u.user_login,u.user_nickname';

        $cmsBannerModel = new CmsBannerModel();
        $articlesQuery   = $cmsBannerModel->alias('a');
        // $articlesQuery->join('user u', 'a.user_id = u.id');

        $channel_id = empty($filter['channel_id']) ? 0 : intval($filter['channel_id']);
        // if (!empty($channel_id) || $isPage == false) {//
            $articlesQuery->join('cms_channel b', 'a.channel_id = b.id');
            $field = 'a.*,b.name,b.route';
        // }

        $articles = $articlesQuery->field($field)
            ->where('a.create_time', '>=', 0)
            ->where('a.delete_time', 0)
            ->where(function (Query $query) use ($filter, $isPage) {
                $channel_id = empty($filter['channel_id']) ? 0 : intval($filter['channel_id']);
                if (!empty($channel_id)) {
                    $query->where('b.id', $channel_id);
                }

                $startTime = empty($filter['start_time']) ? 0 : strtotime($filter['start_time']);
                $endTime   = empty($filter['end_time']) ? 0 : strtotime($filter['end_time']);
                if (!empty($startTime)) {
                    $query->where('a.published_time', '>=', $startTime);
                }
                if (!empty($endTime)) {
                    $query->where('a.published_time', '<=', $endTime);
                }

                $keyword = empty($filter['keyword']) ? '' : $filter['keyword'];
                if (!empty($keyword)) {
                    $query->where('a.title', 'like', "%$keyword%");
                }
            })
            ->order('published_time', 'DESC')
            ->paginate(isset($filter["pageSize"]) ? $filter["pageSize"] : 10);
        return $articles;
    }

}
