<?php
// +----------------------------------------------------------------------
// | 控制器模板
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);
namespace app\cms\controller;

use cmf\controller\AdminBaseController;
use app\cms\model\CmsModelModel;
use app\cms\model\CmsChannelModel;
use app\cms\service\AdminChannelService;
use app\cms\model\CmsFieldsModel;
use think\facade\Db;
use tree\Tree;
use think\facade\Cache;
use app\cms\model\{$Capitalize_tpl}Model;


class {$controller_tpl} extends AdminBaseController
{

    /**
     * article模型对象
     */
    protected $model = null;
    protected $channelModel = null;

    public function initialize() 
    {
        parent::initialize();
        //栏目模型
        $this->channelModel = new CmsChannelModel();
        //栏目接口
        $this->channelService = new AdminChannelService();
        //当前控制器模型
        $this->model = new {$Capitalize_tpl}Model;
        //当前控制器表名
        $this->table = '{$table_tpl}';
        //当前控制器
        $this->controller = '{$low_contro_tpl}';
        //当前模型id
        $this->model_id = {$model_id_tpl};
        //当前语言标识
        $this->lang = input('lang')??config('config.admin_lang');
        //模型表
        $this->cmsModel = new CmsModelModel();
        //模型字段表
        $this->cmsModelFields = new CmsFieldsModel();

        $this->assign('table',$this->table);
    }



    public function index()
    {

        $param = $this->request->param();
        $param['start_time'] = isset($param['start_time']) ? $param['start_time'] : '';
        $param['end_time'] = isset($param['end_time']) ? $param['end_time'] : '';
        $category = !empty($param['category']) ? $param['category'] : '';

        $start_time = !empty($param['start_time']) ? $param['start_time'] : 0;
        $end_time = !empty($param['end_time']) ? $param['end_time'] : '2040-12-12 10:04:56';

        $keyword = isset($param['keyword']) ? $param['keyword'] : '';
        $channel_id = $this->request->param('channel_id', 0, 'intval'); //栏目id
        $search_fields = 'title';//默认搜索title
        $search_field = $this->cmsModelFields->where('channel_id', $channel_id)->where('search', 1)->column('name'); //开启搜索的字段
        //查询是否开启分类字段
        $categoryField = $this->cmsModelFields->where('channel_id', $channel_id)->where('name','category')->where('status',1)->find(); 
        if($categoryField>0)
        {
            $categoryField['content'] = json_decode($categoryField['content'],true);
            $this->assign('categoryField',$categoryField['content']);
        }else{
            $this->assign('categoryField',0);
        }
        if (!empty($search_field)) {
            foreach ($search_field as $v) {
                $search_fields .=  '|'.$v . '|';
            }
            $search_fields = rtrim($search_fields, '|');
        }
        $where = [];
        if (!empty($channel_id)) {
            $where[] = ['a.channel_id', '=', $channel_id];
        }
        if (!empty($category)) {
            $where[] = ['a.category', '=', $category];
        }
        $wheres = '';
        $admin_id = cmf_get_current_admin_id();
        $articles = Db::name($this->table . ' a')
            ->join('cms_channel b', 'a.channel_id=b.id')
            ->field('a.*,b.name as b_name')
            ->where($search_fields, 'like', "%{$keyword}%")
            ->where('a.delete_time', '=', 0)
            ->where($where)
            //->where('auth_id', 'in', '0,' . cmf_get_current_admin_id())
            ->order('a.published_time desc,a.list_order asc')
            // ->where($wheres)
            // ->where($wheress)
            // ->fetchSql(true)
            // ->select();
            ->where('a.published_time', '>=', $start_time)
            ->where('a.published_time', '<=', $end_time)
            // ->fetchSql(true)
            // ->select();
            // ->where('a.published_time','<=', $param['end_time']??'2099-10-02 16:27')
            ->paginate(10, null, ['query' => $param]);
        // dump($articles);die;
        $page = $articles->render();
        $articles = $articles->toArray()['data'];



        //控件权限
        $contentButtonAuth = contentButtonAuth($channel_id, $model_id);
        // dump($contentButtonAuth);die;
        $this->assign('action', $contentButtonAuth ?? "");
        $this->assign('category', $category);
        $this->assign('start_time', isset($param['start_time']) ? $param['start_time'] : '');
        $this->assign('end_time', isset($param['end_time']) ? $param['end_time'] : '');
        $this->assign('keyword', isset($param['keyword']) ? $param['keyword'] : '');
        $this->assign('articles', $articles);
        $this->assign('channel_id', $channel_id);
        $this->assign('page', $page);
        $this->assign('model_id', $model_id);


        return $this->fetch();
    }

    /**
     * 内容管理
     *
     * @return void
     */
    public function channel()
    {
        $this->assign('lang',$this->lang);
        return $this->fetch();
    }

    public function channelPost()
    {    
        $categoryTree = $this->channelService->channelTree();
        if($categoryTree){
            $this->success('ok','',$categoryTree);
        }else{
            $this->error('TooBad');
        }
    }

    /**
     * 添加
     */
    public function add()
    {

        $channel_id = $this->request->param('channel_id', 0, 'intval');
        $model_id = $this->request->param('model_id', 0, 'intval');
        $table = $this->table;
        $categoryTree        = $this->channelService->adminChannelTree(0);
        $this->assign('category_tree', $categoryTree);
        $this->assign('channel_id', $channel_id);
        $this->assign('table', $table);
        $this->assign('model_id', $model_id);
        return $this->fetch();
    }

    /**
     * 添加提交
     */
    public function addPost()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();
            //状态只能设置默认值
            $data['post']['status'] = 1;
            $post = $data['post'];
            //数据处理
            if (empty(cms_dataPost($data, $post)['data'])) {
                $this->error(cms_dataPost($data, $post)['msg']);
            }
            $data = cms_dataPost($data, $post)['data'];

            //模型不能用缓存数据  ??用不上缓存sql 
            // $this->model->adminAddArticle($data['post']);
            //字段数据处理
            $data['post'] = cms_field($data['post']);
            $data['post']['create_time'] = time();
            $data['post']['update_time'] = time();
            $newData = thumb($data['post']['channel_id'], $data['post'])['data'];
            // dump($newData);
            foreach ($newData as $value) {
                $newData['lang'] = $this->lang;
            }
            if(empty($newData['published_time']))
            {
                $newData['published_time'] = date('Y-m-d H:i',time());
            }
            $newData['model_id'] = $this->model_id;
            $row = Db::name($this->table)->insertGetId($newData);
            if ($row) {
                $data['post']['id'] = $row;
                op_log(1, '添加文章提交', '添加');
                //更新缓存标识
                setCache($this->table,1);
                cmf_clear_cache();
                $this->success('添加成功!', url('edit', ['id' => $row]));
            }
            op_log(0, '添加文章提交', '添加');
            $this->success('添加失败!');
        }
    }

    /**
     * 编辑文章
     * @adminMenu(
     *     'name'   => '编辑文章',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '编辑文章',
     *     'param'  => ''
     * )
     */
    public function edit()
    {

        $modelxlist = CmsModelxModel::all();
        if (count($modelxlist) == 0) {
            $this->error('未找到对应模型');
        }

        $id = $this->request->param('id', 0, 'intval');

        $post = Db::name('cms_article')->where('id', $id)->find();
        //必定字段
        $post['content'] = cmf_replace_content_file_url(htmlspecialchars_decode($post['content']));
        $post['table'] = $this->table;

        //cmf格式
        $categoryTree        = $this->channelModel->adminChannelTree(0);
        // dump($categoryTree);
        $this->assign('category_tree', $categoryTree);
        $this->assign('post', $post);

        return $this->fetch();
    }

    /**
     * 编辑提交
     */
    public function editPost()
    {

        if ($this->request->isPost()) {
            $data = $this->request->param();

            $post   = $data['post'];
            $msg =  cms_dataPost($data, $post);
            //数据处理
            if (empty($msg['data'])) {
                $this->error($msg['msg']);
            }
            $data = $msg['data'];
            // dump($data['post']);die;
            // $this->model->adminEditArticle($data['post'], $data['post']['channel_id']);

            //字段数据处理
            $data['post'] = cms_field($data['post']);
            $data['post']['update_time'] = time();
            $newData = thumb($data['post']['channel_id'], $data['post'])['data'];
            // dump($newData);die;
            $newData['model_id'] = $this->model_id;
            $row = Db::name($this->table)->update($newData);
            if ($row) {
                op_log(1, '编辑文章提交', '编辑');
                setCache($this->table,1);
                cmf_clear_cache();

                // dump(Cache::get($this->table));
                $this->success('编辑成功!');
            }
            op_log(0, '编辑文章提交', '编辑');
            $this->error('编辑失败!');
        }
    }

    /**
     * 删除
     */
    public function delete()
    {
        $param           = $this->request->param();

        //单条删除
        if (isset($param['id'])) {
            $id           = $this->request->param('id', 0, 'intval');
            Db::startTrans();
            try {
                $resultPortal = $this->model
                    ->where('id', $id)
                    ->update(['delete_time' => time()]);
                $row = Db::name('recycle_bin')->where(['table_name' => $this->table, 'object_id' => $id])->count();
                if ($row <= 0) {
                    //添加到回收站
                    $data['object_id'] = $id;
                    $data['table_name'] = $this->table;
                    $data['create_time'] = time();
                    $data['user_id'] = cmf_get_current_admin_id();
                    Db::name('recycle_bin')->insert($data);
                }
                $op = true;
                // 提交事务
                Db::commit();
            } catch (\Exception $e) {
                $op = false;
                $op_msg = $e->getMessage();
                // 回滚事务
                Db::rollback();
            }



            if ($op) {
                op_log(1, '文章删除',  '删除');
                setCache($this->table,1);
                cmf_clear_cache();
                $this->success("删除成功！", '');
            } else {
                op_log(0, '文章删除', '删除');
                $this->error($op_msg, '');
            }
        }

        //批量删除
        if (isset($param['ids'])) {
            $ids     = $this->request->param('ids/a');
            // dump($ids);die;
            Db::startTrans();
            try {
                $result  = $this->model->where('id', 'in', $ids)->update(['delete_time' => time()]);
                //添加到回收站
                // dump($ids);die;
                foreach ($ids as $k => $v) {
                    $row = Db::name('recycle_bin')->where(['table_name' => $this->table, 'object_id' => $v])->count();
                    if ($row <= 0) {
                        $data[$k]['object_id'] = $v;
                        $data[$k]['table_name'] = $this->table;
                        $data[$k]['create_time'] = time();
                        $data[$k]['user_id'] = cmf_get_current_admin_id();
                    }
                }
                if (!empty($data)) {
                    Db::name('recycle_bin')->insertAll($data);
                }
                $op = true;

                // 提交事务
                Db::commit();
            } catch (\Exception $e) {
                $op = false;

                $op_msg = $e->getMessage();
                // 回滚事务
                Db::rollback();
            }

            if ($result) {
                op_log(1, '文章批量删除',  '批量删除');
                setCache($this->table,1);
                cmf_clear_cache();
                $this->success("批量删除成功！", '');
            } else {
                op_log(0, '文章删除', '删除');
                $this->error($op_msg, '');
            }
        }
    }

    /**
     * 文章发布
     * @adminMenu(
     *     'name'   => '文章发布',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '文章发布',
     *     'param'  => ''
     * )
     */
    public function publish()
    {
        $param           = $this->request->param();

        if (isset($param['ids']) && isset($param["yes"])) {
            $ids = $this->request->param('ids/a');
            $this->model->where('id', 'in', $ids)->update(['status' => 1, 'published_time' => time()]);
            op_log(1, '文章发布',  '发布');
            setCache($this->table,1);
            cmf_clear_cache();

            $this->success("发布成功！", '');
        }

        if (isset($param['ids']) && isset($param["no"])) {
            $ids = $this->request->param('ids/a');
            $this->model->where('id', 'in', $ids)->update(['status' => 0]);
            op_log(1, '文章批量发布',  '发布');
            setCache($this->table,1);
            cmf_clear_cache();

            $this->success("取消发布成功！", '');
        }
    }

    /**
     * 文章排序
     */
    public function listOrder()
    {
        parent::listOrders($this->model);
        setCache($this->table,1);
        cmf_clear_cache();

        $this->success("排序更新成功！", '');
    }


    /**
     * 获取栏目字段
     * @internal
     */
    public function get_channel_fields()
    {
        $channel_id = $this->request->post('channel_id');
        $model_id = $this->request->post('model_id');
        $aid = $this->request->post('aid');
        //获取栏目信息
        $channel = $this->channelModel->get($channel_id, 'model');
        cms_get_channel_fields($channel_id, $model_id, $aid, $channel);
    }
    /**
     * 回收站
     */
    public function recyclebin()
    {


        $param = $this->request->param();
        $keyword = $param['keyword'] ?? "";

        //文章模型的回收站
        $data = DB::name('recycle_bin a')
            ->join($this->table . ' b', 'a.object_id=b.id')
            ->where('table_name', $this->table)
            ->where('b.title', 'like', "%$keyword%")
            ->where('b.delete_time', '>', 0)
            ->paginate(15, null, ['query' => $param]);

        $page = $data->render();
        $data = $data->toArray()['data'];


        // $this->assign('start_time', isset($param['start_time']) ? $param['start_time'] : '');
        // $this->assign('end_time', isset($param['end_time']) ? $param['end_time'] : '');
        $this->assign('keyword', isset($param['keyword']) ? $param['keyword'] : '');
        $this->assign('data', $data);
        $this->assign('page', $page);


        return $this->fetch();
    }

    /**
     * 销毁
     * @param string $ids
     */
    public function destroy()
    {
        $param = $this->request->param();
        if (isset($param['ids'])) {
            $ids     = $this->request->param('ids/a');
            $result = $this->model->destroy($ids);
            if ($result) {
                op_log(1, '回收站销毁', '销毁');
                cmf_clear_cache();

                $this->success("删除成功！", '');
            } else {
                op_log(0, '回收站销毁', '销毁');
                $this->error("删除失败！");
            }
        }
    }

    /**
     * 还原
     * @param mixed $ids
     */
    public function restore()
    {
        $param = $this->request->param();
        if (isset($param['ids'])) {
            $ids     = $this->request->param('ids/a');
            $result = $this->model->where('id', 'in', $ids)->update(['delete_time' => 0]);
            if ($result) {
                op_log(1, '回收站还原', '还原');
                cmf_clear_cache();
                $this->success("还原成功！", '');
            } else {
                op_log(0, '回收站还原', '还原');
                $this->error("还原失败！");
            }
        }
    }




    /**
     * 批量复制树桩栏目
     */
    public function copy()
    {
        $ids    = input('ids');
        $ids = str_replace(' ids .html', '', $ids);
        // dump($ids);

        $model_id    = $this->request->param('model_id', 0, 'intval');
        // dump($model_id);die;
        $model = new \app\cms\model\CmsChannelModel;
        $categoryTree = $model->adminChannelCopyTree($this->lang,$model_id);
        $this->assign('category_tree', $categoryTree);
        $this->assign('model_id', $model_id);
        $this->assign('idss', $ids);


        return $this->fetch();
    }
    /**
     * 批量复制提交
     *
     * @return void
     */
    public function copy_post()
    {
        $data = input();
        $ids    = $data['ids']; //栏目id
        $channel = Db::name('cms_channel')->where('id','in',$ids)->field('id,lang')->select()->toArray();
        $idss = str_replace('   ids   .html', '', $data['idss']);
        $model_id    = $data['model_id']; //模型id
        $table_name = $this->cmsModel::where('id', $model_id)->value('table');
        // dump($data);die;
        // $newData = [];
        $copyData = DB::name($this->table)
            ->where('id', 'in', $idss)
            ->select()
            ->toArray();

        // dump($copyData);die;

        Db::startTrans();
        try {

            // Db::name('cms_archives')->insertAll($newData);

            foreach ($channel as $k => $v) {

                foreach ($copyData as $key => $value) {

                    unset($copyData[$key]['id']);
                    $copyData[$key]['published_time'] = date('Y-m-d H:i', time());
                    $copyData[$key]['create_time'] = time();
                    $copyData[$key]['channel_id'] = $v['id'];
                    $copyData[$key]['lang'] = $v['lang'];
                    // $newData[] = $copyData[$key];
                    // dump($copyData[$key]);die;
                    // $this->model->adminAddArticle($copyData[$key], $v);
                    $newData[] = $copyData[$key];

                }
                // dump($copyData);
            }
            // dump($newData);die;
            Db::name($table_name)->insertAll($newData);
            // dump($userId);die;
            $op = true;
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            $op = false;
            $op_msg = $e->getMessage();
            // 回滚事务
            Db::rollback();
            //注意：我们做了回滚处理，所以id为1039的数据还在
        }
        setCache($this->table,1);
        if ($op) {
            op_log(1, '文章批量复制', '批量复制');
            cmf_clear_cache();

            $this->success('批量复制成功');
        } else {
            op_log(0, '文章批量复制', '批量复制');
            $this->error($op_msg);
        }
    }

    public function remove()
    {
        $ids    = input('ids'); //需要移动的栏目id
        $ids = str_replace(' ids .html', '', $ids);
        $channel_id = input('category');
        $channel_id = str_replace('html', '', $channel_id); //当前栏目id
        $aid = input('aid'); //需要移动的文章id
        $aid = str_replace(' ids .html', '', $aid);

        // dump($aid);

        $model_id    = $this->request->param('model_id', 0, 'intval');
        // dump($model_id);die;
        $model = new \app\cms\model\CmsChannelModel;
        $categoryTree = $model->adminChannelCopyTree($model_id, $channel_id);
        $this->assign('category_tree', $categoryTree);
        $this->assign('model_id', $model_id);
        $this->assign('idss', $ids);

        return $this->fetch();
    }

    public function remove_post()
    {
        $data = input();
        // $moveData = Db::name('cms_archives')->where('id','in',$data['aid'])->update(['channel_id'])
    }
}
