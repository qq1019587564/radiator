<?php
// +----------------------------------------------------------------------
// | 轮播图管理
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
namespace app\cms\controller;

use cmf\controller\AdminBaseController;
use app\cms\model\CmsBannerModel;
use app\cms\model\CmsChannelModel;
use app\cms\service\AdminChannelService;
use think\facade\Db;
use think\facade\Request;


class AdminBannerController extends AdminBaseController
{
    /**
     * 初始化数据
     */
    protected $model = null;

    public function initialize()
    {
        parent::initialize();
        $this->model = new CmsBannerModel();
    }

    /**
     * 幻灯片页面列表
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index()
    {
        
        // dump(Request::domain(true));die;
        $result  = CmsBannerModel::select();
        foreach ($result as $k => $v) {
            $result[$k]['channel_name'] = implode(',', CmsChannelModel::where('id', 'in', $v['channel_id'])->column('name'));
        }
        $this->assign('result', $result);
        return $this->fetch();
    }

    /**
     * 幻灯片页面添加
     */
    public function add()
    {
        return $this->fetch();
    }

    /**
     * 幻灯片页面添加提交
     */
    public function addPost()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();
            $data['user_id'] = cmf_get_current_admin_id();
            $result = $this->model->add($data);
            if ($result === false) {
                op_log(0, '添加banner-' . $data['title'], '添加');
                $this->error('banner添加失败');
            }
            $id = $this->model->id;

            op_log(1, '添加banner-' . $data['title'], '添加');
            $this->success('添加成功!', url('AdminBanner/edit', ['id' => $id]));
        }
    }

    /**
     * 幻灯片页面编辑
     */
    public function edit()
    {

        $id     = $this->request->param('id', 0, 'intval');
        $result = CmsBannerModel::where('id', $id)->find();
        if (empty($result)) {
            $this->error('数据不存在', url('index'));
        }
        $channels = CmsChannelModel::where('id', 'in', $result['channel_id'])->column('name');
        $channels = implode(' ', $channels);
        $this->assign('result', $result);
        $this->assign('channels', $channels);
    
        return $this->fetch();
    }

    /**
     * 幻灯片页面编辑
     */
    public function editPost()
    {
        if ($this->request->isPost()) {
            $data = $this->request->param();
           
            $data['user_id'] = cmf_get_current_admin_id();
            $result = $this->model->edit($data);
            if ($result === false) {
                op_log(0, '编辑banner-' . $data['title'], '编辑');
                $this->error('保存失败!');
            }
            op_log(1, '编辑banner-' . $data['title'], '编辑');
            
            
            $channel_id = $data['channel_id'];
            $channel_id = explode(',',$channel_id);
            foreach ($channel_id as $k=>$v)
            {
                $where = [];
                $sql_category = "FIND_IN_SET('{$v}',channel_id) ";
                $where[] = ['', 'exp', Db::raw($sql_category)];
                $banners     = $this->model
                    ->where($where)
                    ->where('delete_time', 0)
                    ->where('status', 1)
                    ->order('list_order ASC')
                    ->select()
                    ->toArray();
                if (!empty($banners)) {
                    $banners[0]['one'] = 'active';
                }
                
                cache($v.'banners', $banners, 3600*24*7);
            }

            
            $this->success('保存成功!');
        }
    }

    /**
     * 幻灯片页面删除
     */
    public function delete()
    {
        if ($this->request->isPost()) {
            $id = $this->request->param('id', 0, 'intval');

            $AdminBanner = CmsBannerModel::find($id);

            $result = CmsBannerModel::destroy($id);
            if ($result) {
                op_log(1, '删除幻灯片-' . $AdminBanner['title'], '删除');
                $this->success("删除成功！", url("AdminBanner/index"));
            } else {
                op_log(0, '删除幻灯片-' . $AdminBanner['title'], '删除');
                $this->error('删除失败！');
            }
        }
    }

    /**
     * 幻灯片页面隐藏
     */
    public function ban()
    {
        if ($this->request->isPost()) {
            $id = $this->request->param('id', 0, 'intval');
            if ($id) {
                $rst = CmsBannerModel::where('id', $id)->update(['status' => 0]);
                if ($rst) {
                    op_log(1, '设置banner隐藏', '隐藏');
                    $this->success("幻灯片隐藏成功！");
                } else {
                    op_log(0, '设置banner隐藏', '隐藏');
                    $this->error('幻灯片隐藏失败！');
                }
            } else {
                $this->error('数据传入失败！');
            }
        }
    }

    /**
     * 幻灯片页面显示
     */
    public function cancelBan()
    {
        if ($this->request->isPost()) {
            $id = $this->request->param('id', 0, 'intval');
            if ($id) {
                $result = CmsBannerModel::where('id', $id)->update(['status' => 1]);
                if ($result) {
                    op_log(1, '设置banner显示', '显示');
                    $this->success("幻灯片启用成功！");
                } else {
                    op_log(0, '设置banner显示', '显示');
                    $this->error('幻灯片启用失败！');
                }
            } else {
                $this->error('数据传入失败！');
            }
        }
    }

    /**
     * 幻灯片页面排序
     */
    public function listOrder()
    {
        $CmsBannerModel = new  CmsBannerModel();
        parent::listOrders($CmsBannerModel);
        op_log(1, 'banner排序', '排序');
        $this->success("排序更新成功！");
    }

    /**
     * 多选树状栏目
     */
    public function select()
    {
        $ids                 = $this->request->param('ids');
        $selectedIds         = explode(',', $ids);

        $tpl = <<<tpl
<tr class='data-item-tr'>
    <td>
        <input type='checkbox' class='js-check' data-yid='js-check-y' data-xid='js-check-x' name='ids[]'
               value='\$id' data-name='\$name' \$checked>
    </td>
    <td>\$id</td>
    <td>\$spacer <a href='\$url' target='_blank'>\$name</a></td>
</tr>
tpl;

        $categoryTree = AdminChannelService::adminChannelTableTree($selectedIds, $tpl);

        $categories = CmsChannelModel::where('delete_time', 0)->select();

        $this->assign('categories', $categories);
        $this->assign('selectedIds', $selectedIds);
        $this->assign('categories_tree', $categoryTree);
        return $this->fetch();
    }
}
