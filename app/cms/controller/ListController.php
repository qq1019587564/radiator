<?php
// +----------------------------------------------------------------------
// | 前台列表控制器
// +----------------------------------------------------------------------
// | zsh
// +----------------------------------------------------------------------
namespace app\cms\controller;

use cmf\controller\HomeBaseController;
use app\cms\model\CmsChannelModel;
use think\Db;

class ListController extends HomeBaseController
{
    /***
     * 列表
     */
    public function index()
    {
        $id = $this->request->param('id', 0, 'intval');
       
            $cmsChannelModel = new CmsChannelModel();
            $channel = $cmsChannelModel->where('id', $id)->where('delete_time', 0)->find();
            cache($id.'List',$channel);
        
        
        if (empty($channel)) {
            $this->redirect('/404.html', 404);
        }

        $this->assign('channel', $channel);
        $listTpl = $channel['list_tpl'];
        
         if(web_type() && cmf_is_mobile())
        {
            $url = $this->request->domain().str_replace('s=','/m',$this->request->query());
            $this->redirect($url,301);
        }
        $manifest = "themes/xst/cms/{$listTpl}.html";
        if (!file_exists_case($manifest)) {
            $this->redirect('/404.html', 404);
        }
        return $this->fetch('/' . $listTpl);
    }
}
