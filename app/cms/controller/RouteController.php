<?php
// +----------------------------------------------------------------------
// | 前台单页控制器
// +----------------------------------------------------------------------
// | zsh
// +----------------------------------------------------------------------
namespace app\cms\controller;

use cmf\controller\HomeBaseController;
use think\facade\Db;
use app\admin\model\RouteModel;


class RouteController extends HomeBaseController
{

    public function route()
    {
        $data = $this->request->param();
        // dump($data);die;
        // $key = 'e184QJHs/r+ifuGn59ojMz+5XglLYUL2qj1PBgWX5HHI4FIoDjpJhY5cpKJ1';
        $key = $data['key'];
        $key = cmf_str_decode($key, 'xinsehngtai@2021');

        if (empty($key) || $key != 'xinsehngtai@2021') {
            return msg(404, [], 'toobad');
        }
        // $aaa =Db::name('route')->select()->toArray();
        // dump($aaa);die;
        //设置伪静态
        $routeModel = new RouteModel();
        $routeModel->getRoutes(true);

    }
}
