<?php
// +----------------------------------------------------------------------
// | 前台文章详情页控制器
// +----------------------------------------------------------------------
// | zsh
// +----------------------------------------------------------------------
namespace app\cms\controller;

use cmf\controller\HomeBaseController;
use think\facade\Db;
use app\cms\model\CmsChannelModel;
use app\cms\model\CmsArticleModel;
use app\cms\model\CmsCaseModel;
use app\cms\model\CmsTagModel;
use think\facade\Request;
use app\cms\service\CmsService;

class ArticleController extends HomeBaseController
{
    /**
     * 页面管理
     * @return mixed
     */
    public function index()
    {
    
        $id = $this->request->param('id', 0, 'intval');
        $where = [];
  
        $CmsArticlelModel = new CmsArticleModel();
        $article = $CmsArticlelModel->where(['id'=>$id,'status' => 1, 'delete_time' => 0])->find();
        if (empty($article)) {
            $this->redirect('/404.html', 404);
        }
        $fields = DB::name('cms_fields_bind')->where('channel_id', $article['channel_id'])->where('status', 1)->select()->toArray();
        foreach ($fields as $k => $v) {
            switch ($v['type']) {
                case 'editor':
                    $article[$v['name']] = cmf_replace_content_file_url(htmlspecialchars_decode($article[$v['name']]));
                    break;
                case 'images':
                    $article[$v['name']] = json_decode($article[$v['name']], true);
                    if (!empty($article[$v['name']])) {
                        $name = $v['name'];
                        foreach ($article[$name] as $k => $v) {
                            @$article[$name][$k]['url'] = image($v['url']);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        
        $channel = CmsChannelModel::where('id', $article['channel_id'])->where('delete_time', 0)->find();
        if (empty($channel)) {
            $this->redirect('/404.html', 404);
        }

        
        $prevArticle = CmsService::publishedPrevArticle($id);
        $nextArticle = CmsService::publishedNextArticle($id);
        $this->assign('channel', $channel);
        $this->assign('article', $article);
        $this->assign('prev', $prevArticle);
        $this->assign('next', $nextArticle);
        
         if(web_type() && cmf_is_mobile())
        {
            $url = $this->request->domain().str_replace('s=','/m',$this->request->query());
            $this->redirect($url,301);
        }
        $showtpl = $channel['article_tpl'];
        $manifest = "themes/xst/cms/{$showtpl}.html";
        if (!file_exists_case($manifest)) {
            $this->redirect('/404.html', 404);
        }
        return $this->fetch('/' . $channel['article_tpl']);
    }
}
