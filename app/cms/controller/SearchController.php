<?php
// +----------------------------------------------------------------------
// | 前台搜索、表单提交
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
namespace app\cms\controller;

use cmf\controller\HomeBaseController;
use think\facade\Db;
use app\form\model\FormModel;
use app\agent\model\AgentModel;
use think\facade\Env;
//引入框架自带分页渲染类
use think\paginator\driver\Bootstrap;

class SearchController extends HomeBaseController
{
    /**
     * 搜索
     * @return mixed
     */
    public function index()
    {
        $keyword = $this->request->param('keyword');
        $q = $this->request->param('q');
        $page = $this->request->param('page') ?? 1;
        //客户端传过来的分页
        $pageNumber = $page ? $page : '1';
        if($pageNumber > 0){
            $pageNumber_one = $pageNumber - 1;
        } else {
            $pageNumber_one = 1;
        }
        $q = str_replace(['select','insert','update','delete',',',';'],'',strtolower($q));
        $keyword = str_replace(['select','insert','update','delete',',',';'],'',strtolower($keyword));
        
        $limit = 10;//每页显示条数
        $offset = $pageNumber_one * $limit;
        $field = "`a`.`title`,`a`.`id`,`a`.`thumbnail`,`a`.`seo_description`,`a`.`published_time`,`a`.`click`,b.name as channel_name,`b`.`route`";
        $field1 = "`c`.`title`,`c`.`id`,`c`.`thumbnail`,`c`.`seo_description`,`c`.`published_time`,`c`.`click`,d.NAME AS channel_name,`d`.`route`";
        $where = "`title` LIKE '%{$keyword}%'  AND `a`.`delete_time` = '0'  AND `a`.`status` = '1'";
        $where1 = "`title` LIKE '%{$keyword}%' and c.delete_time=0 and c.status=1";
        $orderby = "order by a.published_time desc";
        $orderby1 = "order by c.published_time desc";
        
        if (!empty($keyword)) {
            
            $sql ="( SELECT {$field} FROM `xst_cms_article` `a` INNER JOIN `xst_cms_channel` `b` ON `a`.`channel_id`=`b`.`id` WHERE  {$where} {$orderby} ) UNION ( SELECT {$field1} FROM `xst_cms_image` `c` INNER JOIN `xst_cms_channel` `d` ON `c`.`channel_id` = `d`.`id` WHERE {$where1} {$orderby1} ) limit $offset,$limit";
            $data = Db::query($sql);
             
            //查询的总条数
            $sqlTotal = "( SELECT {$field} FROM `xst_cms_article` `a` INNER JOIN `xst_cms_channel` `b` ON `a`.`channel_id`=`b`.`id` WHERE  {$where} {$orderby} ) UNION ( SELECT {$field1} FROM `xst_cms_image` `c` INNER JOIN `xst_cms_channel` `d` ON `c`.`channel_id` = `d`.`id` WHERE {$where1} {$orderby1} )";
            
            $counts = Db::query($sqlTotal);
            $count = count($counts);
            
            //分页
            $pagernator = Bootstrap::make($data,$limit,$pageNumber,$count,false,['path'=>Bootstrap::getCurrentPath(),'query'=>request()->param()]);
            $page = $pagernator->render();
            foreach ($data as $k => $v) {
                $data[$k]['route'] = $v['route'] . $v['id'] . '.html';
                $data[$k]['thumbnail'] = image($v['thumbnail']);
                $data[$k]['title'] = str_replace($keyword,'<font color=\'red\'>'.$keyword.'</font>',$v['title']);
            }
        }elseif(!empty($q))
        {
            $where = "`title` LIKE '%{$q}%'  AND `a`.`delete_time` = '0'  AND `a`.`status` = '1'";
            $where1 = "`title` LIKE '%{$q}%' and c.delete_time=0 and c.status=1";
        
            $sql ="( SELECT {$field} FROM `xst_cms_article` `a` INNER JOIN `xst_cms_channel` `b` ON `a`.`channel_id`=`b`.`id` WHERE  {$where} {$orderby} ) UNION ( SELECT {$field1} FROM `xst_cms_image` `c` INNER JOIN `xst_cms_channel` `d` ON `c`.`channel_id` = `d`.`id` WHERE {$where1} {$orderby1} ) limit $offset,$limit";
            $data = Db::query($sql);
             
            //查询的总条数
            $sqlTotal = "( SELECT {$field} FROM `xst_cms_article` `a` INNER JOIN `xst_cms_channel` `b` ON `a`.`channel_id`=`b`.`id` WHERE  {$where} {$orderby} ) UNION ( SELECT {$field1} FROM `xst_cms_image` `c` INNER JOIN `xst_cms_channel` `d` ON `c`.`channel_id` = `d`.`id` WHERE {$where1} {$orderby1} )";
            
            $counts = Db::query($sqlTotal);
            $count = count($counts);
            
            //分页
            $pagernator = Bootstrap::make($data,$limit,$pageNumber,$count,false,['path'=>Bootstrap::getCurrentPath(),'query'=>request()->param()]);
            $page = $pagernator->render();
            foreach ($data as $k => $v) {
                $data[$k]['route'] = $v['route'] . $v['id'] . '.html';
                $data[$k]['thumbnail'] = image($v['thumbnail']);
                $data[$k]['title'] = str_replace($keyword,'<font color=\'red\'>'.$keyword.'</font>',$v['title']);
            }
            $keyword = $q;
        }
        
        
        $this->assign("data", $data ?? '');
        $this->assign('page', $page);
        $this->assign("keyword", $keyword);
        $showtpl = 'search';
        
        if(cmf_is_mobile() && web_type())
        {
            $showtpl = '/search_m';
            $manifest = "themes/xst/cms/search_m.html";
            if (!file_exists_case($manifest)) {
                $showtpl = '/search';
            }
        }
        
        return $this->fetch($showtpl);
    }

    /**
     * 留言
     *
     * @return array
     */
    public function message()
    {

        $data = $this->request->param();
        // dump($data);die;
        if ($this->request->isPost()) {
            if (!empty($data['d'])) {
                $data = $data['d'];
            }
            if (empty($data['channel_id'])) {
                $this->error('缺乏必要参数');
            } elseif (!intval($data['channel_id'])) {
                $this->error('别搞花里胡哨的好吧');
            }
            unset($data['null']);
            unset($data['formid']);
            //获取当前用户ip地址
            $ip = $this->request->ip(0, true);
            $map = array(
                'ip'    => $ip,
                'lang'      => $data['lang'] ?? config('config.home_lang') ?? 'cn',
            );
            $count = DB::name('cms_message')->where($map)->where('create_time', '>', time() - 120)->count('id');
            if ($count > 0) {
                $this->error('同一个IP在120秒之内不能重复提交！');
            }
            $data['create_time'] = time();
         
              $message = Db::name('cms_message')->strict(false)->insert($data);
            if ($message) {
                $this->success('留言成功');
            }
            $this->error('留言失败');
        } else {
            $this->redirect(cmf_get_domain());
        }
    }






















    /**
     * ocpc线索api回传
     */
    public function ocpc()
    {
        $this->success('ok');
        if ($this->request->isAjax()) {
            $data = $this->request->param();
            //解密token
            $jieMi = cmf_str_decode($data['token'], 'ebaidu@2021.');
            if ($jieMi != 'ebaidu_ocpc' . Env::get('DATABASE_AUTHCODE')) {
                $this->error('请求失败');
            }
            switch ($data['type']) {
                case '1':
                    $token = config('config.consult_token') ?? "";
                    break;
                case '2':
                    $token = config('config.tel_token') ?? "";
                    break;
                case '3':
                    $token = config('config.form_token') ?? "";
                    break;
            }
            if (!empty($data['ocpc_url']) && !empty($data['type']) && !empty($data['token'])) {
                $cv = array(
                    'logidUrl' => $data['ocpc_url'], // 您的落地页url
                    'newType' => $data['type'] // 转化类型请按实际情况填写
                );
                $conversionTypes = array($cv);
                $status = sendOcpcData($token, $conversionTypes);
                if ($status) {
                    $apiData['create_time'] = getTime();
                    $apiData['status'] = $status;
                    Db::name('ocpc_api')->insert($apiData);
                    $this->success('ok');
                }
            }
            $this->error('error');
        }
    }
}
