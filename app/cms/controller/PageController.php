<?php
// +----------------------------------------------------------------------
// | 前台单页控制器
// +----------------------------------------------------------------------
// | zsh
// +----------------------------------------------------------------------
namespace app\cms\controller;

use cmf\controller\HomeBaseController;
use think\facade\Db;

use app\cms\model\CmsPageModel;
use app\cms\model\CmsChannelModel;

class PageController extends HomeBaseController
{
    /**
     * 页面管理
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index()
    {
        $pageId      = $this->request->param('id', 0, 'intval');
       
        $page = CmsPageModel::where('id', $pageId)->find();
        if (empty($page)) {
            $this->redirect('/404.html', 404);
        }
        //栏目信息
        $channel = CmsChannelModel::where('id', $page['channel_id'])->find();
        if (empty($channel)) {
            $this->redirect('/404.html', 404);
        }
        
        $fields = DB::name('cms_fields_bind')->where('channel_id', $channel['id'])->where('status', 1)->select()->toArray();
        foreach ($fields as $k => $v) {
            switch ($v['type']) {
                case 'editor':
                    $page[$v['name']] = cmf_replace_content_file_url(htmlspecialchars_decode($page[$v['name']]));
                    break;
                case 'images':
                    $page[$v['name']] = json_decode($page[$v['name']], true);
                    if (!empty($page[$v['name']])) {
                        $name = $v['name'];
                        foreach ($page[$name] as $k => $v) {
                            @$page[$name][$k]['url'] = image($v['url']);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        
        $this->assign('page', $page);
        $this->assign('channel', $channel);
        $showtpl = $channel['page_tpl'];
        $tplName = empty($showtpl) ? 'page' : $showtpl;
        if (web_type() && cmf_is_mobile()) {
            $url = $this->request->domain() . str_replace('s=', '/m', $this->request->query());
            $this->redirect($url, 301);
        }
        $manifest = "themes/xst/cms/{$showtpl}.html";
        if (!file_exists_case($manifest)) {
            $this->redirect('/404.html', 404);
        }
        return $this->fetch("/$tplName");
    }
}
