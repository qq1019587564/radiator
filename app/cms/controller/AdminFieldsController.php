<?php
// +----------------------------------------------------------------------
// | 模型字段管理
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
namespace app\cms\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;
use app\cms\model\ConfigModel;
use app\cms\model\CmsFieldsModel;
use app\cms\model\CmsChannelModel;
use app\cms\model\CmsModelModel;
use app\cms\model\CmsFieldsBindModel;
use app\cms\service\AdminChannelService;

use tree\Tree;


class AdminFieldsController extends AdminBaseController
{
    /**
     * Fields模型对象
     */
    protected $model = null;

    public function initialize() 
    {
        parent::initialize();
        $this->model = new CmsFieldsModel();
        $this->CmsModelModel = new CmsModelModel();
        $this->channelService = new AdminChannelService();
        $this->view->assign("statusList", $this->model->getStatusList());
        $this->view->assign('typeList', ConfigModel::getTypeList());
        $this->view->assign('bindtypes', ConfigModel::getBindType());
        $this->view->assign('showoptions', ConfigModel::getShowOption());
        $this->view->assign('regexList', Db::name('cms_rule')->select()->toArray());
    }

    /**
     * 查看模型字段
     */
    public function index()
    {
        $model_id = $this->request->param('model_id', 0);
        $diyform_id = $this->request->param('diyform_id', 0);
        $condition = ['model_id' => $model_id, 'status' => 1];

        //设置过滤方法
        $this->request->filter(['strip_tags']);

        $list = $this->model
            ->where($condition)
            ->order('list_order', 'asc')
            ->paginate(10);

        $this->assign('fieldsList', $list);
        $this->assign('model_id', $model_id);
        $this->assign('diyform_id', $diyform_id);
        $this->assign('page', $list->render());

        return $this->fetch();
    }

    /**
     * 查看所属模型已有字段
     */
    public function already()
    {
        $model_id = $this->request->param('model_id', 0);
        $channel_id = $this->request->param('channel_id', 0);

        $condition = ['model_id' => $model_id];
        $channel_fields = Db::name('cms_fields_bind')->where('channel_id', $channel_id)->column('field_id');
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        //    dump($model_id);
        $list = Db::name('cms_fields')
            ->where($condition)
            ->where('id', 'not in', $channel_fields)
            ->order('list_order', 'asc')
            // ->fetchsql(true)
            ->select()
            ->toArray();
        // dump($list);die;
        $this->assign('fieldsList', $list);
        $this->assign('model_id', $model_id);
        $this->assign('channel_id', $channel_id);
        return $this->fetch();
    }

    /**
     * 查看模型栏目绑定字段
     */
    public function index_bind()
    {
        $model_id = $this->request->param('model_id', 0);
        $channel_id = $this->request->param('channel_id', 0);

        $condition = ['model_id' => $model_id, 'channel_id' => $channel_id];
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        $list = Db::name('cms_fields_bind')
            // ->join('cms_fields b', 'a.field_id=b.id')
            // ->field('a.*,b.type,b.title')
            ->where($condition)
            ->order('list_order asc,create_time desc')
            // ->fetchsql(true)
            ->select()
            ->toArray();
        foreach ($list as $k => $v) {
            switch ($v['search']) {
                case '1':
                    $list[$k]['search'] = '是';
                    break;
                default:
                    $list[$k]['search'] = '否';
                    break;
            }
        }
        $this->assign('fieldsList', $list);
        $this->assign('model_id', $model_id);
        $this->assign('channel_id', $channel_id);
        return $this->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        $model_id = $this->request->param('model_id', 0);
        $diyform_id = $this->request->param('diyform_id', 0);
        $channel_id = $this->request->param('channel_id', 0);
        $this->view->assign('model_id', $model_id);
        $this->view->assign('diyform_id', $diyform_id);
        $this->view->assign('channel_id', $channel_id);
        return $this->fetch();
    }
    /**
     * 栏目字段添加
     */
    public function add_bind()
    {
        $model_id = $this->request->param('model_id', 0);
        $diyform_id = $this->request->param('diyform_id', 0);
        $channel_id = $this->request->param('channel_id', 0);
        $this->view->assign('model_id', $model_id);
        $this->view->assign('diyform_id', $diyform_id);
        $this->view->assign('channel_id', $channel_id);
        $categoryTree = $this->channelService->adminFieldsTree($model_id);
        $this->assign('category_tree', $categoryTree);
        return $this->fetch();
    }
    /**
     * 添加已有字段
     * 2019年10月10日15:40:23
     */
    public function alreadyAdd()
    {
        $data = $this->request->param();
        if (empty($data)) {
            op_log(0, '添加已有字段', '添加');
            $this->error('请选择字段');
        }
        // dump($data);die;
        //查询是否已插入字段 重复就草他妈b
        foreach ($data['ids'] as $k => $v) {
            $field = DB::name('cms_fields_bind')->field('name')->where(['model_id' => $data['model_id'], 'channel_id' => $data['channel_id']])->where('field_id', $v)->find();
            if ($field) {
                op_log(0, '添加已有字段', '添加');
                $this->error($field['name'] . '字段已存在，不要重复勾上！！');
            }
        }
        //查询模型的字段 插入
        $readyData = DB::name('cms_fields')->where('id', 'in', $data['ids'])->select()->toArray();
        foreach ($readyData as $k => $v) {
            $readyData[$k]['channel_id'] = $data['channel_id'];
            $readyData[$k]['create_time'] = time();
            $readyData[$k]['update_time'] = time();
            $readyData[$k]['field_id'] = $v['id'];
            if ($v['bindtype'] == 'channel') {
                $readyData[$k]['bindtext'] = $data['channel_id'];
            }
            unset($readyData[$k]['id']);
            unset($readyData[$k]['list_order']);
        }
        // dump($readyData);die;
        $field = Db::name('cms_fields_bind')->insertAll($readyData);
        if ($field) {
            op_log(1, '添加已有字段',  '添加');
            $this->success('添加成功');
        }
        op_log(0, '添加已有字段',  '添加');
        $this->error('添加失败');
    }



    /**
     * 模型添加字段提交操作
     */
    public function addPost()
    {
        if ($this->request->isPost()) {

            $data = $this->request->param();
            $post = $data['post'];
            $model_id = $post['model_id'];
            $table = $this->CmsModelModel->where('id', $model_id)->value('table');
            $result = $this->validate($this->request->param(), 'AdminFields');
            if ($result !== true) {
                op_log(0, '模型添加字段提交操作', '添加');
                $this->error($result);
            } else {
                if (!preg_match("/^([a-zA-Z0-9_]+)$/i", $post['name'])) {
                    $this->error("字段只支持字母数字下划线");
                }
                if (is_numeric(substr($post['name'], 0, 1))) {
                    $this->error("字段不能以数字开始");
                }
                //判断字段是否存在
                if ($post['model_id']) {
                    $tableFields = Db::name($table)->getTableFields();
                    if (in_array(strtolower($post['name']), $tableFields)) {
                        $this->error("字段已经在主表存在了");
                    }
                }
                //绑定sql
                if (!empty($post['bindtype']) && $post['bindtype'] == 'sql' && $post['showoption'] != 'window') {
                    Db::startTrans();
                    try {
                        $res = Db::query($post['bindtext']);
                        // 提交事务
                        Db::commit();
                    } catch (\Exception $e) {
                        $this->error($e->getMessage());
                        // 回滚事务
                        Db::rollback();
                    }
                    $content = [];
                    foreach ($res as $k => $v) {
                        $content[$v[$post['sqlval']]] = $v[$post['sqlname']];
                    }
                    $post['content'] =  json_encode($content, JSON_UNESCAPED_UNICODE);
                }

                //树状显示 select
                if (!empty($post['bindtype']) && $post['bindtype'] == 'tree') {
                    $tree       = new Tree();
                    $tree->icon = ['&nbsp;&nbsp;│', '&nbsp;&nbsp;├─', '&nbsp;&nbsp;└─'];
                    $tree->nbsp = '&nbsp;&nbsp;';
                    $newCategories = [];
                    Db::startTrans();
                    try {
                        if ($post['sqltreeid'] >= 0 && isset($post['sqltreeid']) && strlen($post['sqltreeid']) > 0) {

                            $sql = $post['bindtexts'] . 'where parent_id =' . $post['sqltreeid'];
                        } else {
                            $sql = $post['bindtexts'];
                        }

                        $res = Db::query($sql);
                        foreach ($res as $item) {
                            $item['selected'] = $post['defaultvalue'] == $item['id'] ? "selected" : "";
                            array_push($newCategories, $item);
                        }
                        $tree->init($newCategories);
                        $str     = '<option value=\"{$id}\" {$selected}>{$spacer}{$name}</option>';
                        $post['content'] = $tree->getTree(0, $str);
                        // 提交事务
                        Db::commit();
                    } catch (\Exception $e) {
                        $this->error($e->getMessage());
                        // 回滚事务
                        Db::rollback();
                    }
                }

                //指定当前栏目
                if (!empty($post['bindtype']) && $post['bindtype'] == 'channel') {
                    $post['bindtext'] = $post['channel_id'] ?? 0;
                    $channel_data = Db::name($table)->where('delete_time', '=', 0)->field('id,title')->select()->toArray();
                    // dump($channel_data);
                    $content = [];
                    foreach ($channel_data as $k => $v) {
                        $content[$v['id']] = $v['title'];
                    }
                    $post['content'] =  json_encode($content, JSON_UNESCAPED_UNICODE);
                }

                $result = $this->model->add($post);
                $field_id = $this->model->id;

                //在栏目添加字段
                if (!empty($post['channel_id'])) {
                    $post['field_id'] = $field_id;
                    $post['create_time'] = time();
                    $post['update_time'] = time();
                    Db::startTrans();
                    try {
                        Db::name('cms_fields_bind')->insert($post);
                        // 提交事务
                        Db::commit();
                    } catch (\Exception $e) {
                        $this->error($e->getMessage());
                        // 回滚事务
                        Db::rollback();
                    }
                }
                if ($result) {
                    op_log(1, '模型添加字段提交操作', '添加');
                    $this->success("添加成功!");
                } else {
                    op_log(0, '模型添加字段提交操作', '添加');
                    $this->error("添加失败");
                }
            }
        }
    }


    /**
     * 编辑
     */
    public function edit()
    {
        $id = $this->request->param('id', 0);
        $model = $this->model->find($id)->toArray();
        $this->view->assign($model);
        return $this->fetch();
    }

    /**
     * 编辑栏目字段
     * 2019年10月10日17:16:38
     */
    public function edit_bind()
    {
        $id = $this->request->param('id', 0);
        $channel_id = $this->request->param('channel_id', 0);
        $model = new CmsFieldsBindModel();
        $model = $model->find($id)->toArray();
        $this->view->assign($model);
        $model_id = $this->request->param('model_id', 0);
        $field_id =  Db::name('cms_fields_bind')->where('id', $id)->find();
        $channel_id = Db::name('cms_fields_bind')
            ->where('field_id', $field_id['field_id'])
            ->where(['title' => $field_id['title'], 'type' => $field_id['type']])
            ->column('channel_id');
        $categoryTree = $this->channelService->adminFieldsTree($model_id, $channel_id);
        $this->assign('category_tree', $categoryTree);
        return $this->fetch();
    }


    /**
     * 编辑栏目字段提交
     *
     * @return void
     */
    public function edit_bindPost()
    {
        if ($this->request->isPost()) {

            $data = $this->request->param();
            $post = $data['post'];
            $model_id = $post['model_id'];
            $table = Db::name('cms_model')->where('id', $model_id)->value('table');
            // dump($data);die;

            $result = $this->validate($this->request->param(), 'AdminFields');
            if ($result !== true) {
                op_log(0,  '编辑栏目字段提交', '编辑');
                $this->error($result);
            } else {
                //判断
                if (!preg_match("/^([a-zA-Z0-9_]+)$/i", $post['name'])) {
                    $this->error("字段只支持字母数字下划线");
                }
                if (is_numeric(substr($post['name'], 0, 1))) {
                    $this->error("字段不能以数字开始");
                }

                //绑定sql
                if (!empty($post['bindtype']) && $post['bindtype'] == 'sql' && $post['showoption'] != 'window') {

                    Db::startTrans();
                    try {
                        $res = Db::query($post['bindtext']);
                        // 提交事务
                        Db::commit();
                    } catch (\Exception $e) {
                        op_log(0, '编辑字段选项sql', '编辑');
                        $this->error($e->getMessage());
                        // 回滚事务
                        Db::rollback();
                    }
                    $content = [];
                    foreach ($res as $k => $v) {
                        $content[$v[$post['sqlval']]] = $v[$post['sqlname']];
                    }
                    $post['content'] =  json_encode($content, JSON_UNESCAPED_UNICODE);
                }

                //树状显示 select
                if (!empty($post['bindtype']) && $post['bindtype'] == 'tree') {
                    $tree       = new Tree();
                    $tree->icon = ['&nbsp;&nbsp;│', '&nbsp;&nbsp;├─', '&nbsp;&nbsp;└─'];
                    $tree->nbsp = '&nbsp;&nbsp;';
                    $newCategories = [];
                    Db::startTrans();
                    try {

                        if ($post['sqltreeid'] >= 0 && isset($post['sqltreeid']) && strlen($post['sqltreeid']) > 0) {
                            $sql = $post['bindtexts'] . 'where parent_id =' . $post['sqltreeid'];
                        } else {
                            $sql = $post['bindtexts'];
                        }
                        $res = Db::query($sql);

                        foreach ($res as $item) {
                            $item['selected'] = $post['defaultvalue'] == $item['id'] ? "selected" : "";
                            array_push($newCategories, $item);
                        }
                        $tree->init($newCategories);
                        $str     = '<option value=\"{$id}\" {$selected}>{$spacer}{$name}</option>';
                        $post['content'] = $tree->getTree(0, $str);
                        // 提交事务
                        Db::commit();
                    } catch (\Exception $e) {
                        op_log(0, '编辑字段选项树状显示', '编辑');
                        $this->error($e->getMessage());
                        // 回滚事务
                        Db::rollback();
                    }
                }
                //指定当前栏目
                if (!empty($post['bindtype']) && $post['bindtype'] == 'channel') {
                    $post['bindtext'] = $post['channel_id'] ?? 0;
                    $channel_data = Db::name($table)->where('delete_time', '=', 0)->field('id,title')->select()->toArray();
                    $content = [];
                    foreach ($channel_data as $k => $v) {
                        $content[$v['id']] = $v['title'];
                    }
                    $post['content'] =  json_encode($content, JSON_UNESCAPED_UNICODE);
                }
                $post['update_time'] = time();

                //判断多少个栏目
                if (empty($data['channel_id']) || !is_array($data['channel_id'])) {
                    op_log(0, '编辑栏目字段提交', '编辑');

                    $this->error('请选择栏目');
                }
                unset($post['id']);
                Db::startTrans();
                try {
                    foreach ($data['channel_id'] as $k => $v) {
                        $result = Db::name('cms_fields_bind')->where(['channel_id' => $v, 'model_id' => $post['model_id'], 'field_id' => $post['field_id']])->update($post);
                        if ($result == 0) {
                            $post['channel_id'] = $v;
                            Db::name('cms_fields_bind')->insert($post);
                        }
                    }
                    $op = true;
                    // 提交事务
                    Db::commit();
                } catch (\Exception $e) {
                    $op = false;
                    $op_msg = $e->getMessage();
                    // 回滚事务
                    Db::rollback();
                }

                if ($op) {
                    op_log(0, '编辑栏目字段提交', '编辑');
                    $this->success("修改成功!");
                } else {
                    op_log(0, '编辑栏目字段提交', '编辑');
                    $this->error($op_msg);
                }
            }
        }
    }
    /**
     * 编辑提交
     */
    public function editPost()
    {

        if ($this->request->isPost()) {

            $data = $this->request->param();
            $post = $data['post'];
            $result = $this->validate($this->request->param(), 'AdminFields');
            if ($result !== true) {
                op_log(0, '模型编辑字段提交操作', '编辑');
                $this->error($result);
            } else {
                if (!preg_match("/^([a-zA-Z0-9_]+)$/i", $post['name'])) {
                    $this->error("字段只支持字母数字下划线");
                }
                if (is_numeric(substr($post['name'], 0, 1))) {
                    $this->error("字段不能以数字开始");
                }
                //绑定sql
                if (!empty($post['bindtype']) && $post['bindtype'] == 'sql' && $post['showoption'] != 'window') {

                    Db::startTrans();
                    try {
                        $res = Db::query($post['bindtext']);
                        // 提交事务
                        Db::commit();
                    } catch (\Exception $e) {
                        $this->error($e->getMessage());
                        // 回滚事务
                        Db::rollback();
                    }
                    $content = [];
                    foreach ($res as $k => $v) {
                        $content[$v[$post['sqlval']]] = $v[$post['sqlname']];
                    }
                    $post['content'] =  json_encode($content, JSON_UNESCAPED_UNICODE);
                }

                //树状显示 select
                if (!empty($post['bindtype']) && $post['bindtype'] == 'tree') {
                    $tree       = new Tree();
                    $tree->icon = ['&nbsp;&nbsp;│', '&nbsp;&nbsp;├─', '&nbsp;&nbsp;└─'];
                    $tree->nbsp = '&nbsp;&nbsp;';
                    $newCategories = [];
                    Db::startTrans();
                    try {
                        if ($post['sqltreeid'] >= 0 && isset($post['sqltreeid']) && strlen($post['sqltreeid']) > 0) {
                            $sql = $post['bindtexts'] . 'where parent_id =' . $post['sqltreeid'];
                        } else {
                            $sql = $post['bindtexts'];
                        }
                        dump($sql);die;
                        $res = Db::query($sql);
                        foreach ($res as $item) {
                            $item['selected'] = $post['defaultvalue'] == $item['id'] ? "selected" : "";
                            array_push($newCategories, $item);
                        }
                        $tree->init($newCategories);
                        $str     = '<option value=\"{$id}\" {$selected}>{$spacer}{$name}</option>';
                        $post['content'] = $tree->getTree(0, $str);
                        // 提交事务
                        Db::commit();
                    } catch (\Exception $e) {
                        $this->error($e->getMessage());
                        // 回滚事务
                        Db::rollback();
                    }
                }
                //指定当前栏目
                if (!empty($post['bindtype']) && $post['bindtype'] == 'channel') {
                    $post['bidntext'] = $post['channel_id'] ?? 0;
                }
                $result = $this->model->edit($post);
                // $result = $this->model->allowField(true)->isupdate(true)->save($post);
                if ($result) {
                    op_log(1, '模型字段编辑提交', '编辑');
                    $this->success("修改成功!");
                } else {
                    op_log(0, '模型字段编辑提交', '编辑');
                    $this->error("修改失败");
                }
            }
        }
    }

    /**
     * 删除操作
     */
    public function delete()
    {
        $param           = $this->request->param();

        //单条删除
        if (isset($param['id'])) {
            $id           = $this->request->param('id', 0, 'intval');

            //$this->model->where('id',$id)->delete(); 
            //以上这种删除方式不触发after_delete事件
            //解决参考：https://www.baidu.com/link?url=zxH3BQ1BDdWmsdtpgJ7H_aJNWryCk1kvx9P7yq2Dbu71Jdj0GRGeJ1exipKIpATtj28bnQrkeKIlgIg1JESW_dBEQ6TdVX1P_Lw4ezXjz-y&wd=&eqid=8e07fe8d0001d3fb000000065cb553a7

            $result = $this->model->destroy($id);
            DB::name('cms_fields_bind')->where('field_id', $id)->delete();

            if ($result) {
                op_log(1, '模型字段删除', '删除');

                $this->success("删除成功！", '');
            } else {
                op_log(0, '模型字段删除', '删除');

                $this->error("删除失败！");
            }
        }

        //批量删除
        if (isset($param['ids'])) {
            $ids     = $this->request->param('ids/a');
            $result = $this->model->destroy($ids);
            DB::name('cms_fields_bind')->where('field_id', 'in', $ids)->delete();
            if ($result) {
                op_log(1, '模型字段批量删除', '删除');

                $this->success("删除成功！", '');
            } else {
                op_log(0, '模型字段批量删除', '删除');

                $this->error("删除失败！");
            }
        }
    }

    /**
     * 删除操作 栏目字段删除
     */
    public function delete_bind()
    {
        $param           = $this->request->param();

        //单条删除
        if (isset($param['id'])) {
            $id    = $this->request->param('id', 0, 'intval');

            $result = Db::name('cms_fields_bind')->where('id', $id)->delete();
            if ($result) {

                op_log(1, '栏目字段删除', '删除');
                $this->success("删除成功！", '');
            } else {
                op_log(0, '栏目字段删除', '删除');

                $this->error("删除失败！");
            }
        }

        //批量删除
        if (isset($param['ids'])) {
            $ids     = $this->request->param('ids/a');
            // dump($ids);die;
            $result =  Db::name('cms_fields_bind')->delete($ids);
            if ($result) {
                op_log(1,  '栏目字段批量删除', '删除');

                $this->success("删除成功！", '');
            } else {
                op_log(0,  '栏目字段批量删除', '删除');

                $this->error("删除失败！");
            }
        }
    }
    /**
     * 字段排序
     */
    public function listOrder()
    {
        // dump($this->model);die;
        parent::listOrders($this->model);
        $this->success("排序更新成功！", '');
    }

    /**
     * 栏目字段排序
     */
    public function listOrder_bind()
    {
        $model = new CmsFieldsBindModel();
        parent::listOrders($model);
        $this->success("排序更新成功！", '');
    }

    /**
     * 子栏目字段沿用
     *
     * @return void
     */
    public function son_bind()
    {
        $param           = $this->request->param();



        //批量添加字段
        if (isset($param['channel_id']) && isset($param['model_id'])) {
            // $ids     = $this->request->param('ids/a');
            // dump($ids);die;

            //栏目原有的字段
            $channel_fields =  Db::name('cms_fields_bind')
                ->where('channel_id', $param['channel_id'])
                ->where('status', 1)
                ->select()
                ->toArray();
            //获取栏目的所有同模型的子栏目
            // $table = cmsModel($param['channel_id']);
            $sonsId = getAllChildcateIds($param['channel_id'], 'cms_channel', $param['model_id']);
            // dump($sonsId);
            // die;
            //把原有的字段删除掉
            Db::startTrans();
            try {
                Db::name('cms_fields_bind')->where('channel_id', 'in', $sonsId)->delete();
                $newFields = [];
                foreach ($sonsId as $key => $value) {
                    foreach ($channel_fields as $k => $v) {
                        unset($channel_fields[$k]['id']);
                        $newFields[$k] = $channel_fields[$k];
                        $newFields[$k]['channel_id'] = $value;
                    }
                }
                // dump($newFields);die;
                Db::name('cms_fields_bind')->insertAll($newFields);
                $op = true;
                // 提交事务
                Db::commit();
            } catch (\Exception $e) {
                $op = false;
                $op_msg  = $e->getMessage();
                // 回滚事务
                Db::rollback();
            }


            if ($op) {
                op_log(1,  '栏目字段批量沿用', '沿用');
                $this->success("沿用成功！", '');
            } else {
                op_log(0,  '栏目字段批量沿用', '沿用');
                $this->error($op_msg);
            }
        } else {
            $this->error('缺少栏目id');
        }
    }

    /**
     * 同级栏目绑定
     *
     * @return void
     */
    public function peer_bind()
    {
        $param           = $this->request->param();
        //批量添加字段
        if (isset($param['channel_id']) && isset($param['model_id'])) {
            //栏目原有的字段
            $channel_fields =  Db::name('cms_fields_bind')
                ->where('channel_id', $param['channel_id'])
                ->where('status', 1)
                ->select()
                ->toArray();
            //获取栏目的所有同级
                        
            $parent_id = Db::name('cms_channel')->where('id',$param['channel_id'])->value('parent_id');
            $peer =  Db::name('cms_channel')
            ->where('parent_id',$parent_id)
            ->where('model_id',$param['model_id'])
            ->column('id');

            if(empty($peer))
            {
                $this->error('暂无相同模型的同级栏目');
            }
            // dump($peer);die;
            //把原有的字段删除掉
            Db::startTrans();
            try {
                Db::name('cms_fields_bind')->where('channel_id', 'in', $peer)->delete();
                $newFields = [];
                foreach ($peer as $key => $value) {
                    foreach ($channel_fields as $k => $v) {
                        unset($channel_fields[$k]['id']);
                        $channel_fields[$k]['channel_id'] = $value;
                        $newFields[] = $channel_fields[$k];
                        // $newFields[][$k]['channel_id'] = $value;
                    }
                }
                Db::name('cms_fields_bind')->insertAll($newFields);
                $op = true;
                // 提交事务
                Db::commit();
            } catch (\Exception $e) {
                $op = false;
                $op_msg  = $e->getMessage();
                // 回滚事务
                Db::rollback();
            }


            if ($op) {
                op_log(1,  '栏目字段批量沿用', '沿用');
                $this->success("沿用成功！", '');
            } else {
                op_log(0,  '栏目字段批量沿用', '沿用');
                $this->error($op_msg);
            }
        } else {
            $this->error('缺少栏目id');
        }
    }
}
