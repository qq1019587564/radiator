<?php
// +----------------------------------------------------------------------
// | 表单模型
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\controller;

use think\facade\Db;
use cmf\controller\AdminBaseController;
use app\cms\model\CmsChannelModel;
use app\cms\service\AdminChannelService;
use app\cms\model\CmsFieldsModel;
use app\cms\model\CmsFieldsBindModel;

class AdminMessageController extends AdminBaseController
{
    public function initialize()
    {
        parent::initialize();
        $this->table = 'cms_message';
        $this->lang = input('lang') ?? config('config.admin_lang');
    }
    public function index()
    {
        $param = $this->request->param();
        //搜索
        $keyword = $param['keyword'] ?? '';
        $search_fields = '';
        $search_field = CmsFieldsBindModel::where('search', 1)->where('channel_id', $param['channel_id'])->column('name');
        if (!empty($search_field)) {
            foreach ($search_field as $v) {
                $search_fields .=  $v . '|';
            }
            $search_fields = rtrim($search_fields, '|');
        }
        $order = Db::name($this->table)
            ->where($search_fields, 'like', "%{$keyword}%")
            ->where('channel_id', $param['channel_id'])
            ->where('delete_time', 0)
            // ->where('lang', $this->lang)
            ->order('create_time desc')

            ->paginate(15, false, ['query' => $param]);

        $page = $order->render();
        $order = $order->toArray()['data'];

        $pages = input('page') ?? 1;
        $total = Db::name($this->table)->where($search_fields, 'like', "%{$keyword}%")
            ->where('channel_id', $param['channel_id'])
            ->where('lang', $this->lang)->count();

        foreach ($order as $k => $v) {
            // $order[$k]['sort'] = ($pages - 1) * 15 + $k + 1;
            $order[$k]['sort'] = ($total - $k) - ($pages - 1) * 15;
            $order[$k]['create_time'] = date('Y-m-d H:i', $v['create_time']);
        }

        $order = $this->data($order, $param['channel_id']);
        //控件权限
        $this->assign('keyword', isset($param['keyword']) ? $param['keyword'] : '');
        $this->assign('order', $order);
        $this->assign('channel_id', $param['channel_id']);
        $this->assign('page', $page);
        return $this->fetch();
    }

    public function data($data, $channel_id)
    {
        $html = '';
        $fields = CmsFieldsBindModel::where('status', 1)->field('name,title')->where('channel_id', $channel_id)->order('list_order asc')->select()->toArray();
        if (is_array($data) && !empty($data)) {
            foreach ($data as $k => $v) {
                $html .= '<tr>';
                $html .=  '<td>';
                $html .=      '<input type="checkbox" class="js-check" data-yid="js-check-y" data-xid="js-check-x" name="ids[]" value="' . $v['id'] . '" title="ID:' . $v['id'] . '">';
                $html .=   '</td>';
                $html .=   '<td>' . $v['sort'] . '</td>';
                $html .=    '<td>';
                $html .=      ' <div style="padding-left: 10px;">';
                foreach ($fields as $key => $value) {
                    //    $data[$k][$value['title']] = $data[$k][$value['name']];
                    $html .= '<strong>' . $value['title'] . '：</strong>';
                    $html .= $data[$k][$value['name']] . '<br>';
                    unset($data[$k][$value['name']]);
                }
                $html .=  ' </div>';
                $html .=    '</td>';
                $html .=    '<td>' . $v['create_time'] . '</td>';
                $html .=    '<td>  <a class="btn btn-xs btn-danger js-ajax-delete" data-msg="您确定删除吗？删除后无法恢复，请注意操作！" href="' . url("delete", ["id" => $v['id'], "model_id" => $v['model_id']]) . '">' . lang('DELETE') . '</a></td>';
                $html .= '</tr>';
            }
        }
        return $html;
    }


    public function delete()
    {
        $param           = $this->request->param();

        //单条删除
        if (isset($param['id'])) {
            $id           = $this->request->param('id', 0, 'intval');
            Db::startTrans();
            try {
                $resultPortal = Db::name($this->table)
                    ->where('id', $id)
                    ->update(['delete_time' => 0]);
                // 提交事务
                Db::commit();
            } catch (\Exception $e) {
                $this->error($e->getMessage());
                // 回滚事务
                Db::rollback();
            }

            if ($resultPortal) {
                op_log(1, '删除留言id:' . $id,   '删除');
                $this->success("删除成功！", '');
            }
            op_log(0, '删除留言id:' . $id,   '删除');
            $this->success("删除失败！", '');
        }

        //批量删除
        if (isset($param['ids'])) {
            $ids     = $this->request->param('ids/a');
            // dump($ids);die;
            Db::startTrans();
            try {
                $result = Db::name($this->table)
                    ->where('id',  'in', $ids)
                    ->update(['delete_time' => time()]);
                // 提交事务
                Db::commit();
            } catch (\Exception $e) {
                $this->error($e->getMessage());
                // 回滚事务
                Db::rollback();
            }
            if ($result) {
                op_log(1, '留言批量删除',  '批量删除');
                $this->success("批量删除成功！", '');
            }
            op_log(0, '留言批量删除',  '批量删除');
            $this->success("批量删除失败！", '');
        }
    }
}
