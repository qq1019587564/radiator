<?php
// +----------------------------------------------------------------------
// | 栏目管理
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\controller;

use think\Request;
use cmf\controller\AdminBaseController;
use app\admin\model\RouteModel;
use app\admin\model\RecycleBinModel;
use app\cms\model\CmsChannelModel;
use app\cms\model\CmsArticleModel;
use app\cms\model\CmsPageModel;
use app\admin\model\ThemeModel;
use app\cms\model\CmsModelModel;
use Overtrue\Pinyin\Pinyin;
use app\cms\service\AdminChannelService;
use tree\Tree;
use think\facade\Db;



class AdminChannelController extends AdminBaseController
{

    /**
     * 初始化数据
     */
    protected $model = null;
    protected $channelService = null;


    public function initialize()
    {
        parent::initialize();
        $this->model = new CmsChannelModel();
        $this->channelService = new AdminChannelService();

        $themeModel        = new ThemeModel();
        $listThemeFiles    = $themeModel->getActionThemeFiles('cms/List/index');
        $articleThemeFiles = $themeModel->getActionThemeFiles('cms/Article/index');
        $pageThemeFiles = $themeModel->getActionThemeFiles('cms/Page/index');
        $this->assign('list_theme_files', $listThemeFiles);
        $this->assign('article_theme_files', $articleThemeFiles);
        $this->assign('page_theme_files', $pageThemeFiles);
    }

    /**
     * 栏目管理首页
     */
    public function index()
    {
        $keyword             = $this->request->param('keyword');
        if (empty($keyword)) {
            $channelTree = AdminChannelService::adminChannelTableTree();
            $this->assign('channel_tree', $channelTree);
        } else {
            $channels = $this->model->where('name', 'like', "%{$keyword}%")
                ->where('delete_time', 0)->select();
            $this->assign('channels', $channels);
        }
        $this->assign('keyword', $keyword);
        return $this->fetch();
    }

    /**
     * 添加栏目
     */
    public function add()
    {
        $parentId            = $this->request->param('parent', 0, 'intval');
        $channelTree      = $this->channelService->adminChannelTree($parentId);
        //模型列表
        $modelList = CmsModelModel::select()->toArray();
        $this->assign('channel_tree', $channelTree);
        $this->assign('modelList', $modelList);
        return $this->fetch();
    }


    /**
     * 栏目添加提交
     */
    public function addPost()
    {
        $data = $this->request->param();

        $data['user_id'] = cmf_get_current_admin_id();

        $result = $this->validate($data, 'AdminChannel');

        if ($result !== true) {
            $this->error($result);
        }

        $result = $this->model->add($data);

        if ($result === false) {
            op_log(0, '添加栏目-' . $data['name'], '添加');

            $this->error('栏目添加失败');
        }
        op_log(1, '添加栏目-' . $data['name'], '添加');

        $this->success('添加成功!', url('AdminChannel/index'));
    }


    /**
     * 编辑栏目
     */
    public function edit()
    {
        $id = $this->request->param('id', 0, 'intval');
        if ($id > 0) {
            $channel            = $this->model->find($id)->toArray();
            $parentId            = $channel['parent_id'];
            $channelTree      = $this->channelService->adminChannelTree($parentId, $id, 0);
            $modelList = CmsModelModel::select()->toArray();
            $this->assign('channel_tree', $channelTree);
            $this->assign('modelList', $modelList);
            $this->assign($channel);
            
            return $this->fetch();
        } else {
            $this->error('操作错误!');
        }
    }

    /**
     * 编辑栏目提交
     */
    public function editPost()
    {
        $data = $this->request->param();
        // $validate = new \app\cms\validate\AdminChannelValidate;
        // $result = $validate->scene('edit')->check($data);
        $result = $this->validate($data, 'AdminChannel');
        $data['user_id'] = cmf_get_current_admin_id();
        if ($result !== true) {
            $this->error($result);
        }
        $result = $this->model->edit($data);

        if ($result === false) {
            op_log(0, '编辑栏目-' . $data['name'], '编辑');
            $this->error('保存失败!');
        }
        op_log(1, '编辑栏目-' . $data['name'], '编辑');
        // //更新导航栏的栏目
        // $where = [];
        // $navMenus     = $this->model->where($where)
        //     ->field('id,name,route,parent_id')
        //     ->where('delete_time', 0)
        //     ->where('status', 1)
        //     ->order('list_order ASC')
        //     ->select()
        //     ->toArray();
        // // dump($navMenus);die;
        // $navMenusTree = [];
        // if (!empty($navMenus)) {
        //     $tree = new Tree();
        //     $tree->init($navMenus);
        //     $navMenusTree = $tree->getTreeArray(0, 0);
        // }
        // cache('navMenusTree', $navMenusTree, 3600 * 24 * 7);

        // //更新单个栏目
        // $channel = $this->model->where('id', $data['id'])->find();
        // cache($data['id'] . 'list', $channel, 3600 * 24 * 7);

        // cache($data['id'] . 'detail', $channel, 3600 * 24 * 7);


        $this->success('保存成功!');
    }

    /**
     * 栏目排序
     */
    public function listOrder()
    {
        parent::listOrders('cms_channel');
        op_log(1, '栏目排序', '排序');
        $this->success("排序更新成功！", '');
    }


    /**
     * 栏目显示隐藏
     */
    public function toggle()
    {
        $data                = $this->request->param();
        $ids                 = $this->request->param('ids/a');
        if (isset($data['ids']) && !empty($data["display"])) {
            $this->model->where('id', 'in', $ids)->update(['status' => 1]);
            op_log(1, '批量设置栏目显示', '设置显示');
            $this->success("更新成功！");
        }
        if (isset($data['ids']) && !empty($data["hide"])) {
            op_log(1, '批量设置栏目隐藏', '设置隐藏');
            $this->model->where('id', 'in', $ids)->update(['status' => 0]);
            $this->success("更新成功！");
        }
    }


    /**
     * 栏目删除
     */
    public function delete()
    {
        $id                  = $this->request->param('id');
        //获取删除的内容
        $findchannel = $this->model->where('id', $id)->find();

        if (empty($findchannel)) {
            $this->error('栏目不存在!');
        }
        //判断此栏目有无子栏目（不算被删除的子栏目）
        $channelChildrenCount = $this->model->where(['parent_id' => $id, 'delete_time' => 0])->count();

        if ($channelChildrenCount > 0) {
            $this->error('此栏目有子栏目无法删除!');
        }

        $articleCount = CmsArticleModel::where('channel_id', $id)->count();

        if ($articleCount > 0) {
            $this->error('此栏目有文章无法删除!');
        }

        $data   = [
            'object_id'   => $findchannel['id'],
            'create_time' => time(),
            'table_name'  => 'cms_channel',
            'name'        => $findchannel['name']
        ];
        $result = $this->model
            ->where('id', $id)
            ->update(['delete_time' => time()]);
        if ($result) {
            RecycleBinModel::insert($data);
            //删除路由
            $routeModel = new RouteModel();
            $routeModel->where('url', $findchannel['route'])->update(['status' => 0]);
            $routeModel->getRoutes(true);
            op_log(1, '删除栏目-' . $findchannel['name'], '删除');
            $this->success('删除成功!');
        } else {
            op_log(0, '删除栏目-' . $findchannel['name'], '删除');
            $this->error('删除失败');
        }
    }
}
