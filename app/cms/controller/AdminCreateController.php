<?php
// +----------------------------------------------------------------------
// | 文件同步管理
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
namespace app\cms\controller;

use app\form\model\FormModel;
use cmf\controller\AdminBaseController;
use app\form\service\AdminFormService;
use think\facade\Db;
use app\domain\model\DomainModel;
use think\facade\View;
use app\cms\model\CmsChannelModel;
use app\cms\model\CmsArticleModel;
use app\cms\model\CmsCaseModel;
use app\cms\model\CmsTagModel;
use cmf\controller\HomeBaseController;
use app\cms\model\CmsPageModel;
use think\facade\Console;
// use swoole\WebsocketClient;
use think\queue\Job;

require CMF_ROOT . '/extend/swoole/sw_client.php';

class AdminCreateController extends AdminBaseController
{
    public function index()
    {
        return $this->fetch();
    }


    /**
     * 生成详情页
     *
     * @param Job $job
     * @param array $data
     * @return void
     */
    public function detail(Job $job, $data)
    {
        //首页、列表、单页、详情页
        //公共变量
        //基本参数
        $setting = Db::name('setting')->select()->toArray();
        foreach ($setting as $k => $v) {
            switch ($v['type']) {
                case '3':
                    $setting[$k]['value'] = cmf_get_image_url($v['value']);
                    break;
            }
            $setting[$v['read_name']] = $setting[$k];
            unset($setting[$k]);
        }
        View::share('setting', $setting ?? '');

        $HomeBaseController = new HomeBaseController($this->app);
        //域名
        $domain  = 'http://' . request()->host();

        $HomeBaseController->assign('article', $data);
        $channel = CmsChannelModel::where('id', $data['channel_id'])->where(['status' => 1, 'delete_time' => 0])->find();
        if(empty($channel))
        {
            $job->delete();
            return true;
        }
        $HomeBaseController->assign('channel', $channel);
        //模板地址
        $template = root_path() . 'public' . $channel['route'] . $data['id'] . '.html';
        $templateDir = root_path() . 'public' . $channel['route'];
        //跳转的路径
        $templateUrl = $domain . '/public' . $channel['route']   . $data['id'] . '.html';
        //判断静态界面是否存在
        beforeBuild($templateUrl, $templateDir, $template);
        $html = $HomeBaseController->fetch('/' . $channel['article_tpl']);
        afterBuild($templateUrl, $template, $html);
        $cacheData[] = '详情页页生成完成' . date('Y-m-d H:i:s', time());
        Cache('queue1', $cacheData, 3000);
    }














    public function addPost()
    {
        $data = input();

        $client = new \WebSocketClient('127.0.0.1', '9501');

        // $this->success('功能开发中');
        foreach ($data as $k => $v) {
            if (!file_exists($v)) {
                $this->error($v . '   文件不存在');
            } else {
                $sendData = [
                    'dir' => $v
                ];
                // dump($sendData);die;
                $client->sendData(json_encode($sendData));
                // die;
                // $output = Console::call('sync', [$v]);
                // $a = exec('sh /root/shell/file_sync.sh '.$v,$out,$status);
                // dump($status);die;
                // file_put_contents('/www/wwwroot/ebaidu/file_rsync_log.txt',date('Y-m-d H:i:s',time()).'  '.$v.PHP_EOL,FILE_APPEND);
                // file_put_contents('/www/wwwroot/ebaidu/file.txt',$v.PHP_EOL,FILE_APPEND);
                // dump($status);die;
            }
        }
        $this->success('文件同步成功');
    }

    //电脑
    public function index1()
    {
        //基本参数
        $setting = Db::name('setting')->select()->toArray();
        foreach ($setting as $k => $v) {
            switch ($v['type']) {
                case '3':
                    $setting[$k]['value'] = cmf_get_image_url($v['value']);
                    break;
            }
            $setting[$v['read_name']] = $setting[$k];
            unset($setting[$k]);
        }
        View::share('setting', $setting ?? '');

        $HomeBaseController = new HomeBaseController($this->app);
        //域名
        $domain  = 'http://' . request()->host();


        //生成首页
        $url = request()->url(true);
        //模板生成的地址
        $templateC = root_path() . 'public/index.html';
        //跳转的路径
        $templateUrl = $url . 'index.html';
        //判断静态界面是否存在
        beforeBuild($url, root_path() . 'public', $templateC);
        $html = $HomeBaseController->fetch(':index');
        //生成静态界面
        afterBuild($templateUrl, $templateC, $html);


        //生成列表页
        $channel = CmsChannelModel::where('type', 1)->where('delete_time', 0)->where('status', 1)->select()->toArray();
        $domain  = request()->host();
        foreach ($channel as $k => $v) {
            $HomeBaseController->assign('channel', $v);
            //模板地址
            $template = root_path() . 'public' . $v['route'] . 'index.html';
            $templateDir = root_path() . 'public' . $v['route'];
            //跳转的路径
            $templateUrl = $domain . '/public' . $v['route'] . 'index.html';
            //判断静态界面是否存在
            beforeBuild($templateUrl, $templateDir, $template);
            $html = $HomeBaseController->fetch('/' . $v['list_tpl']);
            //生成静态界面
            afterBuild($templateUrl, $template, $html);
        }

        //生成详情页
        //生成新闻的
        $CmsArticlelModel = new CmsArticleModel();
        $article = $CmsArticlelModel->where(['status' => 1, 'delete_time' => 0])->select()->toArray();
        foreach ($article as $k => $v) {
            $channel = CmsChannelModel::where('id', $v['channel_id'])->find();
            $HomeBaseController->assign('channel', $channel);
            if (!empty($v)) {
                $tags = CmsTagModel::where('id', 'in', $v['tag_ids'])->where('type', 1)->field('id,title')->select()->toArray();
                // $tags = implode('|', $tags);
                $v['tag_ids'] = $tags;
            }
            $HomeBaseController->assign('article', $v);
            //模板地址
            $template = root_path() . 'public/news/' . $v['id'] . '.html';
            $templateDir = root_path() . 'public/news/';
            //跳转的路径
            $templateUrl = $domain . '/public/news/'  . $v['id'] . '.html';
            //判断静态界面是否存在
            beforeBuild($templateUrl, $templateDir, $template);
            // TODO 需要填入的模板数据
            $html = $HomeBaseController->fetch('/' . $channel['article_tpl']);
            //生成静态界面
            afterBuild($templateUrl, $template, $html);
        }

        //生成案例
        $CmsCaseModel = new CmsCaseModel();
        $article = $CmsCaseModel->where(['status' => 1, 'delete_time' => 0])->select()->toArray();
        foreach ($article as $k => $v) {
            $channel = CmsChannelModel::where('id', $v['channel_id'])->find();
            $HomeBaseController->assign('channel', $channel);
            $HomeBaseController->assign('article', $v);
            if (!empty($v)) {
                $tags = CmsTagModel::where('id', 'in', $v['tag_ids'])->where('type', 2)->field('id,title')->select()->toArray();
                // $tags = implode('|', $tags);
                // $article[$k]['tag_ids'] = $tags;
                $v['tag_ids'] = $tags;
            }
            $HomeBaseController->assign('article', $v);
            //模板地址
            $template = root_path() . 'public/case/' . $v['id'] . '.html';
            $templateDir = root_path() . 'public/case/';
            //跳转的路径
            $templateUrl = $domain . '/public/case/' . $v['id'] . '.html';
            //判断静态界面是否存在
            beforeBuild($templateUrl, $templateDir, $template);
            $html = $HomeBaseController->fetch('/' . $channel['article_tpl']);
            //生成静态界面
            afterBuild($templateUrl, $template, $html);
        }

        //生成单页
        $page = CmsPageModel::where('delete_time', 0)->where('status', 1)->where('id', '>', 1)->select()->toArray();
        foreach ($page as $k => $v) {
            $channel = CmsChannelModel::where('id', $v['channel_id'])->where('delete_time', 0)->where('status', 1)->where('type', 2)->find();
            if (!empty($channel)) {
                $HomeBaseController->assign('channel', $channel);

                if ($channel['page_tpl'] == 'page_more') {
                    $channel['page_tpl'] = 'page_about';
                }
                $page_tpl = $channel['page_tpl'];

                //模板地址
                $template = root_path() . 'public' . $channel['route'] . 'index.html';
                $templateDir = root_path() . 'public' . $channel['route'];
                //跳转的路径
                $templateUrl = $domain . '/public' . $channel['route'] . 'index.html';
                //判断静态界面是否存在
                beforeBuild($templateUrl, $templateDir, $template);
                $html = $HomeBaseController->fetch('/' . $page_tpl);
                //生成静态界面
                afterBuild($templateUrl, $template, $html);
            } else {
                continue;
            }
        }

        //生成标签页
        $tag = CmsTagModel::where('delete_time', 0)->select()->toArray();
        foreach ($tag as $k => $v) {
            $id = $v['id'];
            //生成新闻标签页
            $sql_category = "FIND_IN_SET('{$id}',tag_ids) ";
            $where[] = ['', 'exp', Db::raw($sql_category)];
            $where[] = ['status', '=', 1];
            $where[] = ['delete_time', '=', 0];
            if ($v['type'] == 2) {
                //案例
                $route = 'case';
                $list = CmsCaseModel::where($where)->select()->toArray();
            } else {
                //文章
                $route = 'news';
                $list = CmsArticleModel::where($where)->select()->toArray();
            }
            $HomeBaseController->assign('list', $list);
            $HomeBaseController->assign('route', $route);
            $HomeBaseController->assign('page', $v);
            //模板地址
            $template = root_path() . 'public/tag/' . $v['id'] . '/index.html';
            $templateDir = root_path() . 'public/tag/' . $v['id'];
            //跳转的路径
            $templateUrl = $domain . '/public/tag/' . $v['id'] . '.html';
            //判断静态界面是否存在
            beforeBuild($templateUrl, $templateDir, $template);
            $html = $HomeBaseController->fetch('/tag');
            //生成静态界面
            afterBuild($templateUrl, $template, $html);
        }
    }
}
