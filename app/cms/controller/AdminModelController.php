<?php
// +----------------------------------------------------------------------
// | 模型管理
// +----------------------------------------------------------------------
// | zsh 
// +----------------------------------------------------------------------
namespace app\cms\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;
use app\cms\model\CmsModelModel;

class AdminModelController extends AdminBaseController
{

    /**
     * Model模型对象
     */
    protected $model = null;

    public function initialize() 
    {
        parent::initialize();
        $this->model = new CmsModelModel();
    }


    /**
     * 内容模型列表
     */
    public function index()
    {
        $list = $this->model
            //->where($condition)
            //->where($where)
            //->order($sort, $order)
            //->limit($offset, $limit)
            //->select();
            ->paginate(10);
        $this->assign('modelList', $list);
        $this->assign('page', $list->render());
        return $this->fetch();
    }

    /**
     * 添加操作
     */
    public function add()
    {
        $defaultTheme = config('template.cmf_default_theme');
        if ($temp = session('cmf_default_theme')) {
            $defaultTheme = $temp;
        }
        //单页模板
        $channeltpl = Db::name('theme_file')->where('theme', $defaultTheme)->where('file', 'like', '%page%')->field('file')->order('list_order ASC')->select()->toArray();

        //列表页模板
        $listtpl = Db::name('theme_file')->where('theme', $defaultTheme)->where('file', 'like', '%list%')->field('file')->order('list_order ASC')->select()->toArray();

        //详情页模板
        $showtpl = Db::name('theme_file')->where('theme', $defaultTheme)->where('file', 'like', '%show%')->field('file')->order('list_order ASC')->select()->toArray();

        $this->assign("channeltpl", $channeltpl);
        $this->assign("listtpl", $listtpl);
        $this->assign("showtpl", $showtpl);
        return $this->fetch();
    }

    /**
     * 提交操作
     */
    public function addPost()
    {
        if ($this->request->isPost()) {

            $data = $this->request->param();
            $post = $data['post'];

            $result = $this->validate($data, 'AdminModel');
            if ($result !== true) {
                $this->error($result);
            } else {
                $result = $this->model->add($post);
                if ($result) {
                    op_log(1, '添加模型-'.$post['name'], '添加');
                    $this->success("添加成功!", url("AdminModel/index"));
                } else {
                    op_log(0, '添加模型-'.$post['name'], '添加');
                    $this->error("添加失败");
                }
            }
        }
    }

    /**
     * 编辑操作
     */
    public function edit()
    {
        $id = $this->request->param('id', 0, 'intval');
        if ($id > 0) {
            $data            = $this->model->find($id)->toArray();

            $defaultTheme = config('template.cmf_default_theme');
            if ($temp = session('cmf_default_theme')) {
                $defaultTheme = $temp;
            }
            //栏目页模板
            $channeltpl = Db::name('theme_file')->where('theme', $defaultTheme)->where('file', 'like', ['%channel%', '%list%'], 'OR')->field('file')->order('list_order ASC')->select()->toArray();

            //列表页模板
            $listtpl = Db::name('theme_file')->where('theme', $defaultTheme)->where('file', 'like', '%list%')->field('file')->order('list_order ASC')->select()->toArray();

            //详情页模板
            $showtpl = Db::name('theme_file')->where('theme', $defaultTheme)->where('file', 'like', '%detail%')->field('file')->order('list_order ASC')->select()->toArray();

            $this->assign("channeltpl", $channeltpl);
            $this->assign("listtpl", $listtpl);
            $this->assign("showtpl", $showtpl);
            $this->assign("post", $data);
            return $this->fetch();
        } else {
            $this->error('操作错误!');
        }
    }

    /**
     * 编辑模型提交
     */
    public function editPost()
    {
        if ($this->request->isPost()) {

            $data = $this->request->param();
            $post = $data['post'];
            unset($post['table']); //表名不能修改

            $result = $this->validate($data, 'AdminModel');
            if ($result !== true) {
                $this->error($result);
            } else {
                //判断
                $count = Db::name('cms_model')->where([
                    ["name", "=", $post['name']],
                    ["id", "<>", $post["id"]]
                ])->count();
                if ($count > 0) {
                    $this->error("该模型名称已存在！");
                }
                $result = $this->model->update($post);
                if ($result) {
                    op_log(1, '编辑'.$data['table'].'模型', '编辑');
                    $this->success("编辑成功!", url("AdminModel/index"));
                } else {
                    op_log(0, '编辑'.$data['table'].'模型', '编辑');
                    $this->error("编辑失败");
                }
            }
        }
    }


    /**
     * 删除操作
     */
    public function delete()
    {
        $param           = $this->request->param();

        //单个id删除
        if (isset($param['id'])) {
            $id           = $this->request->param('id', 0, 'intval');
            $result = $this->model->destroy($id);
            if ($result) {
                op_log(1, '删除模型id:'.$id, '删除');

                $this->success("删除成功！", '');
            } else {
                op_log(0, '删除模型id:'.$id, '删除');

                $this->error("删除失败！");
            }
        }

        //多个id删除
        if (isset($param['ids'])) {
            $ids     = $this->request->param('ids/a');
            $result = $this->model->destroy($ids);
            if ($result) {
                op_log(1, '批量删除模型', '删除');
                $this->success("删除成功！", '');
            } else {
                op_log(0, '批量删除模型', '删除');
                $this->error("删除失败！");
            }
        }
    }
}
