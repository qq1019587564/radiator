<?php
// +----------------------------------------------------------------------
// | 生成更新管理
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
namespace app\cms\controller;

use app\form\model\FormModel;
use cmf\controller\AdminBaseController;
use think\facade\Db;
use think\facade\View;
use app\cms\model\CmsChannelModel;
use app\cms\model\CmsArticleModel;
use app\cms\model\CmsImageModel;
use cmf\controller\HomeBaseController;
use app\cms\model\CmsPageModel;
use think\facade\Console;
use think\facade\Queue;
use app\queue\controller\CreateController;
use think\facade\Request;

class AdminUpdateController extends AdminBaseController
{
    public function index()
    {
        $this->assign('web_type', config('config.web_type'));
        return $this->fetch();
    }

    /**
     * 生成整站
     *
     * @return void
     */
    public function create()
    {
        //首页、列表、单页、详情页

        //公共变量
        //基本参数
        $setting = Db::name('setting')->select()->toArray();
        foreach ($setting as $k => $v) {
            switch ($v['type']) {
                case '3':
                    $setting[$k]['value'] = cmf_get_image_url($v['value']);
                    break;
            }
            $setting[$v['read_name']] = $setting[$k];
            unset($setting[$k]);
        }
        View::share('setting', $setting ?? '');



        //生成首页START
        $HomeBaseController = new HomeBaseController($this->app);
        //域名
        $domain  = request()->domain();
        // //生成首页
        // $url = request()->url(true);
        // //模板生成的地址
        // $template = root_path() . 'public/index.html';
        // //跳转的路径
        // $templateUrl = $url . 'index.html';
        // //判断静态界面是否存在
        // beforeBuild($url, root_path() . 'public', $template);
        // try {
        //     $html = $HomeBaseController->fetch(':index');
        // } catch (\Throwable $th) {
        //     $this->error($th->getMessage());
        // }
        // $sendData = ['url' => $templateUrl, 'temp' => $template, 'html' => $html];
        // $this->push('index', $sendData);
        //生成首页END

        //生成列表页START
        $channel = CmsChannelModel::where('type', 1)->where('delete_time', 0)->where('status', 1)->select()->toArray();
        if (!empty($channel)) {
            foreach ($channel as $k => $v) {
                $HomeBaseController->assign('channel', $v);
                //模板地址
                $template = root_path() . 'public' . $v['route'] . 'index.html';
                $templateDir = root_path() . 'public' . $v['route'];
                //跳转的路径
                $templateUrl = $domain . '/public' . $v['route'] . 'index.html';
                //判断静态界面是否存在
                $html = $HomeBaseController->fetch('/' . $v['list_tpl']);
                if (strpos($html, '/cms/admin_update/create.html?type=pc&amp;') !== false) {
                    preg_match_all('/href=\"\/cms\/admin_update\/create.html.*?page=([0-9]*)\"/', $html, $match);
                    $page_href = array_unique($match[1]);
                    $max = max($page_href);
                    for ($ii = 1; $ii <= $max; $ii++) {
                        $aaurl = $domain . $v['route'] . '?page=' . $ii;
                        $aaurl = $domain."/cms/admin_update/page.html?page=".$ii;
                        dump($aaurl);die;
                        $html = cmf_curl_get($aaurl);
                        for ($iii = 1; $iii <= $max; $iii++) {
                            $fenyeNewname = 'list_' . $v['id'] . '_' . $iii . '.html';
                            $html = str_replace('?page=' . $iii, '/' . $fenyeNewname, $html);
                        }
                        $template = root_path() . 'public' . $v['route'] . $fenyeNewname;
                        beforeBuild($templateUrl, $templateDir, $template);
                        $sendData = ['url' => $templateUrl, 'temp' => $template, 'html' => $html];
                        dump($sendData);
                        $this->push('list', $sendData);
                        die;
                    }
                    dump($max);
                    die;

                    foreach ($page_href as $k1 => $v1) {
                        // dump($aaurl);die;
                        //拼接新的名字
                    }
                    dump($template);
                    die;
                }
                $sendData = ['url' => $templateUrl, 'temp' => $template, 'html' => $html];
                // beforeBuild($templateUrl, $templateDir, $template);
                // $this->push('list', $sendData);
            }
        }
        //生成列表页END

        die;

        //生成详情页
        //生成文章模型  START
        $CmsArticlelModel = new CmsArticleModel();
        $article = $CmsArticlelModel->where(['status' => 1, 'delete_time' => 0])->select()->toArray();
        if (!empty($article)) {
            foreach ($article as $k => $v) {
                // //当轮到该任务时，系统将生成一个该类的实例，并默认调用其 fire 方法
                // $jobHandlerClassName = 'app\cms\controller\AdminCreateController@detail';
                // //将该任务推送到消息队列，等待对应的消费者去执行
                // Queue::push($jobHandlerClassName, $v);
                $channel = CmsChannelModel::where('id', $v['channel_id'])->where(['status' => 1, 'delete_time' => 0])->find();
                if (!empty($channel)) {
                    $HomeBaseController->assign('channel', $channel);
                    $HomeBaseController->assign('article', $v);
                    //模板地址
                    $template = root_path() . 'public' . $channel['route'] . $v['id'] . '.html';
                    $templateDir = root_path() . 'public' . $channel['route'];
                    //跳转的路径
                    $templateUrl = $domain . '/public' . $channel['route']   . $v['id'] . '.html';
                    //判断静态界面是否存在
                    beforeBuild($templateUrl, $templateDir, $template);
                    // TODO 需要填入的模板数据
                    $html = $HomeBaseController->fetch('/' . $channel['article_tpl']);
                    $sendData = ['url' => $templateUrl, 'temp' => $template, 'html' => $html];
                    $this->push('article', $sendData);
                } else {
                    continue;
                }
            }
        }
        //生成文章模型 END

        //生成图集模型  START
        $CmsImageModel = new CmsImageModel();
        $article = $CmsImageModel->where(['status' => 1, 'delete_time' => 0])->select()->toArray();
        if (!empty($article)) {
            foreach ($article as $k => $v) {
                $channel = CmsChannelModel::where('id', $v['channel_id'])->where(['status' => 1, 'delete_time' => 0])->find();
                if (!empty($channel)) {
                    $HomeBaseController->assign('channel', $channel);
                    $HomeBaseController->assign('article', $v);
                    //模板地址
                    $template = root_path() . 'public' . $channel['route'] . $v['id'] . '.html';
                    $templateDir = root_path() . 'public' . $channel['route'];
                    //跳转的路径
                    $templateUrl = $domain . '/public' . $channel['route']   . $v['id'] . '.html';
                    //判断静态界面是否存在
                    beforeBuild($templateUrl, $templateDir, $template);
                    // TODO 需要填入的模板数据
                    $html = $HomeBaseController->fetch('/' . $channel['article_tpl']);
                    $sendData = ['url' => $templateUrl, 'temp' => $template, 'html' => $html];
                    $this->push('image', $sendData);
                } else {
                    continue;
                }
            }
        }
        //生成图集模型 END

        //生成单页 START
        $page = CmsPageModel::where('delete_time', 0)->select()->toArray();
        if (!empty($page)) {
            foreach ($page as $k => $v) {
                $channel = CmsChannelModel::where('id', $v['channel_id'])->where('delete_time', 0)->where('status', 1)->where('type', 2)->find();
                if (!empty($channel)) {
                    $HomeBaseController->assign('channel', $channel);
                    $page_tpl = $channel['page_tpl'];
                    //模板地址
                    $template = root_path() . 'public' . $channel['route'] . 'index.html';
                    $templateDir = root_path() . 'public' . $channel['route'];
                    //跳转的路径
                    $templateUrl = $domain . '/public' . $channel['route'] . 'index.html';
                    //判断静态界面是否存在
                    beforeBuild($templateUrl, $templateDir, $template);
                    $html = $HomeBaseController->fetch('/' . $page_tpl);
                    //生成静态界面
                    $sendData = ['url' => $templateUrl, 'temp' => $template, 'html' => $html];
                    $this->push('page', $sendData);
                } else {
                    continue;
                }
            }
        }
        //生成单页 END

        $this->success('队列添加成功');
    }


    /**
     * 队列推送
     *
     * @param string $type  任务类型
     * @param array $data 队列数据
     * @return void
     */
    public function push($type, $data)
    {
        //当轮到该任务时，系统将生成一个该类的实例，并默认调用其 fire 方法
        $jobHandlerClassName = 'app\queue\controller\CreateController@' . $type;
        //将该任务推送到消息队列，等待对应的消费者去执行
        $isPushed = Queue::push($jobHandlerClassName, $data);
    }


    public function page()
    {
        return 2;
        //公共变量
        //基本参数
        $setting = Db::name('setting')->select()->toArray();
        foreach ($setting as $k => $v) {
            switch ($v['type']) {
                case '3':
                    $setting[$k]['value'] = cmf_get_image_url($v['value']);
                    break;
            }
            $setting[$v['read_name']] = $setting[$k];
            unset($setting[$k]);
        }
        View::share('setting', $setting ?? '');


        $HomeBaseController = new HomeBaseController($this->app);
        //域名
        $domain  = request()->domain();
        //生成列表页START
        $channel = CmsChannelModel::where('type', 1)->where('delete_time', 0)->where('status', 1)->select()->toArray();
        if (!empty($channel)) {
            foreach ($channel as $k => $v) {
                $HomeBaseController->assign('channel', $v);
                //模板地址
                $template = root_path() . 'public' . $v['route'] . 'index.html';
                $templateDir = root_path() . 'public' . $v['route'];
                //跳转的路径
                $templateUrl = $domain . '/public' . $v['route'] . 'index.html';
                //判断静态界面是否存在
                $html = $HomeBaseController->fetch('/' . $v['list_tpl']);
                if (strpos($html, '/cms/admin_update/create.html?type=pc&amp;') !== false) {
                    preg_match_all('/href=\"\/cms\/admin_update\/create.html.*?page=([0-9]*)\"/', $html, $match);
                    $page_href = array_unique($match[1]);
                    $max = max($page_href);
                    for ($ii = 1; $ii <= $max; $ii++) {
                        $aaurl = $domain . $v['route'] . '?page=' . $ii;
                        $html = cmf_curl_get($aaurl);
                        for ($iii = 1; $iii <= $max; $iii++) {
                            $fenyeNewname = 'list_' . $v['id'] . '_' . $iii . '.html';
                            $html = str_replace('?page=' . $iii, '/' . $fenyeNewname, $html);
                        }
                        $template = root_path() . 'public' . $v['route'] . $fenyeNewname;
                        beforeBuild($templateUrl, $templateDir, $template);
                        $sendData = ['url' => $templateUrl, 'temp' => $template, 'html' => $html];
                        dump($sendData);
                        $this->push('list', $sendData);
                        die;
                    }
                    dump($max);
                    die;

                    foreach ($page_href as $k1 => $v1) {
                        // dump($aaurl);die;
                        //拼接新的名字
                    }
                    dump($template);
                    die;
                }
            }
        }
    }
    /**
     * 生成整站
     *
     * @return string
     */
    public function create1()
    {
        // $output = Console::call('createpc');
        // return $output->fetch();

        try {
            //基本参数
            $setting = Db::name('setting')->select()->toArray();
            foreach ($setting as $k => $v) {
                switch ($v['type']) {
                    case '3':
                        $setting[$k]['value'] = cmf_get_image_url($v['value']);
                        break;
                }
                $setting[$v['read_name']] = $setting[$k];
                unset($setting[$k]);
            }
            View::share('setting', $setting ?? '');

            $HomeBaseController = new HomeBaseController($this->app);
            //域名
            $domain  = 'http://' . request()->host();

            //生成首页
            $url = request()->url(true);
            //模板生成的地址
            $templateC = root_path() . 'public/index.html';
            //跳转的路径
            $templateUrl = $url . 'index.html';
            //判断静态界面是否存在
            beforeBuild($url, root_path() . 'public', $templateC);
            $html = $HomeBaseController->fetch(':index');

            //生成静态界面
            // afterBuild($templateUrl, $templateC, $html);
            $output = Console::call('createpc', [$templateUrl, $templateC, $html]);

            //生成列表页
            $channel = CmsChannelModel::where('type', 1)->where('delete_time', 0)->where('status', 1)->select()->toArray();
            $domain  = request()->host();
            foreach ($channel as $k => $v) {
                $HomeBaseController->assign('channel', $v);
                //模板地址
                $template = root_path() . 'public' . $v['route'] . 'index.html';
                $templateDir = root_path() . 'public' . $v['route'];
                //跳转的路径
                $templateUrl = $domain . '/public' . $v['route'] . 'index.html';
                //判断静态界面是否存在
                beforeBuild($templateUrl, $templateDir, $template);
                $html = $HomeBaseController->fetch('/' . $v['list_tpl']);
                //生成静态界面
                // afterBuild($templateUrl, $template, $html);
                $output = Console::call('createpc', [$templateUrl, $template, $html]);
            }

            //生成详情页
            //生成文章模型
            $CmsArticlelModel = new CmsArticleModel();
            $article = $CmsArticlelModel->where(['status' => 1, 'delete_time' => 0])->select()->toArray();
            foreach ($article as $k => $v) {
                $channel = CmsChannelModel::where('id', $v['channel_id'])->find();
                $HomeBaseController->assign('channel', $channel);
                $HomeBaseController->assign('article', $v);
                //模板地址
                $template = root_path() . 'public' . $channel['route'] . $v['id'] . '.html';
                $templateDir = root_path() . 'public' . $channel['route'];
                //跳转的路径
                $templateUrl = $domain . '/public' . $channel['route']   . $v['id'] . '.html';
                //判断静态界面是否存在
                beforeBuild($templateUrl, $templateDir, $template);
                // TODO 需要填入的模板数据
                $html = $HomeBaseController->fetch('/' . $channel['article_tpl']);
                //生成静态界面
                // afterBuild($templateUrl, $template, $html);
                $output = Console::call('createpc', [$templateUrl, $template, $html]);
            }

            //生成单页
            $page = CmsPageModel::where('delete_time', 0)->where('status', 1)->where('id', '>', 1)->select()->toArray();
            foreach ($page as $k => $v) {
                $channel = CmsChannelModel::where('id', $v['channel_id'])->where('delete_time', 0)->where('status', 1)->where('type', 2)->find();
                if (!empty($channel)) {
                    $HomeBaseController->assign('channel', $channel);
                    $page_tpl = $channel['page_tpl'];
                    //模板地址
                    $template = root_path() . 'public' . $channel['route'] . 'index.html';
                    $templateDir = root_path() . 'public' . $channel['route'];
                    //跳转的路径
                    $templateUrl = $domain . '/public' . $channel['route'] . 'index.html';
                    //判断静态界面是否存在
                    beforeBuild($templateUrl, $templateDir, $template);
                    $html = $HomeBaseController->fetch('/' . $page_tpl);
                    //生成静态界面
                    // afterBuild($templateUrl, $template, $html);
                    $output = Console::call('createpc', [$templateUrl, $template, $html]);
                } else {
                    continue;
                }
            }
        } catch (\Throwable $th) {
            op_log(0, '生成整站',  '生成');
            $this->error($th->getMessage());
        }
    }
}
