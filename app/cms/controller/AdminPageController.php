<?php
// +----------------------------------------------------------------------
// | 单页模型
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);
namespace app\cms\controller;

use cmf\controller\AdminBaseController;
use app\cms\model\CmsModelModel;
use app\cms\model\CmsChannelModel;
use think\facade\Db;
use app\cms\model\CmsFieldsBindModel;
use app\cms\service\AdminChannelService;
use app\cms\model\CmsPageModel;

class AdminPageController extends AdminBaseController
{

    /**
     * 模型对象
     */
    protected $model = null;
    protected $channelModel = null;
    public function initialize() 
    {
        parent::initialize();
        //栏目模型
        $this->channelModel = new CmsChannelModel();
        //栏目接口
        $this->channelService = new AdminChannelService();
        //当前控制器模型
        $this->model = new CmsPageModel;
        //当前控制器表名
        $this->table = 'cms_page';
        //当前控制器
        $this->controller = $this->request->controller();
        //当前模型id
        $this->model_id = 5;
        //当前语言标识
        $this->lang = input('lang') ?? config('config.admin_lang');
        //模型表
        $this->cmsModel = new CmsModelModel();
        //模型字段表
        $this->CmsFieldsBindModel = new CmsFieldsBindModel();

        $this->assign('controller', $this->controller);
        $this->assign('model_id', $this->model_id);
    }


    public function index()
    {

        $id = $this->request->param('id', 0, 'intval');
        $post = $this->model->where('id', $id)->find();
        if(empty($post))
        {
            $this->error('数据错误');
        }
        //必定字段
        if(!empty($post['content']))
        {
            $post['content'] = cmf_replace_content_file_url(htmlspecialchars_decode($post['content']));
        }
        $this->assign('post', $post);
        return $this->fetch();
    }

    /**
     * 编辑单页提交
     */
    public function editPost()
    {

        if ($this->request->isPost()) {
            $data = $this->request->param();
            $post   = $data['post'];

            //数据处理
            if (empty(cms_dataPost($data, $post)['data'])) {
                $this->error(cms_dataPost($data, $post)['msg']);
            }
            $data = cms_dataPost($data, $post)['data'];
            //字段数据处理
            $data['post'] = cms_field($data['post']);
            $newData = thumb($data['post']['channel_id'], $data['post'])['data'];
            $row = $this->model->edit($newData);

            if ($row) {
                op_log(1, '编辑'.$newData['title'].'提交', '编辑');
                $this->success('保存成功!');
            } else {
                op_log(0, '编辑'.$newData['title'].'提交', '编辑');
                $this->success('保存失败!');
            }
        }
    }


    /**
     * 获取栏目字段
     * @internal
     */
    public function get_channel_fields()
    {
        $channel_id = $this->request->post('channel_id');
        $model_id = $this->request->post('model_id');
        $aid = $this->request->post('aid');
        //获取栏目信息
        $channel = $this->channelModel->find($channel_id);
        cms_get_channel_fields($channel_id, $model_id, $aid, $channel);
    }
}
