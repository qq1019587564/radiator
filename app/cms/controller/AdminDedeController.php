<?php
// +----------------------------------------------------------------------
// | 织梦数据转换管理
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\cms\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;
use think\facade\Console;
use think\facade\Config;


class AdminDedeController extends AdminBaseController
{

    /**
     * 初始化数据
     */
    protected $model = null;

    public function initialize()
    {
        parent::initialize();
    }

    public function index()
    {
        // $str = '-1 OR 2+207-207-1=0+0+0+1 -- ';
        // $check = json_encode($str);
        // if (strpos($check, 'OR') !== false || strpos($check, 'select') !== false) {
        //     dump($check);
        // }
        return $this->fetch();
    }


    public function change()
    {
        $data = input();
        // set_config($data);
        //定义动态连接信息
        $conparas = [
            // 数据库类型
            'type'            => 'mysql',
            // 服务器地址
            'hostname'        => $data['cfg_dbhost'],
            // 数据库名
            'database'        => $data['cfg_dbname'],
            // 用户名
            'username'        => $data['cfg_dbuser'],
            // 密码
            'password'        => $data['cfg_dbpwd'],
            // 端口
            'hostport'        => '3306',
            //表前缀
            'prefix'    =>  $data['cfg_dbprefix'],
        ];
        $database_config = Config::get('database');
        $database_config['connections']['dede'] = $conparas;

        $config = Config::set($database_config, 'database');


        try {
            //基本参数转换
            $sysconfig = Console::call('sysconfig');
            //栏目转换
            $arctype = Console::call('arctype');
            //文章转换
            $archives = Console::call('archives');
            //友情链接转换
            $flink = Console::call('flink');
            //幻灯片管理转换
            $myppt = Console::call('myppt');
            //留言表单管理转换
            $diyforms = Console::call('diyforms');
            //热门搜索
            $hot = Console::call('hot');
        } catch (\Exception $th) {
            $this->error($th->getMessage());
        }


        //文章模型转换
        dump($sysconfig);
        dump($arctype);
        dump($archives);
        dump($flink);
        dump($myppt);
        dump($diyforms);
        dump($hot);


        // $this->success('ok');
        // return $this->fetch();
    }
}
