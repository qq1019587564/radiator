<?php
// +----------------------------------------------------------------------
// | 内容管理
// +----------------------------------------------------------------------
// | zsh 
// +----------------------------------------------------------------------
namespace app\cms\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;
use app\cms\model\CmsModelModel;
use app\cms\model\CmsChannelModel;
use app\cms\service\AdminChannelService;

class AdminContentController extends AdminBaseController
{


    /**
     * 内容管理列表
     */
    public function index()
    {
        return $this->fetch();
    }

    public function channelPost()
    {
        $AdminChannelService = new AdminChannelService();
        $categoryTree = $AdminChannelService->channelTree();
        if ($categoryTree) {
            $this->success('ok', '', $categoryTree);
        } else {
            $this->error('TooBad');
        }
    }
}
