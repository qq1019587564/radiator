<?php
// +----------------------------------------------------------------------
// | 前台首页控制器
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
namespace app\cms\controller;

use cmf\controller\HomeBaseController;
use think\facade\Cache;
class IndexController extends HomeBaseController
{
    public function index()
    {
        //判断网站是否为pc+手机
        // 1 —— 响应式
        // 2 —— pc+手机
        // 3 —— 纯pc
        if(web_type() && cmf_is_mobile())
        {
            $this->redirect('/m',301);
        }
        return $this->fetch(":index");
    }
}