<?php
// +----------------------------------------------------------------------
// | 生成更新管理
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
namespace app\cms\controller;

use app\form\model\FormModel;
use cmf\controller\AdminBaseController;
use think\facade\Db;
use think\facade\View;
use app\cms\model\CmsChannelModel;
use app\cms\model\CmsArticleModel;
use app\cms\model\CmsImageModel;
use cmf\controller\HomeBaseController;
use app\cms\model\CmsPageModel;
use think\facade\Console;
use think\facade\Queue;
use app\queue\controller\CreateController;
use think\facade\Request;
use app\cms\service\CmsService;

class HomeUpdateController extends HomeBaseController
{
    public function index()
    {
         cmf_clear_cache();
    }

    /**
     * 生成整站
     *
     * @return void
     */
    public function create()
    {
        //首页、列表、单页、详情页
        $data = $this->request->param();
        // $key = 'e184QJHs/r+ifuGn59ojMz+5XglLYUL2qj1PBgWX5HHI4FIoDjpJhY5cpKJ1';
        $key = $data['key'];
        $key = cmf_str_decode($key, 'xinsehngtai@2021');

        if (empty($key) || $key != 'xinsehngtai@2021') {
            return msg(404, [], 'toobad');
        }

        //生成首页START
        //域名
        $domain  = request()->domain();
        // //生成首页
        $url = request()->url(true);
        //模板生成的地址
        $template = root_path() . 'public/index.html';
        //跳转的路径
        $templateUrl = $url . 'index.html';

        $html = $this->fetch(':index');

        chmod(root_path() . 'public', 0755);
        $sendData = ['url' => $templateUrl, 'temp' => $template, 'html' => $html];
        $this->push('index', $sendData);
        //生成首页END  OK

        //生成单页 END
        return msg(200, [], '首页生成成功');
        // $this->success('队列添加成功');
    }



    /**
     * 生成列表页
     *
     * @return void
     */
    public function createList()
    {
        $data = $this->request->param();
        $key = $data['key'];
        $key = cmf_str_decode($key, 'xinsehngtai@2021');

        if (empty($key) || $key != 'xinsehngtai@2021') {
            return msg(404, [], 'toobad');
        }
        //域名
        $domain  = request()->domain();
        //生成列表页START
        $channel = CmsChannelModel::where('type', 1)->where('delete_time', 0)->select()->toArray();
        if (!empty($channel)) {
            foreach ($channel as $k => $v) {
                $this->assign('channel', $v);
                //模板地址
                $template = root_path() . 'public' . $v['route'] . 'index.html';
                $templateDir = root_path() . 'public' . $v['route'];
                //跳转的路径
                $templateUrl = $domain . '/public' . $v['route'] . 'index.html';
                //判断静态界面是否存在
                $html = $this->fetch('/' . $v['list_tpl']);
                $aaurl =  $v['route'] . '?page=';
                preg_match_all('/href=\"\/cms\/home_update\/create?.*?page=([0-9]*)\"/', $html, $match);
                if (!empty($match[1])) {
                    $page_href = array_unique($match[1]);
                    $max = max($page_href);
                    for ($ii = 1; $ii <= $max; $ii++) {
                        $html = '';
                        // $aaurl = $domain . '/index.php' . $v['route'] . '?page=' . $ii;
                        $aaurl = $domain . '/cms/list/index?id=' . $v['id'] . '&page=' . $ii;
                        $tempName = 'list_' . $v['id'] . '_' . $ii . '.html';
                        // $tempName = str_replace(['/index.php','?page=' . $ii], ['', $tempName], $aaurl);
                        $template1 = root_path() . 'public' . $v['route'] . $tempName;
                        $html = cmf_curl_get($aaurl);
                        for ($iii = 1; $iii <= $max; $iii++) {
                            $dongtaiUrl = "/cms/list/index?id=" . $v['id'] . "&amp;";
                            $fenyeNewname = 'list_' . $v['id'] . '_' . $iii . '.html"';
                            $html = str_replace([$dongtaiUrl, '/index.php', 'page=' . $iii . '"'], [$v['route'], '', $fenyeNewname], $html);
                        }
                        beforeBuild($templateUrl, $templateDir, $template1);
                        $sendData = ['url' => $templateUrl, 'temp' => $template1, 'html' => $html];
                        $this->push('list', $sendData);
                        //生成有分页的列表页的index.html
                        if ($ii == 1) {
                            beforeBuild($templateUrl, $templateDir, $template);

                            $sendData = ['url' => $templateUrl, 'temp' => $template, 'html' => $html];
                            $this->push('list', $sendData);
                        }
                    }
                } else {
                    //没有分页的
                    $sendData = ['url' => $templateUrl, 'temp' => $template, 'html' => $html];
                    beforeBuild($templateUrl, $templateDir, $template);
                    $this->push('list', $sendData);
                }
            }
        }
        //生成列表页END
        return msg(200, [], '列表页生成成功');
    }


    /**
     * 生成详情页
     *
     * @return void
     */
    public function createArticle()
    {
        $data = $this->request->param();
        $key = $data['key'];
        $key = cmf_str_decode($key, 'xinsehngtai@2021');

        if (empty($key) || $key != 'xinsehngtai@2021') {
            return msg(404, [], 'toobad');
        }
        //域名
        $domain  = request()->domain();
        //生成详情页
        //生成文章模型  START
        $CmsArticlelModel = new CmsArticleModel();
        $article = $CmsArticlelModel->where(['status' => 1, 'delete_time' => 0])->select()->toArray();
        if (!empty($article)) {
            foreach ($article as $k => $v) {
                // //当轮到该任务时，系统将生成一个该类的实例，并默认调用其 fire 方法
                // $jobHandlerClassName = 'app\cms\controller\AdminCreateController@detail';
                // //将该任务推送到消息队列，等待对应的消费者去执行
                // Queue::push($jobHandlerClassName, $v);
                $channel = CmsChannelModel::where('id', $v['channel_id'])->where(['status' => 1, 'delete_time' => 0])->find();
                if (!empty($channel)) {
                    $this->assign('channel', $channel);
                    $this->assign('article', $v);
                    $prevArticle = CmsService::publishedPrevArticle($v['id']);
                    $nextArticle = CmsService::publishedNextArticle($v['id']);
                    $this->assign('prev', $prevArticle);
                    $this->assign('next', $nextArticle);

                    //模板地址
                    $template = root_path() . 'public' . $channel['route'] . $v['id'] . '.html';
                    $templateDir = root_path() . 'public' . $channel['route'];
                    //跳转的路径
                    $templateUrl = $domain . '/public' . $channel['route']   . $v['id'] . '.html';
                    //判断静态界面是否存在
                    beforeBuild($templateUrl, $templateDir, $template);
                    // TODO 需要填入的模板数据
                    $html = $this->fetch('/' . $channel['article_tpl']);
                    $sendData = ['url' => $templateUrl, 'temp' => $template, 'html' => $html];
                    $this->push('article', $sendData);
                } else {
                    continue;
                }
            }
        }
        //生成文章模型 END

        return msg(200, [], '文章生成成功');
    }

    /**
     * 生成图集
     *
     * @return void
     */
    public function createImage()
    {
        $data = $this->request->param();
        $key = $data['key'];
        $key = cmf_str_decode($key, 'xinsehngtai@2021');

        if (empty($key) || $key != 'xinsehngtai@2021') {
            return msg(404, [], 'toobad');
        }
        //域名
        $domain  = request()->domain();
        //生成图集模型  START
        $CmsImageModel = new CmsImageModel();
        $article = $CmsImageModel->where(['status' => 1, 'delete_time' => 0])->select()->toArray();
        if (!empty($article)) {
            foreach ($article as $k => $v) {
                $channel = CmsChannelModel::where('id', $v['channel_id'])->where(['status' => 1, 'delete_time' => 0])->find();
                if (!empty($channel)) {
                    $this->assign('channel', $channel);
                    $this->assign('article', $v);
                    $prevArticle = CmsService::publishedPrevArticle($v['id']);
                    $nextArticle = CmsService::publishedNextArticle($v['id']);
                    $this->assign('prev', $prevArticle);
                    $this->assign('next', $nextArticle);
                    //模板地址
                    $template = root_path() . 'public' . $channel['route'] . $v['id'] . '.html';
                    $templateDir = root_path() . 'public' . $channel['route'];
                    //跳转的路径
                    $templateUrl = $domain . '/public' . $channel['route']   . $v['id'] . '.html';
                    //判断静态界面是否存在
                    beforeBuild($templateUrl, $templateDir, $template);
                    // TODO 需要填入的模板数据
                    $html = $this->fetch('/' . $channel['article_tpl']);
                    $sendData = ['url' => $templateUrl, 'temp' => $template, 'html' => $html];
                    $this->push('image', $sendData);
                } else {
                    continue;
                }
            }
        }
        //生成图集模型 END
        return msg(200, [], '图集生成成功');
    }

    /**
     * 生成单页
     *
     * @return void
     */
    public function createPage()
    {

        $data = $this->request->param();
        $key = $data['key'];
        $key = cmf_str_decode($key, 'xinsehngtai@2021');
        if (empty($key) || $key != 'xinsehngtai@2021') {
            return msg(404, [], 'toobad');
        }
        //域名
        $domain  = request()->domain();
        //生成单页 START
        $page = CmsPageModel::where('delete_time', 0)->select()->toArray();
        if (!empty($page)) {
            foreach ($page as $k => $v) {
                $channel = CmsChannelModel::where('id', $v['channel_id'])->where('delete_time', 0)->where('type', 2)->find();
                if (!empty($channel)) {
                    $this->assign('channel', $channel);
                    $page_tpl = $channel['page_tpl'];
                    $page = CmsPageModel::where('id', $channel['page_id'])->find();
                    if (empty($page)) {
                        continue;
                    }
                    $this->assign('page', $page);
                    //模板地址
                    $template = root_path() . 'public' . $channel['route'] . 'index.html';
                    $templateDir = root_path() . 'public' . $channel['route'];
                    //跳转的路径
                    $templateUrl = $domain . '/public' . $channel['route'] . 'index.html';
                    //判断静态界面是否存在
                    beforeBuild($templateUrl, $templateDir, $template);
                    $html = $this->fetch('/' . $page_tpl);
                    //生成静态界面
                    $sendData = ['url' => $templateUrl, 'temp' => $template, 'html' => $html];
                    $this->push('page', $sendData);
                } else {
                    continue;
                }
            }
        }
        return msg(200, [], '单页生成成功');
    }




    /**
     * 队列推送
     *
     * @param string $type  任务类型
     * @param array $data 队列数据
     * @return void
     */
    public function push($type, $data)
    {
        // dump($data['temp']);
        //当轮到该任务时，系统将生成一个该类的实例，并默认调用其 fire 方法
        $jobHandlerClassName = 'app\queue\controller\CreateController@' . $type;
        //将该任务推送到消息队列，等待对应的消费者去执行
        $isPushed = Queue::push($jobHandlerClassName, $data);
    }
}
