<?php
// +----------------------------------------------------------------------
// | 参数表模型
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\hot\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class HotModel extends Model
{
    /**
     * 模型名称
     * @var string
     */
    protected $name = 'hot';


    /**
     * 添加
     */
    public function add($data)
    {
        $data['create_time']=time();
        $result = true;
        self::startTrans();
        try {
            $this->save($data);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            $result = $e->getMessage();
            $result = false;
        }
        return $result;
    }

    /**
     * 编辑
     */
    public function edit($data)
    {
        $data['update_time']=time();
        $result = true;
        self::startTrans();
        try {
            $this->update($data);
            self::commit();
        } catch (\Exception $e) {
            self::rollback();
            $result = $e->getMessage();
            $result = false;
        }
        return $result;
    }
}
