<?php
// +----------------------------------------------------------------------
// | 系统基本参数管理
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace app\hot\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;
use think\facade\Validate;
use app\hot\model\HotModel;
use think\Model;

class HotController extends AdminBaseController
{


    public function index()
    {
        $HotModel=new HotModel();
        $data = $HotModel->select()->toArray();

        foreach ($data as $k => $v) {
            if (!empty($v['url'])) {
                $url = '{$setting.' . $v['read_name'] . '.url}';
                $ah = "<a href='" . $v['url'] . "' target='__blank'>";
                $af = "</a>";
            } else {
                $ah = '';
                $af = '';
            }
            switch ($v['status']) {
                case '1':
                    $data[$k]['status'] = '显示';
                    break;
                case '2':
                    $data[$k]['status'] = '不显示';
                    break;
                default:
                    $data[$k]['status'] = '状态错误，请重新编辑！';
                    break;
            }
        }
        $this->assign('data', $data);
        return $this->fetch();
    }


    public function add()
    {
        return $this->fetch();
    }

    public function edit()
    {
        $HotModel=new HotModel();
        $datas = input();
        $data =$HotModel->where('id', $datas['id'])->find();
        if (!$data) {
            $this->error('网络繁忙');
        }
        $this->assign('data', $data);
        return $this->fetch();
    }

    public function addPost()
    {
        $HotModel=new HotModel();
        $data = $this->request->post();
        try {
            $HotModel->add($data);
        } catch (\Exception $th) {
            op_log(0, '添加参数' . $data['key'], '添加');
            $this->error($th->getMessage());
        }
        op_log(1, '添加参数' . $data['key'], '添加');
        $this->success('添加成功');
    }


    public function editPost()
    {
        $data = $this->request->post();
        $HotModel=new HotModel();
        try {
            $HotModel->edit($data);
        } catch (\Exception $th) {
            op_log(0, '编辑参数' . $data['key'], '添加');
            $this->error($th->getMessage());
        }
        op_log(1, '编辑参数' . $data['key'], '添加');
        $this->success('编辑成功');
    }

    //删除
    public function delete()
    {
        $HotModel=new HotModel();
        $data = $this->request->param();
        if (empty($data['id'])) {
            $this->error('网络繁忙');
        }
        $id = $data['id'];

        $res =  $HotModel->where('id', $id)->delete();
        if ($res === false) {
            op_log(0, '删除参数', '删除');

            $this->error('删除失败');
        }
        op_log(1, '删除参数', '删除');

        $this->success('删除成功');
    }
}
