<?php

declare(strict_types=1);

use think\facade\Db;
use cmf\model\OptionModel;
use think\facade\Env;
use think\facade\Url;
use dir\Dir;
use think\facade\Route;
use think\Loader;
use cmf\lib\Storage;
use think\facade\Hook;
use tree\Tree;
use app\cms\model\CmsFieldsBindModel;
use app\cms\model\CmsRuleModel;
use app\cms\model\CmsChannelModel;

/**
 * 记录登录日志
 *
 * @param string $user_name 用户名
 * @param string $ip
 * @param integer $status 0登录失败 1成功
 * @return void
 */
function login_log($user_name = '', $ip = '', $status = 0, $msg = '')
{
    $data['create_time'] = date('Y-m-d H:i:s', time());
    $data['user_name'] = $user_name;
    $data['ip'] = $ip;
    $data['login_status'] = $status;
    $data['msg'] = $msg;
    $data['msg'] = $msg;
    Db::name('login_log')->insert($data);
}



/**
 * 记录操作日志
 *
 * @param string $user_id  用户id
 * @param string $user_name 用户名
 * @param integer $status 操作状态
 * @param string $msg   操作信息
 * @param string $action    操作行为 添加 编辑 
 * @return void
 */
function op_log($status = 0, $msg = '', $action = '')
{
    $data['create_time'] = date('Y-m-d H:i:s', time());
    $data['ip'] = get_client_ip(0, true);
    $data['user_id'] = cmf_get_current_admin_id();
    $data['user_name'] = Db::name('user')->where('id', $data['user_id'])->value('user_login');
    $data['status'] = $status;
    $data['msg'] = $msg;
    $data['action'] = $action;
    Db::name('operation_log')->insert($data);
}

/**
 * 获取时间
 */
function getTime($date = 'Y-m-d H:i:s')
{
    return date($date, time());
}


/**
 * 判断缩略图是否过长
 */
function howLong($str, $length = 900)
{
    if (strlen($str) > $length) {
        return false;
    } else {
        return true;
    }
}

/**
 * 加密方法 用于搜索
 */
function lock($txt, $key = 'eBaidu')
{
    $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-=+";
    $txt = $txt . $key . $chars;
    $str = substr(base64_encode(md5($txt)), 5, 40);
    return $str;
}


/**
 * 判断字符串是否为英文、中文
 */
function Check_stringType($str1)
{
    $strA = trim($str1);
    $lenA = strlen($strA);
    $lenB = mb_strlen($strA, "utf-8");
    if ($lenA === $lenB) {
        return "1"; //全英文
    } else {
        if ($lenA % $lenB == 0) {
            return "2"; //全中文
        } else {
            return "3"; //中英混合
        }
    }
}

/**
 * 返回消息
 */
function msg($code, $data, $msg)
{
    return json_encode(['code' => $code, 'data' => $data, 'msg' => $msg]);
}

/**
 * 前端获取图片路径的方法
 *
 * @param string $file
 * @param string $style
 * @return void
 */
function image($img_src, $style = 'watermark')
{
    if (empty($img_src)) {
        return 'https://gz.bcebos.com/v1/gz-shengtai/default-thumbnail.png';
    }
    if (strpos($img_src, "http") === 0 || strpos($img_src, 'data:image/png;base64') !== false) {
        return $img_src;
    } else if (strpos($img_src, "/") === 0) {
        return $img_src;
    } else {
        // return request()->scheme() . '://ebd-site.cdn.bcebos.com/newbaidu/upload/' . $img_src;
        $storage = Storage::instance();
        return $storage->getImageUrl($img_src, $style);
    }
}



/**
 * 更新系统配置文件
 * @param array $data <br>如：array("URL_MODEL"=>0);
 * @return boolean
 */
function set_config($data)
{
    if (!is_array($data)) {
        return false;
    }
    $config_file = CMF_ROOT . "data/config/config.php";
    if (file_exists($config_file)) {
        $configs = include $config_file;
    } else {
        $configs = array();
    }
    $configs = array_merge($configs, $data);
    $result = file_put_contents($config_file, "<?php\treturn " . var_export($configs, true) . ";");

    return $result;
}


function GetVars($name, $type = 'REQUEST')
{
    $array = &$GLOBALS[strtoupper("_$type")];
    if (isset($array[$name])) {
        return $array[$name];
    } else {
        return null;
    }
}

/**
 * 生成前端的token
 */
function FormToken($key = 'ebaidu')
{
    $token = cmf_str_encode($key . Env::get('DATABASE_AUTHCODE'), 'ebaidu@2021.', 3600 * 3);
    return $token;
}



/**
 * cms管理 
 * 获取栏目字段 渲染页面
 *
 * @param [int] $channel_id  栏目id
 * @param [int] $model_id    模型id
 * @param [int] $aid         文章id
 * @param [array] $channel    栏目信息
 * @return void
 */
function cms_get_channel_fields($channel_id, $model_id, $aid, $channel)
{

    $table = Db::name('cms_model')->where('id', $model_id ?? $channel['model_id'])->value('table');

    if ($channel) {

        //如果是编辑操作
        $values = [];
        if ($aid) {
            $values = Db::name($channel['model']['table'])->where('id', $aid)->find();
        }
        $fields = CmsFieldsBindModel::where([
            "status"   => 1, //启用状态
            'model_id' => $channel['model_id'],
            'channel_id' => $channel_id
        ])
            ->order('list_order asc,id desc')
            ->select()
            ->toArray();
        //获取前先更新一下数据
        foreach ($fields as $k => $v) {
            //绑定sql
            if (!empty($v['bindtype']) && $v['bindtype'] == 'sql') {
                Db::startTrans();
                try {
                    $res = Db::query($v['bindtext']);
                    // 提交事务
                    Db::commit();
                } catch (\Exception $e) {
                    // 回滚事务
                    Db::rollback();
                }
                $content = [];
                foreach ($res as $k1 => $v1) {
                    $content[$v1[$v['sqlval']]] = $v1[$v['sqlname']];
                }
                $content =  json_encode($content, JSON_UNESCAPED_UNICODE);
                Db::name('cms_fields_bind')->where('id', $v['id'])->update(['content' => $content]);
            }

            //树状显示 select
            if (!empty($v['bindtype']) && $v['bindtype'] == 'tree') {
                $tree       = new Tree();
                $tree->icon = ['&nbsp;&nbsp;│', '&nbsp;&nbsp;├─', '&nbsp;&nbsp;└─'];
                $tree->nbsp = '&nbsp;&nbsp;';
                $newCategories = [];
                Db::startTrans();
                try {

                    if ($v['sqltreeid'] >= 0 && isset($v['sqltreeid']) && strlen($v['sqltreeid']) > 0) {
                        $sql = $v['bindtexts'] . 'where parent_id =' . $v['sqltreeid'];
                    } else {
                        $sql = $v['bindtexts'];
                    }
                    $res = Db::query($sql);
                    foreach ($res as $item) {
                        if (!empty($values)) {
                            $item['selected'] = $values[$v['name']] == $item['id'] ? "selected" : "";
                        }
                        array_push($newCategories, $item);
                    }
                    $tree->init($newCategories);
                    $str     = '<option value=\"{$id}\" {$selected}>{$spacer}{$name}</option>';
                    Db::name('cms_fields_bind')->where('id', $v['id'])->update(['content' => $tree->getTree(0, $str)]);

                    // 提交事务
                    Db::commit();
                } catch (\Exception $e) {
                    // $this->error($e->getMessage());
                    // 回滚事务
                    Db::rollback();
                }
            }

            //指定当前栏目
            if (!empty($v['bindtype']) && $v['bindtype'] == 'channel') {
                $v['bindtext'] = $v['channel_id'] ?? 0;
                $channel_data = Db::name($table)->where('delete_time', '=', 0)->field('id,title')->select()->toArray();
                // dump($channel_data);
                $content = [];
                foreach ($channel_data as $k1 => $v1) {
                    $content[$v1['id']] = $v1['title'];
                }
                // dump(json_encode($content, JSON_UNESCAPED_UNICODE));die;
                $res = CmsFieldsBindModel::where('id', $v['id'])->update(['content' => json_encode($content, JSON_UNESCAPED_UNICODE)]);
            }
        }
        $fields = CmsFieldsBindModel::where([
            "status"   => 1, //启用状态
            'model_id' => $channel['model_id'],
            'channel_id' => $channel_id
        ])
            ->order('list_order asc,id desc')
            ->select();
        if (count($fields) > 0) {
            foreach ($fields as $k => $v) {
                //优先取编辑的值,再次取默认值
                $v->value = isset($values[$v['name']]) ? $values[$v['name']] : (is_null($v['defaultvalue']) ? '' : $v['defaultvalue']);
                //值
                if (!empty($values)) {
                    switch ($v->type) {
                        case "checkbox": //复选框
                            if (!empty($values[$v['name']])) {
                                $v->value = ',' . $values[$v['name']] . ',';
                            }
                            break;
                            // case "checkbox": //复选框
                            //     if (!empty($values[$v['name']])) {
                            //         $v->value = ',' . $values[$v['name']] . ',';
                            //     }
                            //     break;
                        case "editor": //编辑器
                            $v->value = cmf_replace_content_file_url(htmlspecialchars_decode($v->value)); //转码
                            break;
                        case "image": //单图片
                            if (empty($v->value)) {
                                $v->value = '/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png';
                            } else {
                                $v->value = image($v->value);
                                // if (!exist_string($v->value, 'http') && !exist_string($v->value, 'themes')) {
                                //     $v->value = '/upload/' . $v->value;
                                // }
                            }
                            break;
                        case "images": //多图片
                            if (!empty($v->value)) {
                                $array_value = json_decode($v->value, true);
                                $html = '';
                                if (is_array($array_value)) {
                                    foreach ($array_value as $k => $vo) {
                                        $img_url = image($vo['url']);
                                        // if (!exist_string($vo['url'], 'http') && !exist_string($vo['url'], 'themes')) {
                                        //     $img_url = '/upload/' . $vo['url'];
                                        // }
                                        $html .= '<li id="saved-image' . $v['name'] . $k . '">';
                                        $html .= '    <input id="photo-' . $v['name'] . $k . '" type="hidden" name="post[' . $v['name'] . '_urls][]" value="' . $img_url . '">';
                                        $html .= '    <input class="form-control" id="photo-' . $v['name'] . $k . '-name" type="text" name="post[' . $v['name'] . '_names][]" value="' . $vo['name'] . '" style="width: 200px;" title="图片名称">';
                                        $html .= '    <img id="photo-' . $v['name'] . $k . '-preview" src="' . $img_url . '" style="height:36px;width: 36px;" onclick="imagePreviewDialog(this.src);">';
                                        $html .= '    <a href="javascript:uploadOneImage(\'图片上传\',\'#photo-' . $v['name'] . $k . '\');">替换</a>';
                                        $html .= '    <a href="javascript:(function(){$(\'#saved-image' . $v['name'] . $k . '\').remove();})();">移除</a>';
                                        $html .= '</li>';
                                    }
                                }
                                $v->value = $html;
                            }
                            break;
                        case "file": //单文件
                            $html = '';
                            $file_url = $v->value;
                            $file_url = image($v->value);
                            // if (!exist_string($v->value, 'http') && !exist_string($v->value, 'themes')) {
                            //     $file_url = '/upload/' . $v->value;
                            // }
                            $html .= '<input id="file-' . $v['name'] . '" class="form-control" type="text" name="post[' . $v['name'] . ']" value="' . $v->value . '" placeholder="请上传文件" style="width: 200px;">';
                            $html .= '<a id="file-' . $v['name'] . '-preview" href="' . $file_url . '"target="_blank">下载</a>';
                            $v->value = $html;
                            break;
                        case "files": //多文件
                            if (!empty($v->value)) {
                                $array_value = json_decode($v->value, true);
                                $html = '';
                                if (is_array($array_value)) {
                                    foreach ($array_value as $k => $vo) {
                                        $file_url = image($vo['url']);
                                        // if (!exist_string($vo['url'], 'http') && !exist_string($vo['url'], 'themes')) {
                                        //     $file_url = '/upload/' . $vo['url'];
                                        // }
                                        $html .= '<li id="saved-file' . $v['name'] . $k . '">';
                                        $html .= '    <input id="file-' . $v['name'] . $k . '" type="hidden" name="post[' . $v['name'] . '_urls][]" value="' . $vo['url'] . '">';
                                        $html .= '    <input class="form-control" id="file-' . $v['name'] . $k . '-name" type="text" name="post[' . $v['name'] . '_names][]" value="' . $vo['name'] . '" style="width: 200px;" title="文件名称">';
                                        $html .= '    <a id="file-' . $v['name'] . $k . '-preview" href="' . $file_url . '" target="_blank">下载</a>';
                                        $html .= '    <a href="javascript:uploadOne(\'文件上传\',\'#file-' . $v['name'] . $k . '\',\'file\');">替换</a>';
                                        $html .= '    <a href="javascript:(function(){$(\'#saved-file' . $v['name'] . $k . '\').remove();})();">移除</a>';
                                        $html .= '</li>';
                                    }
                                }
                                $v->value = $html;
                            }
                            break;
                        default:
                            break;
                    }
                }

                //规则
                if (in_array($v->type, ['checkbox', 'lists', 'images'])) {
                    $checked = '';
                    if ($v['minimum'] && $v['maximum']) {
                        $checked = "{$v['minimum']}~{$v['maximum']}";
                    } elseif ($v['minimum']) {
                        $checked = "{$v['minimum']}~";
                    } elseif ($v['maximum']) {
                        $checked = "~{$v['maximum']}";
                    }
                    if ($checked) {
                        $v->rule .= (';checked(' . $checked . ')');
                    }
                }
                if (in_array($v->type, ['checkbox', 'radio']) && stripos($v->rule, 'required') !== false) {
                    $v->rule = str_replace('required', 'checked', $v->rule);
                }
                if (in_array($v->type, ['selects'])) {
                    $v->extend .= (' ' . 'data-max-options="' . $v['maximum'] . '"');
                }
            }
            echo json_encode($fields);
        } else { //没有字段
            echo "nofields";
        }
    } else {
        echo "nochannel";
    }
}

/**
 * cms管理
 * 添加修改 时 字段处理
 *
 * @param [array] $data
 * @return void
 */
function cms_field($data)
{


    if (!empty($data['thumbnail'])) {
        // $data['thumbnail'] = cmf_asset_relative_url($data['thumbnail']);
        $data['thumbnail']         = $data['thumbnail'];
    }
    //富文本框内容转换
    if (isset($data['content'])) {
        $data['content'] = htmlspecialchars(cmf_replace_content_file_url(htmlspecialchars_decode($data['content']), true)); //转码
    }
    $dynamicFields = Db::name('cms_fields_bind')->where('model_id', $data['model_id'])->where(['channel_id' => $data['channel_id']])->field('name,type,showoption,bindtype')->select()->toArray();
    foreach ($dynamicFields as $k => $v) {
        if ($v['name'] == 'published_time') {
            unset($dynamicFields[$k]);
        }
        $fieldname = $v['name']; //字段名称

        $fieldtype = strtolower($v['type']); //字段类型
        $showoption = strtolower($v['showoption']); //选项的展示方式
        $bindtype = strtolower($v['bindtype']); //绑定的文本类型

        if (isset($data[$fieldname])) { //存在属性
            switch ($fieldtype) {
                case "checkbox": //复选框
                    $data[$fieldname] = implode(",", $data[$fieldname]); //json_encode($data[$fieldname]);
                    break;
                case "text": //文本框
                    $data[$fieldname] = newline2br(nl2br($data[$fieldname])); //回车换成换行
                    break;
                case "option": //复选框
                    switch ($bindtype) {
                        case 'text':
                            switch ($showoption) {
                                case 'checkbox':
                                    $data[$fieldname] = implode(",", $data[$fieldname]);
                                    break;
                            }
                        case 'sql':
                            switch ($showoption) {
                                case 'checkbox':
                                    $data[$fieldname] = implode(",", $data[$fieldname]);
                                    break;
                            }
                            break;
                    }

                    break;
                case "editor": //编辑器
                    $data[$fieldname] = htmlspecialchars(cmf_replace_content_file_url(htmlspecialchars_decode($data[$fieldname]), true)); //转码
                    break;
                case "images": //多图片
                case "files": //多文件
                    $data[$fieldname] = json_encode($data[$fieldname]);
                    break;
                case "image": //单图
                    if (!exist_string($data[$fieldname], 'http') && !exist_string($data[$fieldname], 'themes')) {
                        $data[$fieldname] = str_replace("/upload/", '', $data[$fieldname]);
                    }
                    break;
                default:
                    break;
            }
        }
    }
    return $data;
}

/**
 * cms管理
 *列表类型 字段数据处理
 * @param [array] $data
 * @param [array] $post
 * @return void
 */
function cms_dataPost($data, $post)
{
    /**
     * 单选框
     */
    if (!empty($post['radio_list'])) {
        $list = $post['radio_list'];
        foreach ($list as $v) {
            if (!isset($post[$v])) {
                $data['post'][$v] = []; //顺序不能乱
            }
        }
        unset($data['post']['radio_list']);
    }

    /**
     * 复选框
     */
    if (!empty($post['checkbox_list'])) {
        $list = $post['checkbox_list'];
        foreach ($list as $v) {
            if (!isset($post[$v])) {
                $data['post'][$v] = []; //顺序不能乱
            }
        }
        unset($data['post']['checkbox_list']);
    }

    /**
     * 多图片\多文件处理
     */
    if (!empty($post['images_files'])) {
        $list = $post['images_files'];
        foreach ($list as $v) {
            $data['post'][$v] = []; //顺序不能乱
            if (!empty($post[$v . '_names']) && !empty($post[$v . '_urls'])) {
                foreach ($post[$v . '_urls'] as $key => $url) {
                    $photoUrl = cmf_asset_relative_url($url);
                    array_push($data['post'][$v], ["url" => $photoUrl, "name" => $post[$v . '_names'][$key]]);
                }
                unset($data['post'][$v . '_names']);
                unset($data['post'][$v . '_urls']);
            }
        }
        unset($data['post']['images_files']);
    }
    //验证规则
    $test = test($data['post']);
    // dump($test);die;
    if ($test['code'] == 0) {
        op_log(0, '验证文章字段', '验证');
        return ['code' => 0, 'msg' => $test['msg']];
    }
    return ['data' => $data, 'post' => $post];
}

/**
 * 验证字段
 *
 * @param [array] $data 验证数据
 * @return array data
 */
function test($data)
{
    $model_id = $data['model_id'];
    $channel_id = $data['channel_id'];
    $rules =  Db::name('cms_fields_bind')->where('model_id', $model_id)->where(['channel_id' => $channel_id])->field('title,required,rule,name,type,defaultvalue')->where('status', 1)->select()->toArray();
    $test = Db::name('cms_rule')->select()->toArray();
    try {
        foreach ($rules as $k => $v) {
            $rules[$k]['value'] = isset($data[$v['name']]) ? $data[$v['name']] : (is_null($v['defaultvalue']) ? '' : $v['defaultvalue']);

            //验证是否为空
            if (!empty($v['required'])) {
                if (empty($rules[$k]['value'])) {
                    return ['msg' => $v['name'] . '不能为空', 'code' => 0];
                }
            }
            // dump(1);
            if (!empty($v['rule'])) {
                foreach ($test as $k1 => $v1) {
                    if ($v['rule'] == $v1['name']) {
                        $testFlag = preg_match('/' . $v1['rule'] . '/', $rules[$k]['value']);
                        if ($testFlag) {
                            return ['msg' => '匹配成功', 'code' => 1];
                        } else {
                            return ['msg' => '格式错误，' . $v['title'] . '字段格式为' . $v1['title'], 'code' => 0];
                        }
                    }
                }
            }
            continue;
        }
    } catch (\Exception $e) {
        return ['msg' => $e->getMessage(), 'code' => 0];
    }
    return ['code' => 1, 'msg' => 'ok'];
}


/**
 * 查询字符串是否包含指定字符串
 *
 * @param [string] $string  字符串
 * @param [string] $find    要查找的字符串
 * @return void
 */
function exist_string($str, $findStr)
{
    if (strpos($str, $findStr) !== false) {
        return true;
    } else {
        return false;
    }
}


//$content 文问本内答容
function newline2br($str)
{
    $arr = explode('<br />', $str);
    $ll = '';
    foreach ($arr as $key => $value) {
        if (!empty($value)) {
            $ll .= $value . '<br />';
        }
    }
    return $ll;
}

/**
 * 发送HTTP请求方法
 * @param  string $url    请求URL
 * @param  array  $params 请求参数
 * @param  string $method 请求方法GET/POST
 * @param  bool   $multi  是否传输文件 默认false
 * @return string  $data   响应数据
 */
function safeHttp($url, $params, $method = 'GET', $header = array(), $multi = false)
{
    $opts = array(
        CURLOPT_TIMEOUT => 30,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_HTTPHEADER => $header
    );
    /* 根据请求类型设置特定参数 */
    switch (strtoupper($method)) {
        case 'GET':
            $opts[CURLOPT_URL] = $url . '?' . http_build_query($params);
            break;
        case 'POST':
            //判断是否传输文件
            if ($multi) {
                foreach ($params as $k => $path) {
                    if (is_file($path)) {
                        $post[$k] = new \CURLFile(realpath($path));
                    }
                    break;
                }
            } else {
                $post = http_build_query($params);
            }
            $opts[CURLOPT_URL] = $url;
            $opts[CURLOPT_POST] = 1;
            $opts[CURLOPT_POSTFIELDS] = $post;
            break;
        default:
            throw new \Exception('不支持的请求方式！');
    }

    /* 初始化并执行curl请求 */
    $ch = curl_init();
    curl_setopt_array($ch, $opts);

    // SafeCurl::setHeaders($header);
    // $data = SafeCurl::exec($opts[CURLOPT_URL], $ch);
    $data['curl_url'] = $opts[CURLOPT_URL];
    $data = curl_exec($ch);
    $error = curl_error($ch);
    curl_close($ch);
    if ($error) {
        throw new \Exception('请求发生错误：' . $error);
    }
    return $data;
}


/**
 * @param $token
 * @param $conversionTypes
 * @return bool 发送成功返回true，失败返回false
 */
function sendOcpcData($token, $conversionTypes, $RETRY_TIMES = 3)
{
    $reqData = array('token' => $token, 'conversionTypes' => $conversionTypes);
    $reqData = json_encode($reqData);
    // 发送完整的请求数据
    // do some log
    // print_r('req data: ' . $reqData . "\n<BR>");
    // 向百度发送数据
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_URL, 'https://ocpc.baidu.com/ocpcapi/api/uploadConvertData');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $reqData);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt(
        $ch,
        CURLOPT_HTTPHEADER,
        array(
            'Content-Type: application/json; charset=utf-8',
            'Content-Length: ' . strlen($reqData)
        )
    );
    // 添加重试，重试次数为3
    for ($i = 0; $i < $RETRY_TIMES; $i++) {
        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        // print_r($httpCode. "\n");
        if ($httpCode === 200) {
            // 打印返回结果
            // print_r('retry times: ' . $i . ' res: ' . $response . "\n");
            $res = json_decode($response, true);
            // status为4，代表服务端异常，可添加重试
            $status = $res['header']['status'];
            if ($status !== 4) {
                curl_close($ch);
                return $status === 0;
            }
        }
    }
    curl_close($ch);
    return false;
}

/**
 * 判断是否存在静态
 *
 * @param string $key 静态文件名称，传入，方便出问题时候查看
 * @param string $index 静态文件存放一级文件夹
 */
function beforeBuild($url, $staticHtmlDir, $staticHtmlFile, $key = "", $index = "index")
{
    //生成静态
    $staticHtmlDir = $staticHtmlDir ?? "html/" . $index . '/';
    //静态文件存放地址
    $staticHtmlFile = $staticHtmlFile ?? $staticHtmlDir . $key . '.html';

    //目录不存在，则创建
    if (!file_exists($staticHtmlDir)) {
        mkdir($staticHtmlDir, 0755, true);
        chmod($staticHtmlDir, 0755);
    }

    //静态文件存在,并且没有过期
    // if (file_exists($staticHtmlFile) && filectime($staticHtmlFile) >= time() - 60 ) {
    //     $url = str_replace('/public', '', $url);
    //     return redirect($url);
    //     exit();
    // }
}

/**
 * 开始生成静态文件
 * 
 * @param $html
 */
function afterBuild($url, $staticHtmlFile, $html)
{
    if (!empty($staticHtmlFile) && !empty($html)) {
        if (file_exists($staticHtmlFile)) {
            @unlink($staticHtmlFile);
        }
        file_put_contents($staticHtmlFile, $html);
    }
}


/**
 * 上传到bos
 */
function uploadBos($file_path)
{
    $BOS_CONFIG =
        [
            'credentials' => [
                'accessKeyId' => 'vmNHAc8GBGZOOgcDMo1NXp62',
                'secretAccessKey' => 'eMypBQEpOTqT8GiyZhV59N6BM3TnZhnB',
            ],
            'endpoint' => 'http://bd.bcebos.com',
        ];
    //             $BOS_CONFIG =
    //   [
    //     'credentials' => [
    //       'accessKeyId' => '40a66b3cb9b24707bd3403a524ab35cb',
    //       'secretAccessKey' => 'd4af989452d14576b305662b377956fc',
    //     ],
    //     'endpoint' => 'http://gz.bcebos.com',
    //   ];


    $client = new BosClient($BOS_CONFIG);
    // $response = $client->listBuckets();
    try {

        $bucketName = 'ebd-site';
        // $bucketName = 'gz-shengtai';

        // $key = "newbaidu/upload/cms/1.mp4";
        $key = 'newbaidu/upload/' . $file_path;

        $filename = WEB_ROOT . 'upload/' . $file_path;
        // $filename = "D:/phpstudy_pro/WWW/ebaidu.cn/public/upload/cms/20210923/79e778d73861f8c2e1f1978eb788917e.mp4";
        // $filename = "D:/phpstudy_pro/WWW/ebaidu.cn/data/runtime/upload/4/1.mp4";
        $userMeta = array("private" => "private data");
        $options = array(
            BosOptions::PART_SIZE => 5 * 1024 * 1024,
            BosOptions::USER_METADATA => $userMeta
        );
        // $client->putSuperObjectFromFile($bucketName, $key, $filename);
        $client->putObjectFromFile($bucketName, $key, $filename);
    } catch (BceBaseException $e) {
        var_dump($e->getMessage());
        die;
        // throw new \Exception($e->getMessage());
    }
}


/**
 * 生成缩略图  数据处理
 *
 * @param [int] $channel_id  栏目id
 * @param [array] $data        数据
 * @return void
 */
function thumb($channel_id, $data)
{
    // dump($data);
    $channelFields = Db::name('cms_fields_bind')
        ->where(['channel_id' => $channel_id])
        ->where('type', 'image')
        ->whereOr('type', 'images')
        ->field('id,img_height,img_width,is_thumb,name,is_save')
        ->where('is_thumb', 1)
        ->where('img_height', '>', 0)
        ->where('img_width', '>', 0)
        // ->fetchSql(true)
        ->select()
        ->toArray();
    // dump($channelFields);die;
    if (empty($channelFields)) {
        return ['code' => 1, 'msg' => 'ok', 'data' => $data];
    }
    foreach ($channelFields as $k => $v) {
        foreach ($data as $key => $value) {
            if ($v['name'] == $key) {
                if ($v['is_thumb'] == 1 && $v['img_width'] > 0 && $v['img_height'] > 0 && !empty($value)) {
                    if (is_array(json_decode($value, true))) { //多张缩略图
                        $arrayImg = json_decode($value, true);
                        foreach ($arrayImg as $k1 => $v1) {
                            $arrayImg[$k1]['url'] = create_thumb($v1['url'], $v['img_width'], $v['img_height'], $v['is_save']);
                        }
                        $newImg = json_encode($arrayImg);
                        $data[$key] =  $newImg;
                    } else { //单张缩略图
                        $newImg = create_thumb($value, $v['img_width'], $v['img_height'], $v['is_save']);
                        // dump($newImg);die;
                        $data[$key] =  $newImg;
                    }
                }
            }
        }
    }
    // dump($data);die;
    return ['code' => 1, 'msg' => 'ok', 'data' => $data];
}

/**
 * 生成缩略图
 *
 * @param string $img_url  原图路径
 * @param int $width    缩略图宽度
 * @param int $height   缩略图高度
 * @param int $save      是否保存原图 1是
 * @return void
 */
function create_thumb($img_url, $width, $height, $save = 1)
{
    $absolute_url = $_SERVER['DOCUMENT_ROOT'] . '/upload/' . $img_url;
    if (!file_exists($absolute_url)) {
        return false;
    }
    // dump($absolute_url);die;
    $image = \think\Image::open($absolute_url);
    //路径   "./upload/".pathinfo($value)['dirname'].
    $thumb_name = "./upload/" . pathinfo($img_url)['dirname'] . '/' . pathinfo($img_url)['filename'] . $width . $height . '.' . pathinfo($img_url)['extension']; //.'.'.pathinfo($img_url)['extension']
    //绝对路径 判断文件是否存在
    $ab_thName = $_SERVER['DOCUMENT_ROOT'] . '/upload/' .  pathinfo($img_url)['dirname'] . '/' . pathinfo($img_url)['filename'] . $width . $height . '.' . pathinfo($img_url)['extension'];
    $newImg = str_replace('./upload/', '', $thumb_name);
    //存在就不在执行缩略图函数
    if (file_exists($ab_thName)) {
        return $newImg;
    }
    // dump($thumb_name);die;
    // 按照原图的比例生成一个最大为150*150的缩略图并保存为thumb.png
    $image->thumb($width, $height)->save($thumb_name);
    // dump($save);die;
    if ($save == 0) {
        @unlink($absolute_url);
    }
    /***/
    //  压缩图片 */ todo
    // $source = '1.png';
    // $dst_img = '../copy1.png'; //可加存放路径
    // $percent = 1;  #原图压缩，不缩放
    // $image = (new imgcompress($thumb_name,$percent))->compressImg($thumb_name);
    // dump($newImg);die;
    // return ['url'=>$newImg];
    return $newImg;
}
/**
 * 判断变量是时间戳还是时间
 */
function is_timestamp($time, $format = 'Y-m-d H:i')
{
    if (empty($time)) {
        return date($format, time());
    }
    if (is_numeric($time)) {
        return date($format, (int)$time);
    } else {
        return  date($format, strtotime($time));
    }
}

/**
 * 生成前端表单html
 * @param [int] $channel_id  栏目id
 * @return string
 */
function createFormHtml($channel_id)
{
    //查询字段
    $fields = CmsFieldsBindModel::field('type,name,title,required,rule,msg')->where('channel_id', $channel_id)->where('status', 1)->order('list_order ASC')->select()->toArray();
    //验证规则
    $rules = CmsRuleModel::select()->toArray();
    $form =  '<form method="POST" id="form1" onsubmit="return false">' . PHP_EOL;
    foreach ($fields as $k => $v) {
        if (!empty($v['rule'])) {
            foreach ($rules as $k1 => $v1) {
                if ($v1['name'] == $v['rule']) {
                    if ($v['type'] == 'text') {
                        $form .= '<textarea name="' . $v['name'] . '"  placeholder="' . $v['title'] . '" ' . $v['required'] . ' must="' . $v['required'] . '" rule="' . $v1['home_rule'] . '" msg="' . $v['msg'] . '" rule_name="' . $v1['title'] . '" ></textarea>' . PHP_EOL;
                    } else {
                        $form .= '<input name="' . $v['name'] . '"  placeholder="' . $v['title'] . '"  ' . $v['required'] . ' must="' . $v['required'] . '"  rule="' . $v1['home_rule'] . '" msg="' . $v['msg'] . '" rule_name="' . $v1['title'] . '">' . PHP_EOL;
                    }
                }
            }
        } else {
            if ($v['type'] == 'text') {
                $form .= '<textarea  name="' . $v['name'] . '"  placeholder="' . $v['title'] . '" must="' . $v['required'] . '"  rule="' . $v['rule'] . '" msg="' . $v['msg'] . '"  ' . $v['required'] . ' ></textarea>' . PHP_EOL;
            } else {
                $form .= '<input type="text" name="' . $v['name'] . '"  placeholder="' . $v['title'] . '" must="' . $v['required'] . '"  rule="' . $v['rule'] . '" msg="' . $v['msg'] . '"  ' . $v['required'] . ' >' . PHP_EOL;
            }
        }
    }
    $form .= ' <input type="hidden" value="' . $channel_id . '" name="channel_id">' . PHP_EOL;
    // $form .= ' <input type="hidden" value="{:create_form_token()}" name="token">'.PHP_EOL;
    // $form .= ' <input type="hidden" value="'.$category['lang'].'" name="lang">'.PHP_EOL;
    $form .= ' <input type="button" value="提交" class="form-submit">' . PHP_EOL;
    $form .= ' </form>' . PHP_EOL;
    $form  .= '<script type="text/javascript" src="/static/js/check.js"></script>' . PHP_EOL;
    $form = htmlentities($form);
    return $form;
}


/**
 * 获取文件名、后缀
 * @param string $str  字符串
 * @return string
 */
function getFileName($str)
{
    if(strrchr($str, "/"))
    {
        return substr(strrchr($str, "/"), 1);
    }
}


/**
 *  中文截取2，单字节截取模式
 *
 * @access    public
 * @param     string  $str  需要截取的字符串
 * @param     int  $slen  截取的长度
 * @param     int  $startdd  开始标记处
 * @return    string
 */
if (!function_exists('cn_substr')) {
    function cn_substr($str, $slen, $startdd = 0)
    {
        $slen = intval($slen / 3);
        return mb_substr($str, $startdd, $slen);
    }
}


/**
 *  HTML转换为文本
 *
 * @param    string  $str 需要转换的字符串
 * @param    string  $r   如果$r=0直接返回内容,否则需要使用反斜线引用字符串
 * @return   string
 */
if (!function_exists('Html2Text')) {
    function Html2Text($str, $r = 0)
    {
        if ($r == 0) {
            return SpHtml2Text($str);
        } else {
            $str = SpHtml2Text(stripslashes($str));
            return addslashes($str);
        }
    }
}


function SpHtml2Text($str)
{
    $str = preg_replace("/<sty(.*)\\/style>|<scr(.*)\\/script>|<!--(.*)-->/isU", "", $str);
    $alltext = "";
    $start = 1;
    for ($i = 0; $i < strlen($str); $i++) {
        if ($start == 0 && $str[$i] == ">") {
            $start = 1;
        } else if ($start == 1) {
            if ($str[$i] == "<") {
                $start = 0;
                $alltext .= " ";
            } else if (ord($str[$i]) > 31) {
                $alltext .= $str[$i];
            }
        }
    }
    $alltext = str_replace("　", " ", $alltext);
    $alltext = preg_replace("/&([^;&]*)(;|&)/", "", $alltext);
    $alltext = preg_replace("/[ ]+/s", " ", $alltext);
    return $alltext;
}

/**
 * 获取表名
 * @param int $channel_id 栏目id
 */
function cmsModel($channel_id)
{
    $model_id = Db::name('cms_channel')->where('id', $channel_id)->value('model_id');
    $table = Db::name('cms_model')->where('id', $model_id)->value('table');

    return $table;
}

/**
 * 获取所有的子栏目
 *
 * @param [int] $id 查询id
 * @return string
 */
function getChildrenIds($id)
{
    $ids = '';
    $data = Db::name('cms_channel')->where('delete_time', 0)->where('parent_id', $id)->select()->toArray();
    if(!empty($id))
    {
        if ($data) {
            foreach ($data as $key => $val) {
                $ids .= ',' . $val['id'];
                $ids .= getChildrenIds($val['id']);
            }
        }
    }

    return $ids;
}

/**
 * 导航栏的增加选中样式
 *
 * @param int $channel_id
 * @param int $current_id
 * @param string $class
 * @return string
 */
function addClass($channel_id, $current_id, $class)
{
    $controller = request()->controller();
    $action = request()->action();
    if ($controller  != 'Index' && $controller != 'Search' && $controller !='HomeCreate' && $action !='create' && request()->pathinfo()!='home') {
        if ($channel_id == $current_id) {
            return $class;
        }
        //获取所有的子栏目
        $GetTopTypeId = GetTopTypeId($channel_id);
        if ($GetTopTypeId == $current_id) {
            return $class;
        }
    }
    if ($controller == 'Article') {
        //获取所有的子栏目
        $GetTopTypeId = GetTopTypeId($channel_id);
        if ($GetTopTypeId == $current_id) {
            return $class;
        }
    }
}


/**
 * 获取文件名后缀
 *
 * @param string $file
 * @return string
 */
function getExtension($file)
{
    return pathinfo($file)['extension'] ?? '';
}

/**
 * 判断是否为首页
 *
 * @param string $class
 * @return void
 */
function indexClass($class)
{
    $controller = request()->controller();
    $action = request()->action();
    if ($controller == 'Index' || $controller == 'Search' || $action == 'create' || request()->pathinfo()=='home' ) {
        return $class;
    }
}


/**
 * 获取所有的父级id
 *
 * @param int $id
 * @return string
 */
function getParentIds($id)
{
    $ids = '';
    $parent_id = Db::name('cms_channel')->where('status', 1)->where('delete_time', 0)->where('id', $id)->value('parent_id');
    if ($parent_id != 0) {
        $ids .= ',' . $parent_id;
        $ids .= getParentIds($parent_id);
    }
    return $ids;
}


/**
 * 获取面包屑数据
 * @param int     $id  当前文章所在分类,或者当前分类的id
 * @param boolean $withCurrent 是否获取当前分类
 * @return array 面包屑数据
 */
function breadcrumb($id, $withCurrent = true)
{

        $cmsChannelModel = new CmsChannelModel();
        $str = '<a href="/">主页</a> ';
        //获取所有父级
        $parent_ids = ltrim(getParentIds($id), ',');
        if (!empty($parent_ids)) {
            $breadData = $cmsChannelModel->where('status', 1)->where('delete_time', 0)->where('id', 'in', $parent_ids)->order('list_order asc')->select()->toArray();
            if (!empty($breadData)) {
                foreach ($breadData as $k => $v) {
                    $str .= '&gt; <a href="' . $v['route'] . '">' . $v['name'] . '</a>';
                }
                $channel = $cmsChannelModel->where('id', $id)->field('id,route,name')->find()->toArray();
                $str .= '&gt; <a href="' . $channel['route'] . '">' . $channel['name'] . '</a>';
            }
        } else {
            $data = $cmsChannelModel->where('status', 1)->where('delete_time', 0)->where('id', $id)->field('id,name,route')->find();
            $str .= '&gt; <a href="' . $data['route'] . '">' . $data['name'] . '</a>';
        }
        cache($id.'breadcrumb',$str);
    
    
    return $str;
}

/**
 * 获取最顶级栏目名称
 *
 * @param int $id
 * @return string
 */
function GetTopTypename($id)
{
    $parent_id = Db::name('cms_channel')->field('name,parent_id')->where('status', 1)->where('delete_time', 0)->where('id', $id)->find();
    if(!empty($id))
    {
        if ($parent_id['parent_id'] != '0') {
            return GetTopTypename($parent_id['parent_id']);
        }
    }
    return $parent_id['name'];

}

/**
 * 获取最顶级栏目id
 *
 * @param int $id
 * @return string
 */
function GetTopTypeId($id)
{
    $parent_id = Db::name('cms_channel')->field('name,parent_id,id')->where('delete_time', 0)->where('id', $id)->find();
    if(!empty($id))
    {
        if ($parent_id['parent_id'] != '0') {
            return GetTopTypeId($parent_id['parent_id']);
        }
    }
    

    return $parent_id['id'];
}

/**
 * 获取最顶级栏目别名
 *
 * @param int $id
 * @return string
 */
function GetTopTypenameen($id)
{
    $parent_id = Db::name('cms_channel')->field('alias,parent_id')->where('status', 1)->where('delete_time', 0)->where('id', $id)->find();
    if(!empty($id))
    {
        if ($parent_id['parent_id'] != '0') {
        return GetTopTypenameen($parent_id['parent_id']);
    }
    }
    
    return $parent_id['alias'];
}

/**
 * 时间转换
 *
 * @param string $format 时间格式 Y-m-d H:i:s
 * @param string $time
 * @return void
 */
function MyDate($format, $time)
{
    return is_timestamp($time, $format);
}

/**
 * 去除xss
 *
 * @param string $val
 * @return string
 */
function RemoveXSS($val)
{
    $val = preg_replace('/([\x00-\x08,\x0b-\x0c,\x0e-\x19])/', '', $val);
    $search = 'abcdefghijklmnopqrstuvwxyz';
    $search .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $search .= '1234567890!@#$%^&*()';
    $search .= '~`";:?+/={}[]-_|\'\\';
    for ($i = 0; $i < strlen($search); $i++) {
        $val = preg_replace('/(&#[xX]0{0,8}' . dechex(ord($search[$i])) . ';?)/i', $search[$i], $val); // with a ;
        $val = preg_replace('/(&#0{0,8}' . ord($search[$i]) . ';?)/', $search[$i], $val); // with a ;
    }

    $ra1 = array('javascript', 'vbscript', 'expression', 'applet', 'meta', 'xml', 'blink', 'link', 'style', 'script', 'embed', 'object', 'iframe', 'frame', 'frameset', 'ilayer', 'layer', 'bgsound', 'title', 'base');
    $ra2 = array('onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy', 'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavailable', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterchange', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowenter', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload');
    $ra = array_merge($ra1, $ra2);

    $found = true;
    while ($found == true) {
        $val_before = $val;
        for ($i = 0; $i < sizeof($ra); $i++) {
            $pattern = '/';
            for ($j = 0; $j < strlen($ra[$i]); $j++) {
                if ($j > 0) {
                    $pattern .= '(';
                    $pattern .= '(&#[xX]0{0,8}([9ab]);)';
                    $pattern .= '|';
                    $pattern .= '|(&#0{0,8}([9|10|13]);)';
                    $pattern .= ')*';
                }
                $pattern .= $ra[$i][$j];
            }
            $pattern .= '/i';
            $replacement = substr($ra[$i], 0, 2) . '<x>' . substr($ra[$i], 2);
            $val = preg_replace($pattern, $replacement, $val);
            if ($val_before == $val) {
                $found = false;
            }
        }
    }
    return $val;
}


/**
 * 获取栏目的表单id
 *
 * @return int id
 */
function getFormId()
{
    $id = CmsChannelModel::where('model_id', 3)->order('id', 'asc')->value('id') ?? 1;
    return $id;
}

/**
 * 获取栏目缩略图
 *
 * @param int $id
 * @return string
 */
function GetTopTypeimg($id)
{
    $img = '';
    if (!empty($id)) {
        $img = Db::name('cms_channel')->where('id', GetTopTypeId($id) ?? $id)->value('thumbnail');
        if (!empty($img) && strpos($img, '/skin') !== false) {
            return '/themes/xst/public' . $img;
        }
    }
    return image($img);
}

/**
 * 返回网站类型
 *
 * @return boolean
 */
function web_type()
{
    $web_type = Db::name('option')->where('option_name', 'web_type')->value('option_value') ?? 1;
    if ($web_type == 2) {
        return true;
    }
    return false;
}

/**
 * 获取最顶级栏目的描述
 *
 * @param int $id
 * @return void
 */
function GetTopDescription($id)
{
    if(!empty($id))
    {
        $topid = GetTopTypeId($id);
        return Db::name('cms_channel')->where('id',$topid)->value('description')??''; 
    }
}

/**
 * 公共数据
 * 
 */
function commonData()
{
    //基本参数
    $setting = Db::name('setting')->select()->toArray();
    foreach ($setting as $k => $v) {
        switch ($v['type']) {
            case '3':
                $setting[$k]['value'] = image($v['value']);
                break;
        }
        $setting[$v['read_name']] = $setting[$k];
        unset($setting[$k]);
    }
    
    //侧边栏参数
    $side = Db::name('setting_side')->select()->toArray();
    foreach ($side as $k => $v) {
        $side[$v['read_name']] = $side[$k];
        unset($side[$k]);
    }
        
    return ['setting'=>$setting,'side'=>$side];
}


