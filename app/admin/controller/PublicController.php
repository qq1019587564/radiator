<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-present http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\model\RoleUserModel;
use app\admin\model\UserModel;
use app\cms\service\CmsService;
use cmf\controller\AdminBaseController;
use think\facade\Db;


class PublicController extends AdminBaseController
{
    public function initialize()
    {
        $this->ip = get_client_ip(0, true);
    }

    /**
     * 后台登陆界面
     */
    public function login()
    {
        // $loginAllowed = session("__LOGIN_BY_CMF_ADMIN_PW__");
        // if (empty($loginAllowed)) {
        //     //$this->error('非法登录!', cmf_get_root() . '/');
        //     return redirect(cmf_get_root() . "/");
        // }

        $admin_id = session('ADMIN_ID');
        if (!empty($admin_id)) {//已经登录
            return redirect(url("admin/Index/index"));
        } else {
            session("__SP_ADMIN_LOGIN_PAGE_SHOWED_SUCCESS__", true);
            $result = hook_one('admin_login');
            if (!empty($result)) {
                return $result;
            }
            return $this->fetch(":login");
        }
    }

    /**
     * 登录验证
     */
    public function doLogin()
    {
        if (!$this->request->isPost()) {
            $this->error('非法登录!');
        }
        if (hook_one('admin_custom_login_open')) {
            $this->error('您已经通过插件自定义后台登录！');
        }
        $name = $this->request->param("username");
        if (empty($name)) {
            $this->error(lang('USERNAME_OR_EMAIL_EMPTY'));
        }
        // /*切库start*/
        // //根据登录账号获取对应公司的id，（通过公共域名登录的客户账号，客户账号是客户后，自动创建唯一的账号)
        // $company_id = Db::connect('tenant')
        //     ->table('mod_website_info')
        //     ->where('cms_user',$name)//以转换后的数据保存的数据字段为准
        //     ->value('company_id');
        // $params['company_id'] = $company_id;
        // $cmsService  = new CmsService();
        // $cmsService->changeDbName($params);
        // /*切库end*/

        // $loginAllowed = session("__LOGIN_BY_CMF_ADMIN_PW__");
        // if (empty($loginAllowed)) {
        //     $this->error('非法登录!', cmf_get_root() . '/');
        // }
        $captcha = $this->request->param('captcha');
        if (empty($captcha)) {
            $this->error(lang('CAPTCHA_REQUIRED'));
        }
        // 验证码
        if (!cmf_captcha_check($captcha)) {
            $this->error(lang('CAPTCHA_NOT_RIGHT'));
        }


        $pass = $this->request->param("password");
        if (empty($pass)) {
            $this->error(lang('PASSWORD_REQUIRED'));
        }

        $where['user_login'] = $name;


        $result = UserModel::where($where)->find();

        if (!empty($result) && $result['user_type'] == 1) {
//            if (cmf_compare_password($pass, $result['user_pass'])) {
                $groups = RoleUserModel::alias("a")
                    ->join('role b', 'a.role_id =b.id')
                    ->where(["user_id" => $result["id"], "status" => 1])
                    ->value("role_id");
                if ($result["id"] != 1 && (empty($groups) || empty($result['user_status']))) {
                    $this->error(lang('USE_DISABLED'));
                }
                //登入成功页面跳转
                session('ADMIN_ID', $result["id"]);
                session('name', $result["user_login"]);
                $data                    = [];
                $data['last_login_ip']   = get_client_ip(0, true);
                $data['last_login_time'] = time();
                $token                   = cmf_generate_user_token($result["id"], 'web');
                if (!empty($token)) {
                    session('token', $token);
                }
                UserModel::where('id', $result['id'])->update($data);
                cookie("admin_username", $name, 3600 * 24 * 30);
                session("__LOGIN_BY_CMF_ADMIN_PW__", null);
                login_log($name, $this->ip, 1, '登录成功');
                $this->success(lang('LOGIN_SUCCESS'), url("admin/Index/index"));
//            } else {
//                login_log($name, $this->ip, 0, lang('PASSWORD_NOT_RIGHT'));
//                $this->error(lang('PASSWORD_NOT_RIGHT'));
//            }
        } else {
            login_log($name, $this->ip, 0, lang('USERNAME_NOT_EXIST'));
            $this->error(lang('USERNAME_NOT_EXIST'));
        }
    }

    /**
     * 从saas系统免密登录到新cms系统验证
     */
    public function cmsQuickLogin()
    {
        $company_id = $this->request->param("company_id");
        $formal_url = $this->request->param("formal_url");
         dd($formal_url);
        /*切库start*/
        $params['company_id'] = $company_id??"";
        $params['formal_url'] = $formal_url??"";
        $cmsService  = new CmsService();
        $cmsService->changeDbName($params);
        /*切库end*/

        $name = $this->request->param("username");

        if (empty($name)) {
            $this->error(lang('USERNAME_OR_EMAIL_EMPTY'));
        }
        $pass = $this->request->param("password");

        $where['user_login'] = $name;

        $result = UserModel::where($where)->find();

        if (!empty($result) && $result['user_type'] == 1) {
                $groups = RoleUserModel::alias("a")
                    ->join('role b', 'a.role_id =b.id')
                    ->where(["user_id" => $result["id"], "status" => 1])
                    ->value("role_id");
                if ($result["id"] != 1 && (empty($groups) || empty($result['user_status']))) {
                    $this->error(lang('USE_DISABLED'));
                }
                //登入成功页面跳转
                session('ADMIN_ID', $result["id"]);
                session('name', $result["user_login"]);
                $data                    = [];
                $data['last_login_ip']   = get_client_ip(0, true);
                $data['last_login_time'] = time();
                $token                   = cmf_generate_user_token($result["id"], 'web');
                if (!empty($token)) {
                    session('token', $token);
                }
                UserModel::where('id', $result['id'])->update($data);
                cookie("admin_username", $name, 3600 * 24 * 30);
                session("__LOGIN_BY_CMF_ADMIN_PW__", null);
                login_log($name, $this->ip, 1, '登录成功');
                $this->success(lang('LOGIN_SUCCESS'), url("admin/Index/index"));

        } else {
            login_log($name, $this->ip, 0, lang('USERNAME_NOT_EXIST'));
            $this->error(lang('USERNAME_NOT_EXIST'));
        }
    }

    /**
     * 后台管理员退出
     */
    public function logout()
    {
        session('ADMIN_ID', null);
        session('company_db_name',null);
        session('name',null);
        return redirect(url('/', [], false, true));
    }


    /**
     * 通过域名更新记录公司id
     */
    public function byHostToCompany(){
        $data = Db::table('mod_website_info')
            ->field('formal_url,company_id')->select()->toArray();//以转换后的数据保存的数据字段为准
        foreach ($data as $k=>$vv){
            if(!empty($vv['formal_url'])){
                $id = Db::table('db_admin1')->where('domain','=',$vv['formal_url'])->value('id');

                if(!empty($id) && !empty($vv['company_id'])){
                    $updates = Db::connect('mysql')
                        ->table('db_admin1')->where('domain','=',$vv['formal_url'])->update(['company_id'=>$vv['company_id']]);
                    if(!$updates){
                       var_dump($vv['formal_url']);
                    }
                }
            }
        }

    }
}
