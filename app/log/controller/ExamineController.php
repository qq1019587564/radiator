<?php

declare(strict_types=1);

namespace app\log\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;
use think\Validate;

class ExamineController extends AdminBaseController
{


    public function index()
    {

        $param = $this->request->param();

        //搜索
        $keyword = $param['keyword'] ?? '';
        $start_time = !empty($param['start_time']) ? $param['start_time'] : 0;
        $end_time = !empty($param['end_time']) ? $param['end_time'] : '2040-12-12 10:04:56';

        $data = Db::name('operation_log')
            ->where("user_name|ip|action", 'like', "%{$keyword}%")
            ->where('new', 1)
            ->where('create_time', '>=', $start_time)
            ->where('create_time', '<=', $end_time)
            ->order('create_time desc')
            ->paginate(20);
        $data->appends(['keyword' => $keyword, 'start_time' => $start_time, 'end_time' => $end_time,]);
        $page = $data->render();
        $data = $data->toArray()['data'];

        $pages = input('page') ?? 1;

        foreach ($data as $k => $v) {
            $data[$k]['sort'] = ($pages - 1) * 10 + $k + 1;
            switch ($v['status']) {
                case '1':
                    $data[$k]['status'] = '<b style="color:green">操作成功</b>';
                    $data[$k]['msg'] = '<b style="color:green">' . $v['msg'] . '</b>';
                    break;
                default:
                    $data[$k]['status'] = '<b style="color:red">操作失败</b>';
                    $data[$k]['msg'] = '<b style="color:red">' . $v['msg'] . '</b>';
                    break;
            }
        }

        $this->assign('keyword', isset($param['keyword']) ? $param['keyword'] : '');
        $this->assign('start_time', isset($param['start_time']) ? $param['start_time'] : '');
        $this->assign('end_time', isset($param['end_time']) ? $param['end_time'] : '');
        $this->assign('data', $data);
        $this->assign('page', $page);
        return $this->fetch();
    }
}
