<?php

declare(strict_types=1);

namespace app\log\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;
use think\Validate;

class OperationController extends AdminBaseController
{


    public function index()
    {

        $param = $this->request->param();

        //搜索
        $keyword = $param['keyword'] ?? '';


        $data = Db::name('operation_log')
            ->where("user_name|ip|action", 'like', "%{$keyword}%")
            ->order('create_time desc')
            ->paginate(20);
        $data->appends(['keyword' => $keyword]);

        $page = $data->render();
        $data = $data->toArray()['data'];
        $pages = input('page') ?? 1;

        foreach ($data as $k => $v) {
            $data[$k]['sort'] = ($pages - 1) * 10 + $k + 1;
            switch ($v['status']) {
                case '1':
                    $data[$k]['status'] = '<b style="color:green">操作成功</b>';
                    break;
                default:
                    $data[$k]['status'] = '<b style="color:red">操作失败</b>';
                    break;
            }
        }

        $this->assign('keyword', isset($param['keyword']) ? $param['keyword'] : '');
        $this->assign('data', $data);
        $this->assign('page', $page);
        return $this->fetch();
    }

    /**
     * 日志详情
     */
    public function detail()
    {
        $data = input();
        //判断域名是否存在
        $log = $beofre = Db::name('operation_log')->where('id', $data['id'])->find();
        if (!$log) {
            $this->error('数据不存在');
        }
        $this->assign('data', $log);
        return $this->fetch();
    }
}
