<?php

declare(strict_types=1);

namespace app\log\controller;

use cmf\controller\AdminBaseController;
use think\facade\Db;
use think\Validate;

class LoginController extends AdminBaseController
{


    public function index()
    {

        $param = $this->request->param();

        //搜索
        $keyword = $param['keyword'] ?? '';


        $data = Db::name('login_log')
            ->where("user_name|ip", 'like', "%{$keyword}%")
            ->order('create_time desc')
            ->paginate(20);
        $data->appends(['keyword' => $keyword]);
        // 获取分页显示
        $page = $data->render();
        $data = $data->toArray()['data'];
        $pages = input('page') ?? 1;

        foreach ($data as $k => $v) {
            $data[$k]['sort'] = ($pages - 1) * 10 + $k + 1;
            switch ($v['login_status']) {
                case '1':
                    $data[$k]['login_status'] = '<b style="color:green">登录成功</b>';
                    $data[$k]['msg'] = '<b style="color:green">' . $v['msg'] . '</b>';
                    break;
                default:
                    $data[$k]['login_status'] = '<b style="color:red">登录失败</b>';
                    $data[$k]['msg'] = '<b style="color:red">' . $v['msg'] . '</b>';
                    break;
            }
        }

        $this->assign('keyword', isset($param['keyword']) ? $param['keyword'] : '');

        $this->assign('data', $data);
        $this->assign('page', $page);
        return $this->fetch();
    }
}
