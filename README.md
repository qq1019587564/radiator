# 电脑配件网站模板
![教育网站前台首页](public/upload/readme/20220513103319.jpg)

![教育网站手机首页](public/upload/readme/166282680.jpg)

一个设计精美的电脑配件业务的TP6模板，应用范围很广泛，比如电脑机械、电脑维修店,、电脑等相关服务。模板使用bootstrap框架编码很容易自定义，而且自适用手机上的浏览器。

# 主要特色

- 100%液态响应式
- 干净的代码
- 灵活的布局
- 无限的侧边栏
- 清洁&注释代码
- 先进的排版
- 建立在bootstrap4
- 有效的HTML5和CSS3文件
- 无限的博客页面
- 跨浏览器兼容性
- 视差效果
- 很棒的独特的外观
- 独特的作用和功能

# 后台功能

- 优化SEO

- 生成整站html功能

- 幻灯片管理

- 内容管理

- 栏目管理

- 友情链接管理

- 参数配置

  后台演示：https://demo5.thinkcmf.com/admin/public/login.html

  后台开发手册：https://www.thinkcmf.com/docs/cmf6/

# 项目伪静态设置
```
location / {
	if (!-e $request_filename){
		rewrite  ^(.*)$  /index.php?s=$1  last;   break;
	}
}
location ~* ^\/upload\/.+\.(html|php)$ {
    return 404;
}

location ~* ^\/plugins\/.+\.(html|php)$ {
        return 404;
}

location ~* ^\/themes\/.+\.(html|php)$ {
        return 404;
 }
```
# 相关项目模板
精美响应式电脑配件工业机械设备销售散热器网站模板
 https://gitee.com/qq1019587564/radiator  
简单的在线教育培训教育课程类网站模板
 https://gitee.com/qq1019587564/education
