<?php
// +----------------------------------------------------------------------
// | SwitchTheme [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Author: zsh
// +----------------------------------------------------------------------
namespace plugins\switch_theme;

use cmf\lib\Plugin;

class SwitchThemePlugin extends Plugin
{
    public $info = [
        'name'        => 'SwitchTheme',
        'title'       => '手机端模板控制',
        'description' => '手机端模板控制',
        'status'      => 1,
        'author'      => 'zsh',
        'version'     => '1.0',
        'demo_url'    => '',
        'author_url'  => ''
    ];

    public $hasAdmin = 0;

    public function install()
    {
        return true;
    }

    public function uninstall()
    {
        return true;
    }

    public function switchTheme($param)
    {

        // $config = $this->getConfig();
        // $regex = '/android|adr|iphone|ipad|windows\sphone|kindle|gt\-p|gt\-n|rim\stablet|opera|meego/i';
        // $mobile = false;
        // if (GetVars('alwaystheme', 'COOKIE') == 'mobile') {
        //     $mobile = true;
        // }
        // if (preg_match($regex, GetVars('HTTP_USER_AGENT', 'SERVER'))) {
        //     $mobile = true;
        // }
        // if (GetVars('alwaystheme', 'COOKIE') == 'pc') {
        //     $mobile = false;
        // }
        // $url = request()->host();

        // if ($url == config('config.m_url')) {
        //     $mobile = true;
        // }

        // if ($mobile) {
        //     $cmfDefaultTheme = $config['mobile_theme'];
        // } else {
        //     $cmfDefaultTheme = config('cmf_default_theme');
        // }
        $cmfDefaultTheme = config('cmf_default_theme');
        return $cmfDefaultTheme;
    }
}

function GetVars($name, $type = 'REQUEST')
{
    $array = &$GLOBALS[strtoupper("_$type")];
    if (isset($array[$name])) {
        return $array[$name];
    } else {
        return null;
    }
}
