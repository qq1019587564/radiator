


//bca-disable
//显示
function loading_show() {
  // $("#extend>table").html("<tr><td colspan='2' style='background:#2C3E50; color:#f00; text-align:center; padding:3px 0px;'>加载中...</td></tr>");
}

//隐藏
function loading_hide() {
  $("#extend>table").html("");
}

//动态字段加载
function init(channel_id, postId, table) {
  // console.log(channel_id)
  loading_show();
  $.ajax({
    type: 'post',
    url: '/' + GV.APP +'/'+ table+'/get_channel_fields',
    data: { "channel_id": channel_id, "aid": postId },
    success: function (data) {
      // console.log(data);
      if (data == 'nofields') {//没字段
        loading_hide();
      } else if (data == 'nochannel') {
        alert('请选择下级栏目，并保证该栏目关联了模型！');
        loading_hide();
      } else if (data) {//有字段
        var array = JSON.parse(data);
        if (array.length > 0) {
          //扩展说明
          // $("#extend>table").html("<tr><td colspan='2' style='background:#2C3E50; color:#fff; text-align:center; padding:3px 0px;'>扩展字段</td></tr>");
          for (i = 0; i < array.length; i++) {
            // console.log(array[i]);
            var html = '', uploadtxt = '';
            var item = array[i];
            switch (item.type.toLowerCase()) {
              case "string"://文本
                html += '<tr>';
                html += '    <th>' + item.title + '</th>';
                html += '    <td>';
                html += '        <input class="form-control" type="text" name="post[' + item.name + ']" id="' + item.name + '" value="' + item.value + '" placeholder="请输入' + item.title + '" ' + item.required + '/>';
                html += '    </td>';
                html += '</tr>';
                break;
              case "text"://文本框
                html += '<tr>';
                html += '    <th>' + item.title + '</th>';
                html += '    <td>';
                html += '        <textarea class="form-control" name="post[' + item.name + ']" id="' + item.name + '" cols="30" rows="5" placeholder="请填写' + item.title + '" ' + item.required + '>' + item.value + '</textarea>';
                html += '    </td>';
                html += '</tr>';
                break;
              case "number"://数量
                html += '<tr>';
                html += '    <th>' + item.title + '</th>';
                html += '    <td>';
                html += '        <input class="form-control" type="text" name="post[' + item.name + ']" id="' + item.name + '" value="' + item.value + '" placeholder="请输入' + item.title + '" ' + item.required + '/>';
                html += '    </td>';
                html += '</tr>';
                break;
              case "radio"://单选按钮
                html += '<tr>';
                html += '    <th>' + item.title + '</th>';
                html += '    <td>';
                //可以用字符或字符串分割
                if (item.content != '') {
                  var content = $.trim(item.content).replace(new RegExp('\n', "gm"), ' ');//jquery实现textarea输入内容换行和空格
                  var arr = content.split(' ');//例如：1|是 0|否
                  //开始大遍历
                  $.each(arr, function (i, obj1) {
                    //开始小遍历
                    var arr2 = $.trim(obj1).split('|');//例如：1|是
                    $.each(arr2, function (j, obj2) {
                      // console.log(item.name,obj2)
                      var checked = '';
                      if (item.value == obj2) {
                        checked = " checked";
                      }
                      if (j == 0) {
                        html += '<input type="radio" name="post[' + item.name + ']" value="' + obj2 + '" ' + checked + ' />';
                      } else {
                        html += '&nbsp;' + obj2 + '&nbsp;';
                      }
                    })
                  })
                }
                html += '        <input type="hidden" name="post[radio_list][]" value="' + item.name + '">';//为了插入处理，而增加的
                html += '    </td>';
                html += '</tr>';
                break;
              case "option"://选项
                // console.log(item.bindtype);
                switch (item.bindtype.toLowerCase()) {
                  case 'text':
                    switch (item.showoption.toLowerCase()) {//下拉显示  toLowerCase转换小写
                      case "select":
                        html += '<tr>';
                        html += '    <th>' + item.title + '</th>';
                        html += '    <td><select name="post[' + item.name + ']" class="form-control" ' + item.required + '>';
                        //可以用字符或字符串分割
                        if (item.content != '') {
                          var content = $.trim(item.content).replace(new RegExp('\n', "gm"), ' ');//jquery实现textarea输入内容换行和空格
                          var arr = content.split(' ');//例如：1|是 0|否
                          //开始大遍历
                          $.each(arr, function (i, obj1) {
                            //开始小遍历
                            var arr2 = $.trim(obj1).split('|');//例如：1|是
                            $.each(arr2, function (j, obj2) {
                              var selected = '';
                              if (item.value == obj2) {
                                selected = " selected";
                              }
                              if (j == 0) {
                                html += '<option value="' + obj2 + '" ' + selected + '>';
                              } else {
                                html += '&nbsp;' + obj2 + '&nbsp;</option>';
                              }
                            })
                          })
                        }
                        html += '        <input type="hidden" name="post[radio_list][]" value="' + item.name + '">';//为了插入处理，而增加的
                        html += '   </select> </td>';
                        html += '</tr>';
                        break;
                      case "checkbox"://复选框按钮
                        html += '<tr>';
                        html += '    <th>' + item.title + '</th>';
                        html += '    <td>';
                        //可以用字符或字符串分割
                        if (item.content != '') {
                          var content = $.trim(item.content).replace(new RegExp('\n', "gm"), ' ');//jquery实现textarea输入内容换行和空格
                          var arr = content.split(' ');//例如：1|是 0|否
                          //开始大遍历 
                          //console.log(arr)
                          var b = 0;
                          $.each(arr, function (index, j) {
                            // console.log(j)
                            b++;
                            //开始小遍历
                            var arr2 = $.trim(j).split('|');//例如：1|是
                            $.each(arr2, function (index, k) {
                              if (index == 0) {
                                var checked = '';
                                if (item.value != "" && item.value.indexOf(k) != -1) {
                                  checked = " checked";
                                }
                                html += '<input type="checkbox" name="post[' + item.name + '][]" value="' + k + '" ' + checked + ' />';
                              } else {
                                if (item.showrow != '' || item.showrow != 0) {
                                  //console.log(item.showrow,b)
                                  if (item.showrow == b || b % item.showrow == 0) {
                                    html += '&nbsp;' + k + '&nbsp;<br>';
                                  } else {
                                    html += '&nbsp;' + k + '&nbsp;';
                                  }
                                }
                              }
                            })
                          })
                        }
                        html += '        <input type="hidden" name="post[checkbox_list][]" value="' + item.name + '">';//为了插入处理，而增加的
                        html += '    </td>';
                        html += '</tr>';
                        break;
                      case "radio"://单选按钮
                        html += '<tr>';
                        html += '    <th>' + item.title + '</th>';
                        html += '    <td>';
                        //可以用字符或字符串分割
                        if (item.content != '') {
                          var content = $.trim(item.content).replace(new RegExp('\n', "gm"), ' ');//jquery实现textarea输入内容换行和空格
                          var arr = content.split(' ');//例如：1|是 0|否
                          var b = 0;
                          //开始大遍历
                          $.each(arr, function (i, obj1) {
                            b++;
                            //开始小遍历
                            var arr2 = $.trim(obj1).split('|');//例如：1|是
                            $.each(arr2, function (j, obj2) {
                              var checked = '';
                              console.log(item.value);
                              if (item.value == obj2) {
                                checked = " checked";
                              }
                              if (j == 0) {
                                html += '<input type="radio" name="post[' + item.name + ']" value="' + obj2 + '" ' + checked + ' />';
                              } else {
                                if (item.showrow != '' || item.showrow != 0) {
                                  if (item.showrow == b || b % item.showrow == 0) {
                                    html += '&nbsp;' + obj2 + '&nbsp;<br>';
                                  } else {
                                    html += '&nbsp;' + obj2 + '&nbsp;';
                                  }
                                } else {
                                  html += '&nbsp;' + obj2 + '&nbsp;';
                                }
                              }
                            })
                          })
                        }
                        html += '        <input type="hidden" name="post[radio_list][]" value="' + item.name + '">';//为了插入处理，而增加的
                        html += '    </td>';
                        html += '</tr>';
                        break;
                      default:
                        break;
                    }
                    break;
                  case 'sql':
                    // console.log(item.showoption);

                    switch (item.showoption.toLowerCase()) {
                      case 'select':
                        html += '<tr>';
                        html += '    <th>' + item.title + '</th>';
                        html += '    <td><select name="post[' + item.name + ']" class="form-control" ' + item.required + ' >';
                        //可以用字符或字符串分割
                        if (item.content != '') {
                          var content = JSON.parse(item.content);//json数据转对象
                          $.each(content, function (k, v) {
                            if (item.value == k) {
                              console.log(item.value);
                              html += '<option value="' + k + '" selected>' + v + '</option>';
                            } else {
                              html += '<option value="' + k + '">' + v + '</option>';
                            }
                          })
                        } else {
                          html += '<option value="0">请选择</option>';
                        }
                        html += '        <input type="hidden" name="post[radio_list][]" value="' + item.name + '">';//为了插入处理，而增加的
                        html += '   </select> </td>';
                        html += '</tr>';
                        break;
                      case "window"://文本框
                        html += '<tr>';
                        html += '    <th>' + item.title + '</th>';
                        html += '    <td >';
                        html += '<div id="' + item.name + '"><input type="text" class="form-control"></div >';
                        html += '<a class="btn btn-xs btn-primary sss" onclick=\"sss(\'' + item.name + '\',\'' + item.bindtext + '\', \'' + item.sqlname + '\',\'' + item.sqlval + '\',\'' + channel_id + '\',\'' + postId + '\',\'' + table + '\')\" >选择数据</a>';
                        html += '    </td>';
                        html += '</tr>';
                        let idname = '#' + item.name;
                        // let classname = item.name;
                        console.log(item.value);

                        // localStorage.setItem("idvalue", idvalue);
                        // console.log(localStorage.getItem('idname'));
                        // idname = idname.filter(s => $.trim(s).length > 0);//ja清除数组里面的空数据
                        let idvalue = JSON.parse(item.value);

                        setTimeout(test, 500);

                        function test() {

                          $(idname).tagsinput({
                            itemValue: 'value',
                            itemText: 'name',
                            freeInput: false,    //不允许输入，标签类型为对象时此属性无效
                            allowDuplicates: false,    //不允许重复，只姓名重复不受影响
                            trimValue: true,    //去掉空格
                            interactive: false,//不允许外部输入标签
                          });
                          for (let i = 0; i < idvalue.length; i++) {
                            $(idname).tagsinput('add', { "value": idvalue[i].value, "name": idvalue[i].name });
                          }
                        }
                        // var tag = item.value;
                        html += '<input class="' + item.name + '" type="hidden" name="post[' + item.name + ']" value=\'' + item.value + '\'>';
                        break;
                      case "checkbox"://复选框按钮
                        html += '<tr>';
                        html += '    <th>' + item.title + '</th>';
                        html += '    <td>';
                        //可以用字符或字符串分割
                        if (item.content != '') {
                          var content = JSON.parse(item.content);//json数据转对象
                          var value = item.value.split(",");
                          var c = 0;
                          $.each(content, function (k, v) {
                            //console.log(item.showrow,b)
                            b++;
                            if (value[c] == k) {
                              html += '<input type="checkbox" name="post[' + item.name + '][]" value="' + k + '" checked />';
                              //console.log(item.showrow,b)
                            } else {
                              html += '<input type="checkbox" name="post[' + item.name + '][]" value="' + k + '"  />';
                            }
                            if (item.showrow != '' || item.showrow != 0) {
                              if (item.showrow == b || b % item.showrow == 0) { //???这里怎么判断
                                html += '&nbsp;' + v + '&nbsp;<br>';
                              } else {
                                html += '&nbsp;' + v + '&nbsp;';
                              }
                            } else {
                              html += '&nbsp;' + v + '&nbsp;';
                            }
                            c++;
                            if (c == value.length) {
                              c = 0;
                            }
                          })
                        }
                        html += '        <input type="hidden" name="post[checkbox_list][]" value="' + item.name + '">';//为了插入处理，而增加的
                        html += '    </td>';
                        html += '</tr>';
                        break;
                      case "radio"://单选按钮
                        html += '<tr>';
                        html += '    <th>' + item.title + '</th>';
                        html += '    <td>';
                        //可以用字符或字符串分割
                        if (item.content != '') {
                          var content = JSON.parse(item.content);//json数据转对象
                          var b = 0;
                          $.each(content, function (k, v) {
                            b++;
                            // console.log(b)
                            if (item.value == k) {
                              html += '<input type="radio" name="post[' + item.name + ']" value="' + k + '" checked />';
                              //console.log(item.showrow,b)
                            } else {
                              html += '<input type="radio" name="post[' + item.name + ']" value="' + k + '"  />';
                            }
                            if (item.showrow != '' || item.showrow != 0) {
                              if (item.showrow == b || b % item.showrow == 0) { //???这里怎么判断
                                // console.log(b/item.showrow);
                                html += '&nbsp;' + v + '&nbsp;<br>';
                              } else {
                                html += '&nbsp;' + v + '&nbsp;';
                              }
                            } else {
                              html += '&nbsp;' + v + '&nbsp;';
                            }

                          })

                        }
                        html += '        <input type="hidden" name="post[radio_list][]" value="' + item.name + '">';//为了插入处理，而增加的
                        html += '    </td>';
                        html += '</tr>';
                    }

                    break;
                  case 'tree':
                    html += '<tr>';
                    html += '    <th>' + item.title + '</th>';
                    html += '<td><select name="post[' + item.name + ']"  ' + item.required + ' class="form-control ' + item.name + '"  >' + item.content + '</select></td>';
                    html += '        <input type="hidden" name="post[radio_list][]" value="' + item.name + '">';//为了插入处理，而增加的
                    // html +="<script>";
                    // html += "$()"
                    // html +="</script>";

                    html += '</tr>';
                    break;
                  case 'channel':
                    html += '<tr>';
                    html += '    <th>' + item.title + '</th>';
                    html += '    <td><select name="post[' + item.name + ']" class="form-control" ' + item.required + '>';
                    //可以用字符或字符串分割
                    if (item.content != '') {
                      var content = JSON.parse(item.content);//json数据转对象
                      $.each(content, function (k, v) {
                        if (item.value == k) {
                          html += '<option value="' + k + '" selected>' + v + '</option>';
                        } else {
                          html += '<option value="' + k + '">' + v + '</option>';
                        }
                      })
                    } else {
                      html += '<option value="0">请选择</option>';
                    }
                    html += '        <input type="hidden" name="post[radio_list][]" value="' + item.name + '">';//为了插入处理，而增加的
                    html += '   </select> </td>';
                    html += '</tr>';
                    break;
                  default:
                    break;


                }

                break;
              case "checkbox"://复选框按钮
                html += '<tr>';
                html += '    <th>' + item.title + '</th>';
                html += '    <td>';
                //可以用字符或字符串分割
                if (item.content != '') {
                  var content = $.trim(item.content).replace(new RegExp('\n', "gm"), ' ');//jquery实现textarea输入内容换行和空格
                  var arr = content.split(' ');//例如：1|是 0|否
                  //开始大遍历
                  $.each(arr, function (index, j) {
                    //开始小遍历
                    var arr2 = $.trim(j).split('|');//例如：1|是
                    $.each(arr2, function (index, k) {
                      if (index == 0) {
                        var checked = '';
                        if (item.value != "" && item.value.indexOf(k) != -1) {
                          checked = " checked";
                        }
                        html += '<input type="checkbox" name="post[' + item.name + '][]" value="' + k + '" ' + checked + ' />';
                      } else {
                        html += '&nbsp;' + k + '&nbsp;';
                      }
                    })
                  })
                }
                html += '        <input type="hidden" name="post[checkbox_list][]" value="' + item.name + '">';//为了插入处理，而增加的
                html += '    </td>';
                html += '</tr>';
                break;
              case "date"://日期
                html += '<tr>';
                html += '    <th>' + item.title + '</th>';
                html += '    <td>';
                html += '        <input class="form-control js-bootstrap-' + item.name + '" type="text" name="post[' + item.name + ']" value="' + item.value + '" ' + item.required + '/>';
                html += '<script>';
                html += '   $(function () {';
                html += '       var bootstrapDateInput_' + item.name + ' = $("input.js-bootstrap-' + item.name + '");';
                html += '       if (bootstrapDateInput_' + item.name + '.length) {';
                html += '           Wind.css(\'bootstrapDatetimePicker\');';
                html += '           Wind.use(\'bootstrapDatetimePicker\', function () {';
                html += '               bootstrapDateInput_' + item.name + '.datetimepicker({';
                html += '                   language: \'zh-CN\',';
                html += '                   format: \'yyyy-mm-dd\',';
                html += '                   minView: \'month\',';
                html += '                   todayBtn: 1,';
                html += '                   autoclose: true';
                html += '               });';
                html += '           });';
                html += '       }';
                html += '   })';
                html += '<\/script>';
                html += '    </td>';
                html += '</tr>';
                break;
              case "time"://时间
                html += '<tr>';
                html += '    <th>' + item.title + '</th>';
                html += '    <td>';
                html += '        <input class="form-control js-bootstrap-' + item.name + '" type="text" name="post[' + item.name + ']" value="' + item.value + '" ' + item.required + ' />';
                html += '<script>';
                html += '   $(function () {';
                html += '       var bootstrapDateInput_' + item.name + ' = $("input.js-bootstrap-' + item.name + '");';
                html += '       if (bootstrapDateInput_' + item.name + '.length) {';
                html += '           Wind.css(\'bootstrapDatetimePicker\');';
                html += '           Wind.use(\'bootstrapDatetimePicker\', function () {';
                html += '               bootstrapDateInput_' + item.name + '.datetimepicker({';
                html += '                   language: \'zh-CN\',';
                html += '                   format: \'hh:ii\',';
                html += '                   autoclose: true';
                html += '               });';
                html += '           });';
                html += '       }';
                html += '   })';
                html += '<\/script>';
                html += '    </td>';
                html += '</tr>';
                break;
              case "datetime"://日期时间
                html += '<tr>';
                html += '    <th>' + item.title + '</th>';
                html += '    <td>';
                // if (item.title == '发布时间') {
                //   html += '        <input class="form-control js-bootstrap-' + item.name + '" type="text" name="post[' + item.name + ']" value="' + getFormatDate() + '" ' + item.required + ' />';
                // } else {
                  html += '        <input class="form-control js-bootstrap-' + item.name + '" type="text" name="post[' + item.name + ']" value="' + item.value + '" ' + item.required + ' />';
                // }
                html += '<script>';
                html += '   $(function () {';
                html += '       var bootstrapDateInput_' + item.name + ' = $("input.js-bootstrap-' + item.name + '");';
                html += '       if (bootstrapDateInput_' + item.name + '.length) {';
                html += '           Wind.css(\'bootstrapDatetimePicker\');';
                html += '           Wind.use(\'bootstrapDatetimePicker\', function () {';
                html += '               bootstrapDateInput_' + item.name + '.datetimepicker({';
                html += '                   language: \'zh-CN\',';
                html += '                   format: \'yyyy-mm-dd hh:ii\',';
                html += '                   minView: 0,';
                html += '                   minuteStep: 1,';
                html += '                   todayBtn: 1,';
                html += '                   autoclose: true';
                html += '               });';
                html += '           });';
                html += '       }';
                html += '   })';
                html += '<\/script>';
                html += '    </td>';
                html += '</tr>';
                // console.log(item.value);
                break;
              case "editor"://编辑器
                html += '<tr>';
                html += '    <th style="width:12%;">' + item.title + '<span class="form-required">*</span></th>';
                html += '    <td>';
                html += '        <script type="text/plain" id="' + item.name + '" name="post[' + item.name + ']">' + item.value + '<\/script>';
                html += '    </td>';
                html += '<script>';
                html += '   $(function () {'
                html += '       editor_' + item.name + ' = new baidu.editor.ui.Editor();';
                html += '       editor_' + item.name + '.render(\'' + item.name + '\');';
                html += '       try {';
                html += '           editor_' + item.name + '.sync();';
                html += '       } catch (err) {';
                html += '       }';
                html += '   })';
                html += '<\/script>';
                html += '</tr>';
                break;
              case "image"://单图片
                html += '<tr>';
                html += '    <th>' + item.title + '</th>';
                html += '    <td>';
                html += '        <input type="hidden" name="post[' + item.name + ']" id="' + item.name + '" value="' + item.value + '">';
                html += '        <a href="javascript:uploadOneImage(\'图片上传\',\'#' + item.name + '\');">';
                html += '            <img src="' + item.value + '" id="' + item.name + '-preview" width="60" style="cursor: pointer" />';
                html += '        </a>';
								html += '<p style="color:red;">'+item.msg+'</p>';
                html += '        <input type="button" class="btn btn-sm btn-cancel-' + item.name + '" value="取消图片">';
                html += '    </td>';
                html += '<script>';
                html += '   $(function () {';
                html += '      $(\'.btn-cancel-' + item.name + '\').click(function () {';
                html += '          $(\'#' + item.name + '-preview\').attr(\'src\', \'/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png\');';
                html += '          $(\'#' + item.name + '\').val(\'\');';
                html += '      });';
                html += '   })';
                html += '<\/script>';
                html += '</tr>';
                break;
              case "images"://多图片(相册)
                html += '<tr>';
                html += '    <th>' + item.title + '</th>';
                html += '    <td>';
                // id="foo" class="block__list block__list_words"
                html += '        <ul id="' + item.name + '" class="pic-list list-unstyled form-inline">' + item.value + '</ul>';
								html += '<p style="color:red;">'+item.msg+'</p>';
                html += '        <a href="javascript:uploadMultiImage(\'图片上传\',\'#' + item.name + '\',\'' + item.name + '-item-tpl\');" class="btn btn-default btn-sm">选择图片</a>';
                html += '        <input type="hidden" name="post[images_files][]" value="' + item.name + '">';//为了插入处理，而增加的
                html += '    </td>';
                html += '</tr>';

                uploadtxt += '<script type="text/html" id="' + item.name + '-item-tpl">';
                uploadtxt += '    <li id="saved-image{id}">';
                uploadtxt += '        <input id="photo-{id}" type="hidden" name="post[' + item.name + '_urls][]" value="{filepath}">';
                uploadtxt += '        <input class="form-control" id="photo-{id}-name" type="text" name="post[' + item.name + '_names][]" value="{name}" style="width: 200px;" title="图片名称">';
                uploadtxt += '        <img id="photo-{id}-preview" src="{url}" style="height:36px;width: 36px;" onclick="imagePreviewDialog(this.src);">';
                uploadtxt += '        <a href="javascript:uploadOneImage(\'图片上传\',\'#photo-{id}\');">替换</a>';
                uploadtxt += '        <a href="javascript:(function(){$(\'#saved-image{id}\').remove();})();">移除</a>';
                uploadtxt += '    </li>';
                uploadtxt += '<\/script>';

                uploadtxt += '<script>';
                uploadtxt += '        var foo = document.getElementById("' + item.name + '");'
                uploadtxt += '      Sortable.create(foo, { group: "omega" });'
                uploadtxt += "<\/script>'"
                break;
              case "file"://单文件
                html += '<tr>';
                html += '    <th>' + item.title + '</th>';
                html += '    <td class="form-inline">';
                // html += '            <input id="file-' + item.name + '" class="form-control" type="text" name="post[' + item.name + ']" value="' + item.value + '" placeholder="请上传文件" style="width: 200px;">';
                // html += '            <a id="file-' + item.name + '-preview" href="' + item.value + '"target="_blank">下载</a>';
                html += item.value;
                html += '            <a href="javascript:uploadOne(\'文件上传\',\'#file-' + item.name + '\',\'file\');">上传</a>';
                html += '    </td>';
                html += '</tr>';
                break;
              case "files"://多文件
                html += '<tr>';
                html += '    <th>' + item.title + '</th>';
                html += '    <td>';
                html += '        <ul id="' + item.name + '" class="pic-list list-unstyled form-inline">' + item.value + '</ul>';
                html += '        <a href="javascript:uploadMultiFile(\'文件上传\',\'#' + item.name + '\',\'' + item.name + '-item-tpl\',\'file\');" class="btn btn-default btn-sm">选择文件</a>';
                html += '        <input type="hidden" name="post[images_files][]" value="' + item.name + '">';//为了插入处理，而增加的
                html += '    </td>';
                html += '</tr>';

                uploadtxt += '<script type="text/html" id="' + item.name + '-item-tpl">';
                uploadtxt += '    <li id="saved-file{id}">';
                uploadtxt += '        <input id="file-{id}" type="hidden" name="post[' + item.name + '_urls][]" value="{filepath}">';
                uploadtxt += '        <input class="form-control" id="file-{id}-name" type="text" name="post[' + item.name + '_names][]" value="{name}" style="width: 200px;" title="文件名称">';
                uploadtxt += '        <a id="file-{id}-preview" href="{preview_url}" target="_blank">下载</a>';
                uploadtxt += '        <a href="javascript:uploadOne(\'文件上传\',\'#file-{id}\',\'file\');">替换</a>';
                uploadtxt += '        <a href="javascript:(function(){$(\'#saved-file{id}\').remove();})();">移除</a>';
                uploadtxt += '    </li>';
                uploadtxt += '<\/script>';
                break;
              default:
                break;
            }
            $("#extend>table").append(html);
            $("#extend_upload").append(uploadtxt);
          }
        }
      } else {
        loading_hide();
      }
    }
  })
}

function getFormatDate() {
  var nowDate = new Date();
  var year = nowDate.getFullYear();
  var month = nowDate.getMonth() + 1 < 10 ? "0" + (nowDate.getMonth() + 1) : nowDate.getMonth() + 1;
  var date = nowDate.getDate() < 10 ? "0" + nowDate.getDate() : nowDate.getDate();
  var hour = nowDate.getHours() < 10 ? "0" + nowDate.getHours() : nowDate.getHours();
  var minute = nowDate.getMinutes() < 10 ? "0" + nowDate.getMinutes() : nowDate.getMinutes();
  var second = nowDate.getSeconds() < 10 ? "0" + nowDate.getSeconds() : nowDate.getSeconds();
  return year + "-" + month + "-" + date + " " + hour + ":" + minute;
}

function sss(name, sql, sqlname, sqlval, channel_id, postid, tablename) {
  // console.log('#'+name);
  // var name = "\'#"+name+"\'";
  var idname = "#" + name;
  var classname = "." + name;
  var nextValue = $(classname).val();
  // console.log(nextValue);

  layui.use('table', function () {
    var table = layui.table;
    layer.open({
      type: 1,
      area: ["500px", '230px'],
      title: "添加数据",
      maxmin: true,
      content: '<table id="templateTable" ></table>',
      btn: ['确认选择'],
      success: function (layero, index) {
        table = $.extend(table, { config: { checkName: 'checked' } });//默认已选中的
        table.render({
          elem: '#templateTable'
          , id: 'idTest'
          , url: '/cms/cms_window/windowEditPost'
          , where: { 'sql': sql, 'sqlname': sqlname, 'sqlval': sqlval, 'channel_id': channel_id, 'postid': postid, 'tablename': tablename, 'name': name, 'nextValue': nextValue }
          , page: true //开启分页
          , size: 'sm' //小尺寸的表格
          , cols: [[ //表头
            { type: 'checkbox', width: 50 }
            , { field: 'id', title: 'ID', hide: true }
            , { field: 'id', title: 'ID' }
            , { field: 'name', title: '名称' }
          ]]
          , response: {
            statusName: 'code' //规定数据状态的字段名称，默认：code
            , statusCode: 1 //规定成功的状态码，默认：0
            , msgName: 'msg' //规定状态信息的字段名称，默认：msg
            , countName: 'total' //规定数据总数的字段名称，默认：count
            , dataName: 'data' //规定数据列表的字段名称，默认：data
          }
          ,

          //,…… //其他参数
        });

      },
      yes: function (index, layero) {
        var checkStatus = table.checkStatus('idTest');
        // console.log(checkStatus);
        var ids = [];
        var tags = [];
        $(checkStatus.data).each(function (i, o) {//o即为表格中一行的数据
          // console.log(o);

          ids.push(o.id);
          tags.push({ 'value': o.id, 'name': o.name });
        });
        if (ids.length < 1) {
          layer.msg('无选中项');
          return false;
        }
        ids = ids.join(",");

        // console.log(tags);

        // $.post('/cms/cms_window/windowAddPost', { 'data': tags }, function (data) {
        //   // console.log(data);
        //   if (data.code = 1) {
        layer.msg('编辑成功');
        // } else {
        // layer.msg(data.msg);
        // $().tagsinput('destroy');
        // }
        // })
        // if()
        $(idname).tagsinput('destroy');//清除之前的标签
        tagInput(idname, tags);
        var tag = JSON.stringify(tags);
        // console.log(name);

        // $(idname).parent('td').append('<input class="' + name + '" type="hidden" name="post[' + name + ']" value=\'' + tag + '\'>');
        $(classname).val(tag);
        layer.close(index); //如果设定了yes回调，需进行手工关闭
      },
    });
  });
}

$(window).on('itemRemoved', function (event) {
  // event.item: contains the item
  console.log(event.item);
  var idname = event.target.id;
  // console.log(a);
  var hiddenTag = JSON.parse($('.' + idname).val());
  // console.log(hiddenTag);	
  for (let i = 0; i < hiddenTag.length; i++) {
    if (event.item.name == hiddenTag[i].name) {
      hiddenTag.splice(i, 1);
    }
  }
  // console.log(hiddenTag);
  $('.' + idname).val(JSON.stringify(hiddenTag));
  // console.log(hiddenTag);
});



function tagInput(idname, tags) {
  $(idname).tagsinput({
    itemValue: 'value',
    itemText: 'name',
    freeInput: false,    //不允许输入，标签类型为对象时此属性无效
    allowDuplicates: false,    //不允许重复，只姓名重复不受影响
    trimValue: true,    //去掉空格
    interactive: false,//不允许外部输入标签
  });
  // console.log(name);

  for (let i = 0; i < tags.length; i++) {
    $(idname).tagsinput('add', { "value": tags[i].value, "name": tags[i].name });
  }
  $(idname).tagsinput('refresh');

}
//bca-disable