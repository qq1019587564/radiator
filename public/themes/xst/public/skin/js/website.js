/*!function ($){
	var _w=$(window).width()>=750?750:$(window).width();
	var fontPercent=_w/7.5;//16px;
	$('html').css('font-size',fontPercent);
	$(window).resize(function (){
		var _w=$(window).width()>=750?750:$(window).width();
		var fontPercent=_w/7.5;//16px;
		$('html,body').css('font-size',fontPercent);
	});
}(jQuery);*/
$(function(){
	$(".fix_height").height($(".header").outerHeight(true));
	$(".header .nav a").mouseover(function(){
		$(".header .nav_list .navBox").show();
	});
	$(".goTop").click(function(){
		$('body,html').animate({scrollTop:0},1000); 
	});
	/*$(".right_menu .box.call .op0").click(function(){
		// $(this).children().not(".form").css("opacity",0);
		$(".right_menu .box.call").addClass("hide");
		$(".right_menu .box.call .form").addClass("open");
	});*/
	$(".right_menu .box.call .form .close .fa").click(function(){
		$(".right_menu .box.call").removeClass("hide");
		$(".right_menu .box.call .form").removeClass("open");

	});
	$(".header .ps .hSearch img").click(function(){
		$(this).parent().find("input").show();
	});
	$(".header .ps .m_nav_button").click(function(){
		var nt = $(".header").outerHeight(true);
		$(".mb_nav").css("top",nt);
		$(".mb_nav").toggleClass("show");
		$(".mask").toggleClass("mask_visible");
	});
	$(".mask").click(function(){
		$(this).removeClass("mask_visible");
		$(".mb_nav").removeClass("show");
	});
	$(".right_menu").hover(function(){
		$(this).toggleClass("show");
	});
});

$(window).resize(function(){
	$(".fix_height").height($(".header").outerHeight(true));
	$(".mb_nav").css("top",$(".header").outerHeight(true));
});

function broserBack(u1,u2){
 	if (window.history && window.history.pushState) {

    $(window).on('popstate', function() {
      var hashLocation = location.hash;
      var hashSplit = hashLocation.split("#!/");
      var hashName = hashSplit[1];

      if (hashName !== '') {
        var hash = window.location.hash;
        if (hash === '') {
			window.location=u1;
          //alert('後退按鈕點擊');
        }
      }
    });

  	window.history.pushState('forward', null, u2);
  }
}

function WH(o,a){
	$(window).resize(function(){WHBiLi(o,a)});
	WHBiLi(o,a);
	function WHBiLi(obj,a){
		var a=a||1;
		if($(obj).length<1){
			return false;
		}
		var w=parseInt($(obj).eq(0).width());
		$(obj).css('height',a*w);
	}
}