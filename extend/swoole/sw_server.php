<?php
// require_once 'db.php';
// require_once 'fun.php';
// namespace swoole;

use Swoole\WebSocket;

class WebsocketServer
{
    private $host = "0.0.0.0";

    private $port = "9501";
    /**
     * [private description]
     * @var Swoole\Table
     */
    private $table;

    /**
     * [private description]
     * @var Swoole\WebSocket\Server
     */
    private $server;
    
    // worker_num是用来处理请求逻辑的进程数,task_num是异步任务投递进程
    
    private $config = [
        'worker_num'=>2,
        //task_worker_num  最大数量不超过 swoole_cpu_num*1000
        'task_worker_num' => 8,
        // 1, 使用unix socket通信
        // 2, 使用消息队列通信
        // 3, 使用消息队列通信，并设置为争抢模式
        'task_ipc_mode'   => 3,
    ];
    
    /**
     * [protected description]
     * @var Swoole\Lock
     */
    protected $lock;

    function __construct()
    {
        $this->server = new WebSocket\Server($this->host, $this->port);
        $this->lock = new Swoole\Lock(SWOOLE_MUTEX);
        $this->setConfig();
        $this->onInit();
        // $this->tableInit();
        // echo swoole_get_local_ip()['ens33'].":".$this->port."\n";
        // var_dump(swoole_cpu_num());
        // $this->test();
        // $this->redis =  new Redis();
        // $this->redis->connect('127.0.0.1', 6379);
       
    }

    public function open($server, $req)
    {
        // var_dump($req);
        $server->push($req->fd, "hello, welcome\n");
        // $this->table->set($req->get['http_id'], ['fd' => $req->fd]);
        // $this->table->set($req->get['http_id'], ['fd' => $req->fd]);
        // $this->table->set("startTime", ['fd' => time()]);// 当前完成的量
        // $this->table->set('123456', ['fd' => $req->fd]);
    }
    public function message($server, $frame)
    {
        $recvData = json_decode($frame->data, true);
        $recvData['fd'] = $frame->fd;
        // var_dump($recvData);
        $server->task($recvData);
        // $server->push($frame->fd, json_encode(["msg" => "正在处理".date('Y-m-d H:i:s',time())]));
    }

    public function task($server, $task_id,$reactor_id ,$data)
    {
        // var_dump("select db from `db_all_copy1` where db like '%website%' limit ".$data['data'].','.$data['avg']);
        // var_dump($data);
        
        // // 记录完成的任务
        // $this->lock->lock(); // 加锁避免多进程不安全问题
        // $task_count = ($this->table->get("task_count"))['fd'];
        // $count = $task_count + $data['avg'];
        // // var_dump("当前的处理量".$count);
        // $this->table->set("task_count", ['fd' => $count]);
        // $this->lock->unlock();
        // var_dump($data);
        // var_dump('sh /root/shell/file_rsync.sh '.$data['dir']);
        exec('sh /root/shell/file_rsync.sh '.$data['dir'],$out,$status);
        // // 通知
        $server->finish($data);
    }
    
    public function onfinish($server, $task_id, $data)
    {
        // var_dump($data['dir']);
        $server->push($data['fd'], json_encode(["msg" => "1个任务，处理完成".date('Y-m-d H:i:s',time())]));
    }

    protected function tableInit()
    {
        $this->table = new Swoole\Table(1 * 1024 * 1024);
        $this->table->column('fd', Swoole\Table::TYPE_INT);
        $this->table->create();
    }

    protected function onInit()
    {
        // [$this, 'open'] 把对象方法转为闭包参数传递
        $this->server->on('open', [$this, 'open']);
        $this->server->on('message', [$this, 'message']);
        $this->server->on('close', [$this, 'close']);
        $this->server->on('task', [$this, 'task']);
        $this->server->on('finish', [$this, 'onfinish']);
    }

    protected function setConfig()
    {
        $msg_key = ftok(__DIR__,'u');
        $this->config['message_queue_key'] = $msg_key;
        $this->server->set($this->config);
    }
    
  

    public function close($server, $fd){}

    public function start()
    {
        $this->server->start();
    }
    
 

}

(new WebSocketServer)->start();