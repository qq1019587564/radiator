<?php
use think\facade\Route;

Route::get('/a/guanyuwomen/gongsijianjie/', 'cms/Page/index?id=5')->append(array('id' => '5',));

Route::get('/a/guanyuwomen/qiyewenhua/', 'cms/Page/index?id=6')->append(array('id' => '6',));

Route::get('/a/guanyuwomen/yuangongfengcai/', 'cms/Page/index?id=7')->append(array('id' => '7',));

Route::get('/a/guanyuwomen/fazhanlicheng/', 'cms/Page/index?id=8')->append(array('id' => '8',));

Route::get('/a/lianxiwomen/', 'cms/Page/index?id=12')->append(array('id' => '12',));

Route::get('/a/xinwenzixun/chanpinzhishi/', 'cms/List/index?id=11')->append(array('id' => '11',));

Route::get('/a/xinwenzixun/chanpinzhishi/:id', 'cms/Article/index?cid=11')->append(array('cid' => '11',))->ext('html');

Route::get('/a/chanpinzhanshi/', 'cms/List/index?id=1')->append(array('id' => '1',));

Route::get('/a/chanpinzhanshi/:id', 'cms/Article/index?cid=1')->append(array('cid' => '1',))->ext('html');

Route::get('/a/xinwenzixun/', 'cms/List/index?id=9')->append(array('id' => '9',));

Route::get('/a/xinwenzixun/:id', 'cms/Article/index?cid=9')->append(array('cid' => '9',))->ext('html');

Route::get('/fengshan/', 'cms/List/index?id=13')->append(array('id' => '13',));

Route::get('/fengshan/:id', 'cms/Article/index?cid=13')->append(array('cid' => '13',))->ext('html');

Route::get('/sanreqi/', 'cms/List/index?id=14')->append(array('id' => '14',));

Route::get('/sanreqi/:id', 'cms/Article/index?cid=14')->append(array('cid' => '14',))->ext('html');

Route::get('xinshengtai$', 'admin/Index/index');


