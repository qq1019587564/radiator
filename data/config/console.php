<?php
return [
    'commands' => [
        'sync' => 'app\cms\command\sync',
        'sysconfig' => 'app\cms\command\sysconfig',
        'arctype' => 'app\cms\command\arctype',
        'flink' => 'app\cms\command\flink',
        'archives' => 'app\cms\command\archives',
        'myppt' => 'app\cms\command\myppt',
        'diyforms' => 'app\cms\command\diyforms',
        'updateTheme' => 'app\cms\command\updateTheme',
        'deleteAll' => 'app\cms\command\deleteAll',
        'hot' => 'app\cms\command\hot',
        'channeltype' => 'app\cms\command\channeltype',
        'home' => 'app\cms\command\home',
        'homePage' => 'app\cms\command\homePage',
        'homeList' => 'app\cms\command\homeList',
        'homeArticle'=>'app\cms\command\homeArticle',
        'homeImage'=>'app\cms\command\homeImage',
        'mobile' => 'app\cms\command\mobile',
        'mobilePage' => 'app\cms\command\mobilePage',
        'mobileList' => 'app\cms\command\mobileList',
        'mobileArticle'=>'app\cms\command\mobileArticle',
        'mobileImage'=>'app\cms\command\mobileImage',
        'homeOneList'=>'app\cms\command\homeOneList',
        'homeOneArticle'=>'app\cms\command\homeOneArticle',
        'homeOneImage'=>'app\cms\command\homeOneImage',
        'homeOnePage'=>'app\cms\command\homeOnePage',
        'mobileOneList'=>'app\cms\command\mobileOneList',
        'mobileOneArticle'=>'app\cms\command\mobileOneArticle',
        'mobileOneImage'=>'app\cms\command\mobileOneImage',
        'mobileOnePage'=>'app\cms\command\mobileOnePage',
        'siteXml'=>'app\cms\command\siteXml',
        'createRoute'=>'app\cms\command\createRoute',
        'theme'=>'app\cms\command\theme',
        
        
        
    ]
];